import 'package:flutter/material.dart';
import 'package:flutx/flutx.dart';
import 'package:get/get.dart';

abstract class BaseWidget extends StatelessWidget {
  const BaseWidget({Key? key}) : super(key: key);
}

abstract class BaseScreen extends StatefulWidget {
  const BaseScreen({Key? key}) : super(key: key);
}

abstract class BaseState<Screen extends BaseScreen> extends State<Screen> {}

mixin BasicStateMixin<Screen extends BaseScreen> on BaseState<Screen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: createAppBar(),
          bottomNavigationBar: createBottomBar(),
          body: createBody()),
    );
  }

  Widget createBody();
  Widget? createEndDrawer() => null;

  Widget? createFloatingActionButton() {
    return null;
  }

  FloatingActionButtonLocation setFloatingActionButtonLocation() =>
      FloatingActionButtonLocation.centerDocked;

  AppBar? createAppBar() => buildDefaultAppBar(title: "");

  Widget? createBottomBar() => null;

  AppBar buildDefaultAppBar(
      {required String title,
      IconData? icon,
      List<Widget>? actions,
      bool showBack = true,
      bool isViewPrj = false,
      RxBool? edit,
      Color? color,
      Color? iconColor,
      Color? textColor,
      String? routeName,
      data,
      Function(dynamic)? callback,
      bool automaticallyImplyLeading = true}) {
    return AppBar(
      elevation: 0,
      // centerTitle: true,
      automaticallyImplyLeading: automaticallyImplyLeading,
      title: isViewPrj == false
          ? FxText.titleMedium(
              title,
            )
          : GestureDetector(
              onTap: () => {Get.toNamed(routeName.toString(), arguments: data)},
              child: FxText.titleMedium(
                title,
              ),
            ),
      leading: showBack
          ? BackButton(
              onPressed: () {
                Get.back();
              },
            )
          : null,
      actions: [
        IconButton(
          icon: Icon(icon, color: Colors.black),
          onPressed: () {
            if (callback != null) callback(null);
          },
        )
      ],
    );
  }
}
