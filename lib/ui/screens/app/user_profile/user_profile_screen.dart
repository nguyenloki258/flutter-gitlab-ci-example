import 'package:core_packages/core_package.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:serpapp/localizations/language.dart';
import 'package:serpapp/theme/app_notifier.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/app/base_bottom_bar.dart';
import 'package:serpapp/ui/screens/app/notification/notification_screen.dart';
import 'package:serpapp/ui/screens/app/user_profile/user_profile_controller.dart';

class UserProfileScreen extends BaseScreen {
  static const String routeName = '/user_profile';

  const UserProfileScreen({Key? key}) : super(key: key);
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends BaseState<UserProfileScreen>
    with BasicStateMixin {
  //late ThemeData theme;
  UserProfileController userProfileController = Get.find();
  SelectOptionItem? _selectedLang;
  var inputValue;
  @override
  void initState() {
    //theme = AppTheme.estateTheme;
    super.initState();
  }

  _openCamera() async {
    try {
      final pickedFile = await ImagePicker().pickImage(
        source: ImageSource.gallery,
        imageQuality: 100,
      );

      await userProfileController.updateAvatar(pickedFile?.path ?? '');
      // if (res.state == ActionState.Success) {
      // ToastX.showSnackbarInfo('Avatar was saved successful.', context);
      // } else {
      //   ToastX.showSnackbarError(res.message ?? 'Error'.tr, context);
      // }
    } catch (e) {
      // print('userpapprofile, openCamera - ex=' + e.toString());
    }
  }

  Widget _buildCategory(String setting) {
    return FxText.bodySmall(
      setting,
      fontWeight: 700,
      letterSpacing: 0.2,
      muted: true,
    );
  }

  void changeTheme() {
    Get.changeTheme(Get.isDarkMode ? ThemeData.light() : ThemeData.dark());
    setState(() {});
  }

  showLanguagePicker() {
    Get.bottomSheet(buildPickerSingleChoice(
        title: "Chọn ngôn ngữ",
        value: SelectOptionItem(
            key: Language.currentLanguage.languageName,
            value: Language.currentLanguage.locale,
            data: Language.currentLanguage),
        options: Language.languages
            .map((e) =>
                SelectOptionItem(key: e.languageName, value: e.locale, data: e))
            .toList(),
        onSelect: ((p0) {
          _selectedLang = p0;
        }),
        onSubmit: () {
          setState(() {
            Provider.of<AppNotifier>(context, listen: false)
                .changeLanguage(_selectedLang?.data as Language);
          });
        }));
  }

  showEditText({
    required String title,
    required String? value,
    // required Function(String) onSelect,
    required Function() onSubmit,
    TextInputType? keyboardType,
  }) {
    this.inputValue = value;
    Get.bottomSheet(
        Container(
          height: 250,
          margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8), bottom: Radius.circular(0)),
            color: Get.theme.scaffoldBackgroundColor,
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Drawer(
            child: Container(
              color: Get.theme.scaffoldBackgroundColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: FxSpacing.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () {
                            Get.back();
                          },
                          child: SizedBox(
                            width: 100,
                            child: FxText.bodyMedium(
                              'Huỷ',
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Expanded(
                          child: FxText.bodyLarge(
                            'Sửa: $title',
                            textAlign: TextAlign.center,
                            fontWeight: 600,
                          ),
                        ),
                        InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () {
                            Get.back();
                            onSubmit();
                          },
                          child: SizedBox(
                            width: 100,
                            child: FxText.bodyMedium(
                              'Xác nhận',
                              color: Get.theme.colorScheme.primary,
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  FxSpacing.height(8),
                  Expanded(
                      child: Container(
                    margin:
                        FxSpacing.only(bottom: 10, top: 0, right: 10, left: 10),
                    child: TextFormField(
                      onChanged: (text) {
                        this.inputValue = text;
                      },
                      // controller: myController,
                      enabled: true,
                      initialValue: value,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: keyboardType,
                    ),
                  ))
                ],
              ),
            ),
          ),
        ),
        isScrollControlled: true);
  }

  // showGenderPicker({
  //   required String title,
  //   required int? value,
  //   // required Function(String) onSelect,
  //   required Function() onSubmit,
  // }) {
  //   List _gender = ['Nữ', 'Nam', 'Khác'];
  //   List<Widget> _genderIcon = [
  //     Icon(Icons.female),
  //     Icon(Icons.male),
  //     Icon(Icons.transgender),
  //   ];
  //   this.inputValue = 0;
  //   Container(
  //     height: 250,
  //     margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
  //     decoration: BoxDecoration(
  //       borderRadius: const BorderRadius.vertical(
  //           top: Radius.circular(8), bottom: Radius.circular(0)),
  //       color: Get.theme.scaffoldBackgroundColor,
  //     ),
  //     clipBehavior: Clip.antiAliasWithSaveLayer,
  //     child: Drawer(
  //       child: Container(
  //         color: Get.theme.scaffoldBackgroundColor,
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Padding(
  //               padding: FxSpacing.all(12),
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //                   InkWell(
  //                     splashColor: Colors.transparent,
  //                     highlightColor: Colors.transparent,
  //                     onTap: () {
  //                       Get.back();
  //                     },
  //                     child: SizedBox(
  //                       width: 100,
  //                       child: FxText.bodyMedium(
  //                         'Huỷ',
  //                         textAlign: TextAlign.left,
  //                       ),
  //                     ),
  //                   ),
  //                   Expanded(
  //                     child: FxText.bodyLarge(
  //                       'Sửa: $title',
  //                       textAlign: TextAlign.center,
  //                       fontWeight: 600,
  //                     ),
  //                   ),
  //                   InkWell(
  //                     splashColor: Colors.transparent,
  //                     highlightColor: Colors.transparent,
  //                     onTap: () {
  //                       Get.back();
  //                       onSubmit();
  //                     },
  //                     child: SizedBox(
  //                       width: 100,
  //                       child: FxText.bodyMedium(
  //                         'Xác nhận',
  //                         color: Get.theme.colorScheme.primary,
  //                         textAlign: TextAlign.right,
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             FxSpacing.height(8),
  //             Expanded(
  //                 child: Container(
  //               margin: FxSpacing.only(bottom: 10, top: 0, right: 10, left: 10),
  //               child: CupertinoPicker(
  //                   useMagnifier: true,
  //                   itemExtent: 42,
  //                   onSelectedItemChanged: (value) {
  //                     this.inputValue = value;
  //                     // setState(() {});
  //                   },
  //                   children: List<Widget>.generate(
  //                       3,
  //                       (index) => Row(
  //                             mainAxisAlignment: MainAxisAlignment.center,
  //                             children: [
  //                               Expanded(
  //                                   child: Align(
  //                                       alignment: Alignment.centerRight,
  //                                       child: _genderIcon[index])),
  //                               Expanded(
  //                                   child: Align(
  //                                       alignment: Alignment.centerLeft,
  //                                       child: Text(_gender[index]))),
  //                             ],
  //                           ))),
  //             ))
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget createBody() {
    return userProfileController.obx((state) => (Column(children: [
          Expanded(
              child: (ListView(
            padding: FxSpacing.fromLTRB(
                20, FxSpacing.safeAreaTop(context) + 20, 20, 20),
            children: [
              FxContainer(
                child: Row(
                  children: [
                    FxContainer(
                      onTap: () {
                        _openCamera();
                      },
                      color: Colors.transparent,
                      paddingAll: 0,
                      borderRadiusAll: 4,
                      height: 100,
                      width: 100,
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          ClipRRect(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(60)),
                            child: buildCircleAvatar(state?.name ?? "",
                                avatarUrl: state?.avatarUrl, radius: 50),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: FxCard(
                              paddingAll: 2,
                              borderRadiusAll: 4,
                              clipBehavior: Clip.none,
                              child: FxContainer(
                                paddingAll: 4,
                                borderRadiusAll: 4,
                                color: Get.theme.colorScheme.primaryContainer,
                                child: Icon(
                                  Icons.camera_alt,
                                  size: 16,
                                  color: Get.theme.colorScheme.primary,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    FxSpacing.width(16),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FxText.bodyLarge(state?.name ?? "", fontWeight: 700),
                          FxSpacing.width(6),
                          FxText.bodyMedium(
                            state?.email ?? "",
                          ),
                          // FxSpacing.height(8),
                          // FxText.bodyMedium((state?.roles ?? []).contains("d")
                          //     ? "Giám đốc"
                          //     : "Trưởng phòng"),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              FxSpacing.height(24),
              _buildCategory("THÔNG TIN CÁ NHÂN"),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Tên đầy đủ",
                  value: Text(state?.userDetail?.fullName ?? state?.name ?? ""),
                  icon: FeatherIcons.user,
                  iconAction: FeatherIcons.chevronRight,
                  callback: () {
                    this.inputValue = null;
                    Get.bottomSheet(buildPickerText(
                        title: 'Tên đầy đủ',
                        value: state?.userDetail?.fullName ?? state?.name ?? "",
                        onSelect: (value) {
                          this.inputValue = value;
                        },
                        onSubmit: () {
                          if (this.inputValue != null) {
                            userProfileController.state?.userDetail?.fullName =
                                this.inputValue;
                          }
                        }));
                  }),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Tên hiển thị",
                  value: Text(state?.name ?? ""),
                  icon: FeatherIcons.user,
                  iconAction: FeatherIcons.chevronRight,
                  callback: () {
                    this.inputValue = null;
                    Get.bottomSheet(buildPickerText(
                        title: 'Tên hiển thị',
                        value: state?.name ?? "",
                        onSelect: (value) {
                          this.inputValue = value;
                        },
                        onSubmit: () {
                          if (this.inputValue != null) {
                            userProfileController.state?.name = this.inputValue;
                          }
                        }));
                  }),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Số điện thoại",
                  value: Text(state?.phoneNumber ?? ""),
                  icon: FeatherIcons.phone,
                  iconAction: FeatherIcons.chevronRight,
                  callback: () {
                    this.inputValue = null;
                    Get.bottomSheet(buildPickerText(
                        title: 'Số điện thoại',
                        value: state?.phoneNumber ?? "",
                        keyboardType: TextInputType.number,
                        onSelect: (value) {
                          this.inputValue = value;
                        },
                        onSubmit: () {
                          if (this.inputValue != null) {
                            userProfileController.state?.phoneNumber =
                                this.inputValue;
                          }
                        }));
                  }),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Giới tính",
                  value: Icon(state?.userDetail?.gender == 1
                      ? Icons.male
                      : Icons.female),
                  icon: FeatherIcons.atSign,
                  iconAction: FeatherIcons.chevronRight,
                  callback: () {
                    this.inputValue = 0;
                    Get.bottomSheet(
                      buildPickerGender(
                        title: 'Giới tính',
                        value: state?.userDetail?.gender,
                        onSelect: (value) {
                          this.inputValue = value;
                        },
                        onSubmit: () {
                          userProfileController.state?.userDetail?.gender =
                              this.inputValue;
                          setState(() {});
                        },
                      ),
                    );
                  }),
              FxSpacing.height(16),
              if (state?.userDetail?.birthdate != null)
                buildSettingItem(
                    title: "Ngày sinh",
                    value: Text(DateFormat('dd/MM/yyyy')
                        .format(DateTime.parse(state?.userDetail?.birthdate))),
                    // Text(state?.userDetail?.birthdate),
                    // Text(DateFormat("dd/MM/yyyy").format(DateTime.now())),
                    icon: FeatherIcons.calendar,
                    iconAction: FeatherIcons.chevronRight,
                    callback: () {
                      this.inputValue = null;
                      Get.bottomSheet(buildPickerDate(
                        title: 'Sửa: Ngày sinh',
                        value: DateTime.parse(state?.userDetail?.birthdate),
                        onSelect: (time) {
                          this.inputValue =
                              DateFormat("yyyy-MM-dd HH:mm:ss").format(time);
                        },
                        onSubmit: () {
                          if (this.inputValue != null) {
                            userProfileController.state?.userDetail?.birthdate =
                                this.inputValue;
                            setState(() {});
                          }
                        },
                      ));
                    }),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Mô tả",
                  value: Text(state?.userDetail?.aboutMe ?? ""),
                  icon: FeatherIcons.hash,
                  iconAction: FeatherIcons.chevronRight,
                  callback: () {
                    this.inputValue = null;
                    Get.bottomSheet(buildPickerText(
                        title: 'Sửa: Mô tả',
                        value: state?.userDetail?.aboutMe ?? "",
                        onSelect: (value) {
                          this.inputValue = value;
                        },
                        onSubmit: () {
                          userProfileController.state?.userDetail?.aboutMe =
                              this.inputValue;
                          print(this.inputValue);
                          setState(() {});
                        }));
                  }),
              FxSpacing.height(16),
              _buildCategory("CẤU HÌNH"),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Ngôn ngữ",
                  value: Text(Language.currentLanguage.languageName),
                  icon: FeatherIcons.globe,
                  callback: showLanguagePicker),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Chế độ nền tối",
                  value: Text(
                    Get.isDarkMode ? 'Bật'.tr : 'Tắt'.tr,
                  ),
                  icon: FeatherIcons.moon,
                  callback: changeTheme),
              FxSpacing.height(16),
              _buildCategory("THÔNG BÁO"),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Thông báo",
                  icon: FeatherIcons.bell,
                  callback: () => Get.toNamed(NotificationScreen.routeName)),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Trợ giúp", icon: FeatherIcons.helpCircle),
              FxSpacing.height(24),
              Container(
                alignment: Alignment.center,
                child: FxContainer(
                  onTap: () {
                    userProfileController.logout();
                  },
                  width: 150,
                  padding: FxSpacing.xy(20, 8),
                  borderRadiusAll: 4,
                  color: Get.theme.colorScheme.primary,
                  child: FxText.bodyMedium(
                    'Đăng xuất',
                    textAlign: TextAlign.center,
                    fontWeight: 700,
                    letterSpacing: 0.3,
                    color: Get.theme.colorScheme.onPrimary,
                  ),
                ),
              )
            ],
          )))
        ])));
  }

  @override
  AppBar? createAppBar() => null;

  @override
  Widget? createBottomBar() {
    return userProfileController.obx((state) =>
        baseBottomBar(ticker: userProfileController, indexHighlight: 3));
  }
}
