import 'dart:async';

import 'package:get/get.dart';
import 'package:serpapp/data/models/authentication/auth_model.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/data/repositories/file_management/file_repo.dart';
import 'package:serpapp/ui/screens/auth/login/login_screen.dart';

class UserProfileController extends GetxController
    with GetTickerProviderStateMixin, StateMixin<AuthModel> {
  final AuthenticationRepository authenticationRepository = Get.find();
  final FileRepository fileRepository = Get.find();
  @override
  Future<void> onInit() async {
    change(null, status: RxStatus.loading());
    await fetchData();
    super.onInit();
  }

  Future fetchData() async {
    final res = await authenticationRepository.getLocalUserData();
    if (res != null) {
      change(res, status: RxStatus.success());
    } else {
      change(null, status: RxStatus.error("User is null go back sign in"));
      await logout();
    }
  }

  Future<void> updateAvatar(String pickedAvatarPath) async {
    change(null, status: RxStatus.loading());
    try {
      if (pickedAvatarPath == '') {
        // return ActionModel(state: ActionState.Fail, message: successMsg.tr);
      }

      var fileModel = await fileRepository.uploadImage(pickedAvatarPath, '');
      if (fileModel.fileUrl == null) {
        // return ActionModel(
        //     state: ActionState.Fail, message: 'Cannot get file url.');
      }

      final userAvatarUrl = fileModel.fileUrl ?? '';
      final res = await authenticationRepository.updateUserProfile(
          avatarUrl: userAvatarUrl);
      if (res != null) {
        change(res, status: RxStatus.success());
      }
      // return ActionModel(state: ActionState.Success, message: successMsg.tr);
    } catch (e) {
      // return ActionModel(state: ActionState.Fail, message: e.toString());
    }
  }

  Future logout() async {
    await authenticationRepository.logout();
    Get.offAllNamed(LogInScreen.routeName);
  }
}
