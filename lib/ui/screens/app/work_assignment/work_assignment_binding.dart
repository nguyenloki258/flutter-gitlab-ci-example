import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_controller.dart';

class WorkAssignmentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WorkAssignmentController>(() => WorkAssignmentController());
  }
}
