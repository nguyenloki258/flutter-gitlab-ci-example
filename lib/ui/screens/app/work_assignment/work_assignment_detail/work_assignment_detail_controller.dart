import 'dart:async';
import 'dart:convert';

import 'package:core_packages/core_package.dart';
import 'package:data_module/data_module.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:serpapp/data/constant/constant.dart';
import 'package:serpapp/data/models/tree-management/project_apis/project_content_model.dart';
import 'package:serpapp/data/models/user/users_by_code_model.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/data/repositories/base-manager/base-category/base_category_repo.dart';
import 'package:serpapp/data/repositories/communication/comment_repo.dart';
import 'package:serpapp/data/repositories/file_management/file_repo.dart';
import 'package:serpapp/data/repositories/tree-management/project_repo.dart';
import 'package:serpapp/data/repositories/user/users_repo.dart';

class ProjectEditData {
  List<SelectOptionItem> leaderSelectedData = [];
  List<SelectOptionItem> groupSelectedData = [];
  SelectOptionItem? statusSelectedData;
  DateTime? fromDate;
  DateTime? toDate;
  DateTime? finishDate;
  String? description;
}

class ProjectLoadingState {
  bool loadingInputComment = false;
}

class WorkAssignmentDetailController extends GetxController
    with StateMixin, GetTickerProviderStateMixin {
  final BaseCategoryRepository baseCategoryRepository = Get.find();
  final UsersRepository usersRepository = Get.find();
  final ProjectRepository projectRepository = Get.find();
  final AuthenticationRepository authenticationRepository = Get.find();
  final EncryptedStorage encryptedStorage = Get.find();
  final FileRepository fileRepository = Get.find();
  final CommentRepository commentRepository = Get.find();

  dynamic projectData;
  ProjectEditData projectEditData = new ProjectEditData();
  ProjectLoadingState loadingState = new ProjectLoadingState();

  List<SelectOptionItem> statusData = [];
  List<SelectOptionItem> leaderData = [];
  List<SelectOptionItem> groupData = [];
  List fileData = [];
  List logData = [];
  List commentData = [];

  String commentMessage = "";
  SelectOptionItem activityState =
      new SelectOptionItem(key: "Bình luận", value: 1);
  List<SelectOptionItem> activityStateData = [
    new SelectOptionItem(key: "Bình luận", value: 1),
    new SelectOptionItem(key: "Lịch sử", value: 2)
  ];

  late AnimationController animationController;
  late Animation<double> fadeAnimation;

  @override
  Future<void> onInit() async {
    animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    fadeAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );
    animationController.forward();
    super.onInit();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    await Future.wait([
      fetchStatusData(),
      fetchGroupData(),
      fetchLeaderData(),
      fetchFile(),
      fetchComment(),
    ]);
    var currentStatus =
        statusData.where((element) => element.value == projectData.statusObj);
    if (currentStatus.length > 0) {
      projectEditData.statusSelectedData = currentStatus.toList()[0];
    }
    projectEditData.fromDate = projectData.fromDate;
    projectEditData.toDate = projectData.toDate;
    projectEditData.fromDate = projectData.fromDate;
    projectEditData.finishDate = projectData.finishDate;
    projectEditData.description = projectData.description;
    projectEditData.leaderSelectedData = leaderData
        .where((element) =>
            (projectData.managementObj ?? [])
                .where((y) => y == element.value)
                .length >
            0)
        .toList();
    var currentGroup = (projectData.metadataContentObjs as List)
        .where((x) => x.fieldName == "EcoparkGroup")
        .toList();

    if (currentGroup.length > 0) {
      var currentGroupObj = (jsonDecode(currentGroup[0].fieldValues) as List);
      projectEditData.groupSelectedData = groupData
          .where((element) =>
              (currentGroupObj).where((y) => y == element.value).length > 0)
          .toList();
    }

    change(null, status: RxStatus.success());
  }

  Future<ProjectContentModel> getProjectById(String id) async {
    projectData = await projectRepository.getProjectById(id);
    return projectData;
  }

  List usersAvatarUrl = [];
  getInfoLeader() async {
    usersAvatarUrl.length = 0;

    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }

    for (var i = 0; i < projectData.assigneeObj!.length; i++) {
      var currentUsers =
          users.where((x) => x.id == projectData.assigneeObj![i]);
      usersAvatarUrl.add(UsersByCodeModel(
          id: currentUsers.first.id!,
          name: currentUsers.first.name,
          avatarUrl: currentUsers.first.avatarUrl));
    }
    return usersAvatarUrl;
  }

  Future<List> fetchFile() async {
    fileData = await fileRepository.getFiles(projectData.id) as List;
    return fileData;
  }

  Future<List> fetchComment() async {
    commentData = await commentRepository.getComment(
        projectData.id, 1, 999, '', '', '') as List;
    // Get list user
    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }
    commentData.forEach((cmt) {
      cmt.message = _removeAllHtmlTags(cmt.message);

      var name =
          users.where((user) => user.id == cmt.createdByUserId).first.name;
      var avatarUrl =
          users.where((user) => user.id == cmt.createdByUserId).first.avatarUrl;

      cmt.name = name;
      cmt.avatarUrl = avatarUrl;
    });
    return commentData;
  }

  String _removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  Future<List> fetchStatusData() async {
    statusData = listStatus;
    return statusData;
  }

  Future<List> fetchLeaderData() async {
    var datas = await encryptedStorage.readDataObj("listLeader");
    if (datas != null) {
      return datas;
    }
    var res = await usersRepository.getUsersRolesByCode('TO_TRUONG');

    var response = res as List;
    leaderData = response
        .map((item) =>
            SelectOptionItem(key: item.name, value: item.id, data: item))
        .toList();
    encryptedStorage.writeDataObj("listLeader", leaderData);
    return leaderData;
  }

  Future<List> fetchGroupData() async {
    var datas = await encryptedStorage.readDataObj("listGroup");
    if (datas != null) {
      return datas;
    }
    var res = await baseCategoryRepository.getListCategoryGroups(
        'ecopark-group', 1, 10000, '{}');

    var response = res as List;
    groupData = response
        .map((item) =>
            SelectOptionItem(key: item.displayName, value: item.id, data: item))
        .toList();
    encryptedStorage.writeDataObj("listGroup", groupData);
    return groupData;
  }

  String getGroupName() {
    var groupName = "";
    var currentGroup = (projectData.metadataContentObjs as List)
        .where((x) => x.fieldName == "EcoparkGroupName")
        .toList();
    if (currentGroup.length > 0) {
      groupName = (jsonDecode(currentGroup[0].fieldValues) as List).join(", ");
    }
    return groupName;
  }

  String getPlanDate() {
    var metadata = projectData.metadataContentObjs as List;
    var planDate = metadata
        .where((element) => element.fieldName == 'PlanDate')
        .first
        .fieldValues;
    return planDate ?? "Trống";
  }

  changeUI() {
    change(null, status: RxStatus.success());
  }

  // Update mo ta
  Future<void> updateDescriptionProject(String id, String description) async {
    await projectRepository.updateDescriptionProjects(id, description);
  }

  // Update tổ trưởng
  Future<void> updateAssigneeObjProject(
      String id, List<dynamic> assigneeObj) async {
    List<String> assigneeId = [];
    assigneeObj.forEach((e) {
      assigneeId.add(e.value);
    });
    await projectRepository.updateAssigneeObjProjects(id, assigneeId.toList());
  }

  // Update trang thai
  Future<void> updateStatusProject(String id, statusObj) async {
    change(null, status: RxStatus.loading());
    await projectRepository.updateStateProject(id, statusObj);
    change(null, status: RxStatus.success());
    // await getProjectById(id);
  }

  // Update tổ quản lý
  Future<void> updateMetadataContentObjs(
      String id, List<dynamic> metadataContentObjs) async {
    var currentGroup = (projectData.metadataContentObjs as List);

    await projectRepository.updateMetadataContentObjs(id, currentGroup);
  }

  Future<void> postComment() async {
    loadingState.loadingInputComment = true;
    changeUI();
    var currentUser = await authenticationRepository.getLocalUserData();
    var newComment = await commentRepository.postComment(
        projectData.id, "PROJECT", null, commentMessage, 0, null);
    newComment.name = currentUser?.name ?? "";
    newComment.avatarUrl = currentUser?.avatarUrl;
    commentData.insert(0, newComment);
    loadingState.loadingInputComment = false;
    changeUI();
  }
}

class MetadataContentObjs {
  String? fieldName;
  dynamic fieldValues;
  MetadataContentObjs({
    this.fieldName,
    this.fieldValues,
  });
}
