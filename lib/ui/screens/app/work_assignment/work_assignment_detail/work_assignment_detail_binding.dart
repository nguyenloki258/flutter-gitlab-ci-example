import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_detail/work_assignment_detail_controller.dart';

class WorkAssignmentDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WorkAssignmentDetailController>(
        () => WorkAssignmentDetailController());
  }
}
