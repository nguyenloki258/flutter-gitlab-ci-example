import 'dart:convert';

import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutx/flutx.dart';
import 'package:intl/intl.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_detail/work_assignment_detail_controller.dart';
import 'package:core_packages/core_package.dart';
import 'package:serpapp/widgets/app/app_tag.dart';

class WorkAssignmentDetailScreen extends BaseScreen {
  static const String routeName = '/work_assignment_detail';

  const WorkAssignmentDetailScreen({Key? key}) : super(key: key);
  @override
  _WorkAssignmentDetailScreenState createState() =>
      _WorkAssignmentDetailScreenState();
}

class _WorkAssignmentDetailScreenState
    extends BaseState<WorkAssignmentDetailScreen> with BasicStateMixin {
  //late ThemeData theme;
  WorkAssignmentDetailController workAssignmentDetailController = Get.find();
  var arguments = Get.arguments;

  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
    _asyncMethod();
  }

  _asyncMethod() async {
    workAssignmentDetailController.projectData =
        await workAssignmentDetailController.getProjectById(arguments.id);
    await workAssignmentDetailController.fetchData();
    await workAssignmentDetailController.getInfoLeader();
  }

  Future<void> _onRefresh() async {
    workAssignmentDetailController.fetchData();
    setState(() {});
  }

  Widget _buildLogs() {
    if (workAssignmentDetailController.logData.isEmpty) {
      return Column(
        children: [
          FxSpacing.height(30),
          FxText.bodyMedium("Không có lịch sử"),
          FxSpacing.height(6),
          Icon(
            FeatherIcons.cloudOff,
            size: 20,
          )
        ],
      );
    }
    return Container(
      child: Column(children: [
        FxSpacing.height(16),
        for (var element in workAssignmentDetailController.logData)
          buildLog(
              avatar: element["avatarUrl"],
              detailAction: element["message"],
              name: element["name"],
              action: element["name"],
              time: DateFormat('hh:mm,dd/MM/yyyy')
                  .format(element["createdOnDate"] ?? DateTime.now()))
      ]),
    );
  }

  Widget _buildComments() {
    if (workAssignmentDetailController.commentData.isEmpty) {
      return Column(
        children: [
          FxSpacing.height(30),
          FxText.bodyMedium("Không có bình luận"),
          FxSpacing.height(6),
          Icon(
            FeatherIcons.cloudOff,
            size: 20,
          )
        ],
      );
    }
    return Container(
      child: Column(children: [
        for (var element in workAssignmentDetailController.commentData)
          buildComment(
              avatar: element.avatarUrl,
              contentComment: element.message.toString(),
              name: element.name,
              time:
                  DateFormat('hh:mm,dd/MM/yyyy').format(element.createdOnDate))
      ]),
    );
  }

  Widget _buildActivity() {
    return Container(
      padding: FxSpacing.left(6),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: FxText.bodyMedium("Hoạt động"),
              ),
              InkWell(
                  onTap: () {
                    Get.bottomSheet(buildPickerSingleChoice(
                        title: "Chọn hoạt động",
                        value: workAssignmentDetailController.activityState,
                        options:
                            workAssignmentDetailController.activityStateData,
                        onSelect: (p0) {
                          workAssignmentDetailController.activityState = p0;
                          workAssignmentDetailController.changeUI();
                        },
                        onSubmit: () {}));
                  },
                  child: Row(
                    children: [
                      FxText.bodySmall(
                          workAssignmentDetailController.activityState.key),
                      Icon(FeatherIcons.chevronDown,
                          size: 20,
                          color:
                              Get.theme.colorScheme.onBackground.withAlpha(100))
                    ],
                  ))
            ],
          ),
          workAssignmentDetailController.activityState.value == 1
              ? _buildComments()
              : _buildLogs()
        ],
      ),
    );
  }

  @override
  Widget createBody() {
    return workAssignmentDetailController.obx(
      (state) => (RefreshIndicator(
        onRefresh: _onRefresh,
        color: Colors.white,
        backgroundColor: Get.theme.colorScheme.primary,
        child: Column(
          children: [
            Expanded(
              child: (ListView(
                padding: FxSpacing.fromLTRB(20, 0, 20, 20),
                children: [
                  FxText.titleLarge(
                    workAssignmentDetailController.projectData!.name,
                    textAlign: TextAlign.center,
                  ),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Trạng thái",
                      value: appTag(
                          status: workAssignmentDetailController
                              .projectData.statusObj),
                      icon: FeatherIcons.wifi,
                      callback: () => Get.bottomSheet(buildPickerSingleChoice(
                            title: 'Chọn trạng thái',
                            value: workAssignmentDetailController
                                .projectEditData.statusSelectedData,
                            options: workAssignmentDetailController.statusData,
                            onSelect: (p0) {
                              workAssignmentDetailController
                                  .projectEditData.statusSelectedData = p0;
                              workAssignmentDetailController.changeUI();
                            },
                            onSubmit: () {
                              workAssignmentDetailController
                                  .updateStatusProject(
                                      workAssignmentDetailController
                                          .projectData.id,
                                      workAssignmentDetailController
                                          .projectEditData.statusSelectedData);
                              workAssignmentDetailController
                                      .projectData.statusObj =
                                  workAssignmentDetailController.projectEditData
                                      .statusSelectedData?.value;
                              workAssignmentDetailController.changeUI();
                            },
                            buildOption: (p0) {
                              return appTag(
                                  status: p0.value, isSelectedItem: true);
                            },
                          ))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Tổ trưởng",
                      value: Row(
                        children: [
                          buildAvatarList(
                              workAssignmentDetailController.usersAvatarUrl),
                        ],
                      ),
                      icon: FeatherIcons.user,
                      iconAction: FeatherIcons.userPlus,
                      callback: () => Get.bottomSheet(
                          workAssignmentDetailController
                              .obx((state) => buildPickerMultiChoice(
                                    title: 'Chọn tổ trưởng',
                                    value: workAssignmentDetailController
                                        .projectEditData.leaderSelectedData,
                                    options: workAssignmentDetailController
                                        .leaderData,
                                    onSelect: (p0) {
                                      workAssignmentDetailController
                                          .projectEditData.leaderSelectedData
                                          .add(p0);

                                      workAssignmentDetailController.changeUI();
                                    },
                                    onRemove: (p0) {
                                      workAssignmentDetailController
                                          .projectEditData.leaderSelectedData
                                          .remove(p0);
                                      workAssignmentDetailController.changeUI();
                                    },
                                    onSubmit: () {
                                      workAssignmentDetailController
                                          .updateAssigneeObjProject(
                                              workAssignmentDetailController
                                                  .projectData.id,
                                              workAssignmentDetailController
                                                  .projectEditData
                                                  .leaderSelectedData);
                                      workAssignmentDetailController.changeUI();
                                    },
                                    buildOption: (p0) {
                                      return buildCircleAvatar(p0.data?.name,
                                          radius: 14,
                                          avatarUrl: p0.data?.avatarUrl);
                                    },
                                  )))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Tổ quản lý",
                      value: FxText.bodyMedium(
                          workAssignmentDetailController.getGroupName()),
                      icon: FeatherIcons.users,
                      // iconAction: FeatherIcons.edit3,
                      callback: () => Get.bottomSheet(buildPickerMultiChoice(
                            title: 'Chọn tổ quản lý',
                            value: workAssignmentDetailController
                                .projectEditData.groupSelectedData,
                            options: workAssignmentDetailController.groupData,
                            onSelect: (p0) {
                              workAssignmentDetailController
                                  .projectEditData.groupSelectedData
                                  .add(p0);

                              workAssignmentDetailController.changeUI();
                            },
                            onRemove: (p0) {
                              workAssignmentDetailController
                                  .projectEditData.groupSelectedData
                                  .remove(p0);
                              workAssignmentDetailController.changeUI();
                            },
                            onSubmit: () {
                              workAssignmentDetailController
                                  .updateMetadataContentObjs(
                                      workAssignmentDetailController
                                          .projectData.id,
                                      workAssignmentDetailController
                                          .projectEditData.groupSelectedData);
                              var groupNameData = workAssignmentDetailController
                                  .projectEditData.groupSelectedData
                                  .map((e) => e.key);
                              for (var item in (workAssignmentDetailController
                                  .projectData.metadataContentObjs as List)) {
                                if (item.fieldName == "EcoparkGroupName") {
                                  item.fieldValues = jsonEncode(groupNameData);
                                }
                              }
                              workAssignmentDetailController.changeUI();
                            },
                          ))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Ngày BĐ kế hoạch",
                      value: FxText.bodyMedium(
                          workAssignmentDetailController.projectData.fromDate !=
                                  null
                              ? DateFormat("dd/MM/yyyy").format(
                                  workAssignmentDetailController
                                      .projectData.fromDate)
                              : "Trống"),
                      icon: FeatherIcons.calendar,
                      // iconAction: FeatherIcons.edit3,
                      callback: () => Get.bottomSheet(buildPickerDate(
                          title: 'Ngày BĐ kế hoạch',
                          value: workAssignmentDetailController
                              .projectData.fromDate,
                          onSelect: (p0) {
                            workAssignmentDetailController
                                .projectEditData.fromDate = p0;
                            workAssignmentDetailController.changeUI();
                          },
                          onSubmit: () {
                            // ignore: todo
                            //TODO:
                            workAssignmentDetailController
                                    .projectData.fromDate =
                                workAssignmentDetailController
                                    .projectEditData.fromDate;
                            workAssignmentDetailController.changeUI();
                          }))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Ngày HT kế hoạch",
                      value: FxText.bodyMedium(workAssignmentDetailController
                                  .projectData.toDate !=
                              null
                          ? DateFormat("dd/MM/yyyy").format(
                              workAssignmentDetailController.projectData.toDate)
                          : "Trống"),
                      icon: FeatherIcons.calendar,
                      // iconAction: FeatherIcons.edit3,
                      callback: () => Get.bottomSheet(buildPickerDate(
                          title: 'Ngày HT kế hoạch',
                          value:
                              workAssignmentDetailController.projectData.toDate,
                          onSelect: (p0) {
                            workAssignmentDetailController
                                .projectEditData.toDate = p0;
                            workAssignmentDetailController.changeUI();
                          },
                          onSubmit: () {
                            // ignore: todo
                            //TODO:
                            workAssignmentDetailController.projectData.toDate =
                                workAssignmentDetailController
                                    .projectEditData.toDate;
                            workAssignmentDetailController.changeUI();
                          }))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Ngày BĐ thực tế",
                      value: FxText.bodyMedium(
                          workAssignmentDetailController.getPlanDate()),
                      icon: FeatherIcons.calendar,
                      // iconAction: FeatherIcons.edit3,
                      callback: () => Get.bottomSheet(buildPickerDate(
                          title: 'Ngày BĐ thực tế',
                          value: workAssignmentDetailController
                              .projectData.finishDate,
                          onSelect: (p0) {
                            workAssignmentDetailController
                                .projectEditData.finishDate = p0;
                            workAssignmentDetailController.changeUI();
                          },
                          onSubmit: () {
                            // ignore: todo
                            //TODO:
                            workAssignmentDetailController
                                    .projectData.finishDate =
                                workAssignmentDetailController
                                    .projectEditData.finishDate;
                            workAssignmentDetailController.changeUI();
                          }))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Ngày HT thực tế",
                      value: FxText.bodyMedium(workAssignmentDetailController
                                  .projectData.finishDate !=
                              null
                          ? DateFormat("dd/MM/yyyy").format(
                              workAssignmentDetailController
                                  .projectData.finishDate)
                          : "Trống"),
                      icon: FeatherIcons.calendar,
                      // iconAction: FeatherIcons.edit3,
                      callback: () => Get.bottomSheet(buildPickerDate(
                          title: 'Ngày HT thực tế',
                          value: workAssignmentDetailController
                              .projectData.finishDate,
                          onSelect: (p0) {
                            workAssignmentDetailController
                                .projectEditData.finishDate = p0;
                            workAssignmentDetailController.changeUI();
                          },
                          onSubmit: () {
                            // ignore: todo
                            //TODO:
                            workAssignmentDetailController
                                    .projectData.finishDate =
                                workAssignmentDetailController
                                    .projectEditData.finishDate;
                            workAssignmentDetailController.changeUI();
                          }))),
                  FxSpacing.height(20),
                  buildSettingItem(
                      title: "Mô tả",
                      longValue: Expanded(
                          child: FxText.bodyMedium(
                              workAssignmentDetailController
                                      .projectData.description ??
                                  "")),
                      icon: FeatherIcons.hash,
                      // iconAction: FeatherIcons.edit3,
                      callback: () => Get.bottomSheet(buildPickerText(
                          title: 'Nhập mô tả',
                          value: workAssignmentDetailController
                              .projectData.description,
                          onSelect: (p0) {
                            workAssignmentDetailController
                                .projectEditData.description = p0;
                            workAssignmentDetailController.changeUI();
                          },
                          onSubmit: () {
                            // Update Description API
                            workAssignmentDetailController
                                .updateDescriptionProject(
                                    workAssignmentDetailController
                                        .projectData.id,
                                    workAssignmentDetailController
                                            .projectEditData.description ??
                                        '');
                            // Update UI
                            workAssignmentDetailController
                                    .projectData.description =
                                workAssignmentDetailController
                                    .projectEditData.description;
                            workAssignmentDetailController.changeUI();
                          }))),
                  FxSpacing.height(16),
                  buildFileAttachment(workAssignmentDetailController.fileData),
                  FxSpacing.height(16),
                  _buildActivity()
                ],
              )),
            )
          ],
        ),
      )),
    );
  }

  @override
  Widget? createBottomBar() {
    return workAssignmentDetailController
        .obx((state) => workAssignmentDetailController.activityState.value == 1
            ? FxContainer(
                marginAll: 24,
                paddingAll: 0,
                child: FxTextField(
                  controller: TextEditingController(
                      text: workAssignmentDetailController.commentMessage),
                  readOnly: workAssignmentDetailController
                      .loadingState.loadingInputComment,
                  onChanged: (value) =>
                      workAssignmentDetailController.commentMessage = value,
                  focusedBorderColor: Get.theme.colorScheme.primary,
                  cursorColor: Get.theme.colorScheme.primary,
                  textFieldStyle: FxTextFieldStyle.outlined,
                  labelText: 'Thêm bình luận ...',
                  labelStyle: FxTextStyle.bodyMedium(
                      color: Get.theme.colorScheme.onBackground, xMuted: true),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  filled: true,
                  fillColor: Get.theme.colorScheme.primary.withAlpha(50),
                  style: TextStyle(fontSize: 16),
                  suffixIcon: InkWell(
                      onTap: () => workAssignmentDetailController.postComment(),
                      child: workAssignmentDetailController
                              .loadingState.loadingInputComment
                          ? CircularProgressIndicator()
                          : Icon(
                              FeatherIcons.send,
                              color: Get.theme.colorScheme.primary,
                              size: 20,
                            )),
                ),
              )
            : SizedBox());
  }

  @override
  AppBar? createAppBar() => buildDefaultAppBar(
        title: "Chi tiết dự án",
      );
}
