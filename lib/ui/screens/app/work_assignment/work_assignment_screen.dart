import 'package:core_packages/core_package.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:intl/intl.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/app/base_bottom_bar.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_controller.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_screen.dart';
import 'package:serpapp/utils/format_datetime.dart';
import 'package:serpapp/widgets/app/app_priority.dart';
import 'package:serpapp/widgets/app/app_tag.dart';

class WorkAssignmentScreen extends BaseScreen {
  static const String routeName = '/work_assignment';

  const WorkAssignmentScreen({Key? key}) : super(key: key);
  @override
  _WorkAssignmentScreenState createState() => _WorkAssignmentScreenState();
}

class _WorkAssignmentScreenState extends BaseState<WorkAssignmentScreen>
    with BasicStateMixin {
  //late ThemeData theme;
  WorkAssignmentController workAssignmentController = Get.find();

  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
    workAssignmentController.fetchData();
  }

  Widget _buildSearchMenu() {
    return Column(children: [
      Row(
        children: [
          Expanded(
            child: TextFormField(
              onChanged: (value) {
                workAssignmentController.filterProject.searchText = value;
                workAssignmentController.changeUI();
              },
              style: FxTextStyle.bodyMedium(),
              cursorColor: Get.theme.colorScheme.primary,
              decoration: InputDecoration(
                hintText: "Tìm kiếm phân công công việc ...",
                hintStyle: FxTextStyle.bodySmall(
                    color: Get.theme.colorScheme.onBackground),
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                    borderSide: BorderSide.none),
                enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                    borderSide: BorderSide.none),
                focusedBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: Get.theme.cardTheme.color,
                prefixIcon: Icon(
                  FeatherIcons.search,
                  color: Get.theme.colorScheme.onBackground.withAlpha(150),
                ),
                isDense: true,
              ),
              textCapitalization: TextCapitalization.sentences,
            ),
          ),
          FxSpacing.width(6),
          FxContainer(
            paddingAll: 12,
            borderRadiusAll: 4,
            onTap: () {
              Get.bottomSheet(_buildFilter(),
                  isScrollControlled: true, isDismissible: false);
            },
            color: Get.theme.colorScheme.primary,
            child: Icon(
              FeatherIcons.sliders,
              color: Get.theme.colorScheme.onPrimary,
              size: 20,
            ),
          ),
        ],
      ),
      FxSpacing.height(4),
      Row(
        children: [
          Expanded(
              child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      buildQuickSelectOptions(
                          title: "Trạng thái",
                          selectedOptions:
                              workAssignmentController.filterProject.status,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      FxSpacing.width(10),
                      buildQuickSelectOptions(
                          title: "Tổ trưởng",
                          selectedOptions:
                              workAssignmentController.filterProject.leader,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      FxSpacing.width(10),
                      buildQuickSelectOptions(
                          title: "Tổ",
                          selectedOptions:
                              workAssignmentController.filterProject.group,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      FxSpacing.width(10),
                      buildQuickDateRangeOptions(
                          title: "Ngày bắt đầu",
                          dateTime:
                              workAssignmentController.filterProject.startDate,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      FxSpacing.width(10),
                      buildQuickDateRangeOptions(
                          title: "Ngày HT hợp đồng",
                          dateTime:
                              workAssignmentController.filterProject.endDate,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                    ],
                  ))),
        ],
      ),
    ]);
  }

  Widget _buildFilter() {
    return workAssignmentController.obx((state) => Container(
          height: Get.height - 50,
          margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8), bottom: Radius.circular(0)),
            color: Get.theme.scaffoldBackgroundColor,
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Drawer(
            child: Container(
              color: Get.theme.scaffoldBackgroundColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: FxSpacing.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        FxCard.rounded(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          paddingAll: 6,
                          child: Icon(
                            FeatherIcons.x,
                            color: Get.theme.colorScheme.onBackground,
                          ),
                        ),
                        FxText.bodyMedium(
                          'Tìm kiếm công việc',
                          fontWeight: 600,
                        ),
                        InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () {
                            workAssignmentController.resetFilter();
                          },
                          child: FxText.bodySmall(
                            'Đặt lại',
                          ),
                        )
                      ],
                    ),
                  ),
                  FxSpacing.height(8),
                  Expanded(
                      child: ListView(
                    padding: FxSpacing.all(20),
                    children: [
                      buildSelectOptions(
                          "Trạng thái",
                          workAssignmentController.statusData,
                          workAssignmentController.filterProject.status,
                          (p0) =>
                              workAssignmentController.addChoice(p0, "Status"),
                          (p0) => workAssignmentController.removeChoice(
                              p0, "Status"),
                          (p0) => SizedBox()),
                      FxSpacing.height(8),
                      buildSelectOptions(
                          "Tổ trưởng",
                          workAssignmentController.leaderData,
                          workAssignmentController.filterProject.leader,
                          (p0) =>
                              workAssignmentController.addChoice(p0, "Leader"),
                          (p0) => workAssignmentController.removeChoice(
                              p0, "Leader"),
                          (p0) => Row(children: [
                                buildCircleAvatar(p0.data?.name,
                                    radius: 14, avatarUrl: p0.data?.avatarUrl),
                                FxSpacing.width(6),
                              ])),
                      FxSpacing.height(8),
                      buildSelectOptions(
                          "Tổ",
                          workAssignmentController.groupData,
                          workAssignmentController.filterProject.group,
                          (p0) =>
                              workAssignmentController.addChoice(p0, "Group"),
                          (p0) => workAssignmentController.removeChoice(
                              p0, "Group"),
                          (p0) => SizedBox()),
                      FxSpacing.height(8),
                      _buildDateRangeOptions(
                          "Ngày bắt đầu",
                          workAssignmentController.filterProject.startDate,
                          (p0) => workAssignmentController.setDateRange(
                              p0, "StartDate")),
                      FxSpacing.height(8),
                      _buildDateRangeOptions(
                          "Hạn chót",
                          workAssignmentController.filterProject.endDate,
                          (p0) => workAssignmentController.setDateRange(
                              p0, "EndDate")),
                    ],
                  )),
                  FxSpacing.height(8),
                  Padding(
                    padding: FxSpacing.horizontal(24),
                    child: FxButton.block(
                      borderRadiusAll: 8,
                      onPressed: () {
                        workAssignmentController.changeUI();
                        Navigator.pop(context);
                      },
                      backgroundColor: Get.theme.colorScheme.primary,
                      child: FxText.titleSmall(
                        "Tìm kiếm",
                        fontWeight: 700,
                        color: Get.theme.colorScheme.onPrimary,
                        letterSpacing: 0.4,
                      ),
                    ),
                  ),
                  FxSpacing.height(8),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _buildDateRangeOptions(String title, DateTimeRange? dateTime,
      Function(DateTimeRange?) onChange) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            FxText.bodyMedium(
              title,
              color: Get.theme.colorScheme.onBackground,
              fontWeight: 600,
            ),
          ],
        ),
        FxSpacing.height(8),
        Row(children: [
          FxContainer.none(
            color: dateTime != null
                ? Get.theme.colorScheme.primary.withAlpha(28)
                : Get.theme.cardTheme.color,
            borderRadiusAll: 20,
            padding: FxSpacing.xy(12, 8),
            onTap: () {
              workAssignmentController.currentStep = 0;
              if (dateTime != null) {
                workAssignmentController.startDate = dateTime.start;
                workAssignmentController.endDate = dateTime.end;
              } else {
                workAssignmentController.startDate = null;
                workAssignmentController.endDate = null;
              }
              workAssignmentController.changeUI();

              Get.bottomSheet(
                  workAssignmentController.obx((state) => Container(
                        height: 500,
                        margin: FxSpacing.only(
                            bottom: 0, top: 0, right: 0, left: 0),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(8),
                              bottom: Radius.circular(0)),
                          color: Get.theme.scaffoldBackgroundColor,
                        ),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Drawer(
                          child: Container(
                            color: Get.theme.scaffoldBackgroundColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: FxSpacing.all(12),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      FxCard.rounded(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        paddingAll: 6,
                                        child: Icon(
                                          FeatherIcons.x,
                                          color: Get
                                              .theme.colorScheme.onBackground,
                                        ),
                                      ),
                                      FxText.bodyMedium(
                                        'Chọn: ' + title,
                                        fontWeight: 600,
                                      ),
                                      InkWell(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        onTap: () {
                                          onChange(null);
                                          Navigator.pop(context);
                                        },
                                        child: FxText.bodySmall(
                                          'Xoá',
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                FxSpacing.height(8),
                                Expanded(
                                    child: ListView(
                                  padding: FxSpacing.all(20),
                                  children: [
                                    Stepper(
                                        controlsBuilder: (BuildContext context,
                                            ControlsDetails details) {
                                          return SizedBox();
                                        },
                                        currentStep: workAssignmentController
                                            .currentStep,
                                        onStepTapped: (pos) {
                                          if (pos == 1) {
                                            workAssignmentController.endDate =
                                                workAssignmentController
                                                    .startDate
                                                    ?.add(Duration(days: 1));
                                          } else {
                                            workAssignmentController.startDate =
                                                workAssignmentController.endDate
                                                    ?.subtract(
                                                        Duration(days: 1));
                                          }
                                          workAssignmentController.currentStep =
                                              pos;

                                          workAssignmentController.changeUI();
                                        },
                                        steps: <Step>[
                                          Step(
                                            isActive: true,
                                            title: FxText.bodyMedium(
                                                'Từ ngày: ' +
                                                    (workAssignmentController
                                                                .startDate !=
                                                            null
                                                        ? DateFormat(
                                                                "dd/MM/yyyy")
                                                            .format(workAssignmentController
                                                                    .startDate ??
                                                                DateTime.now())
                                                        : ""),
                                                fontWeight: 600),
                                            content: workAssignmentController
                                                        .currentStep ==
                                                    0
                                                ? Container(
                                                    height: 150,
                                                    child: CupertinoDatePicker(
                                                        initialDateTime:
                                                            workAssignmentController
                                                                .startDate,
                                                        maximumDate:
                                                            workAssignmentController
                                                                .endDate,
                                                        mode:
                                                            CupertinoDatePickerMode
                                                                .date,
                                                        onDateTimeChanged:
                                                            (dateTime) {
                                                          workAssignmentController
                                                                  .startDate =
                                                              dateTime;
                                                          workAssignmentController
                                                              .changeUI();
                                                        }),
                                                  )
                                                : SizedBox(),
                                          ),
                                          Step(
                                            isActive: true,
                                            title: FxText.bodyMedium(
                                                'Đến ngày: ' +
                                                    (workAssignmentController
                                                                .endDate !=
                                                            null
                                                        ? DateFormat(
                                                                "dd/MM/yyyy")
                                                            .format(
                                                                workAssignmentController
                                                                        .endDate ??
                                                                    DateTime
                                                                        .now())
                                                        : ""),
                                                fontWeight: 600),
                                            content: workAssignmentController
                                                        .currentStep ==
                                                    1
                                                ? Container(
                                                    height: 150,
                                                    child: CupertinoDatePicker(
                                                        initialDateTime:
                                                            workAssignmentController
                                                                .endDate,
                                                        minimumDate:
                                                            workAssignmentController
                                                                .startDate,
                                                        mode:
                                                            CupertinoDatePickerMode
                                                                .date,
                                                        onDateTimeChanged:
                                                            (dateTime) {
                                                          workAssignmentController
                                                                  .endDate =
                                                              dateTime;
                                                          workAssignmentController
                                                              .changeUI();
                                                        }),
                                                  )
                                                : SizedBox(),
                                          ),
                                        ]),
                                  ],
                                )),
                                FxSpacing.height(16),
                                Padding(
                                  padding: FxSpacing.horizontal(24),
                                  child: FxButton.block(
                                    borderRadiusAll: 8,
                                    onPressed: () {
                                      onChange(new DateTimeRange(
                                          start: workAssignmentController
                                                  .startDate ??
                                              DateTime.now(),
                                          end: workAssignmentController
                                                  .endDate ??
                                              DateTime.now()));
                                      Navigator.pop(context);
                                    },
                                    backgroundColor:
                                        Get.theme.colorScheme.primary,
                                    child: FxText.titleSmall(
                                      "Xác nhận",
                                      fontWeight: 700,
                                      color: Get.theme.colorScheme.onPrimary,
                                      letterSpacing: 0.4,
                                    ),
                                  ),
                                ),
                                FxSpacing.height(8),
                              ],
                            ),
                          ),
                        ),
                      )),
                  isScrollControlled: true,
                  isDismissible: false);
            },
            child: FxText.bodyMedium(
              dateTime == null
                  ? "Từ ngày - Đến Ngày"
                  : DateFormat("dd/MM/yyyy").format(dateTime.start) +
                      " - " +
                      DateFormat("dd/MM/yyyy").format(dateTime.end),
              color: dateTime == null
                  ? Get.theme.colorScheme.onBackground
                  : Get.theme.colorScheme.primary,
            ),
          ),
        ]),
      ],
    );
  }

  Future<void> _onRefresh() async {
    workAssignmentController.fetchData();
    setState(() {});
  }

  @override
  Widget createBody() {
    return workAssignmentController.obx((state) => RefreshIndicator(
          onRefresh: _onRefresh,
          color: Colors.white,
          backgroundColor: Get.theme.colorScheme.primary,
          child: Column(children: [
            Expanded(
                child: (ListView(
              padding: FxSpacing.fromLTRB(20, 20, 20, 20),
              children: [
                _buildSearchMenu(),
                FxSpacing.height(20),
                SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildProjectGroupList(
                          workAssignmentController.listProjectTree),
                    ],
                  ),
                )
              ],
            )))
          ]),
        ));
  }

  @override
  Widget? createBottomBar() {
    return baseBottomBar(ticker: workAssignmentController, indexHighlight: 1);
  }

  @override
  AppBar? createAppBar() => null;

  Widget _buildProjectGroupList(List data) {
    return Column(
      children: [
        for (var item in data)
          item.children.length != 0
              ? _buildProjectGroupItem(
                  item.name, workAssignmentController.filterData(item.children))
              : _buildProjectOnly(),
      ],
    );
  }

  Widget _buildProjectOnly() {
    return Column(
      children: [
        for (var project in workAssignmentController.listProjectOnly)
          _buildProjectItem(project)
      ],
    );
  }

  Widget _buildProjectGroupItem(title, List dataChildren) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
          child: ExpansionTile(
            collapsedBackgroundColor: Get.theme.cardColor,
            controlAffinity: ListTileControlAffinity.leading,
            tilePadding: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 5),
            childrenPadding:
                FxSpacing.only(bottom: 5, top: 5, right: 5, left: 5),
            title: FxText.labelLarge(
              title,
              color: Get.theme.colorScheme.primary,
              fontWeight: 700,
            ),
            children: [
              for (var project in dataChildren) _buildProjectItem(project)
            ],
          ),
        ),
        FxSpacing.height(8),
      ],
    );
  }

  Widget _buildProjectItem(project) {
    return FadeTransition(
      opacity: workAssignmentController.fadeAnimation,
      child: FxContainer(
        onTap: () {
          Get.toNamed(WorkTrackingScreen.routeName, arguments: project);
        },
        borderRadiusAll: 4,
        paddingAll: 8,
        margin: FxSpacing.only(bottom: 10, top: 0, right: 0, left: 0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Hero(
                        tag: "project_" + project.name,
                        child: FxText.bodyMedium(
                          project.name,
                          color: Get.theme.colorScheme.onBackground,
                          fontWeight: 600,
                        ),
                      ),
                      appTag(status: project.statusObj)
                    ],
                  ),
                  FxSpacing.height(4),
                  Row(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            Icon(
                              FeatherIcons.calendar,
                              color: Get.theme.colorScheme.onBackground
                                  .withAlpha(80),
                              size: 14,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            FxText.labelMedium(
                                project.toDate != null
                                    ? getFormatedDate(project.toDate.toString())
                                    : 'Hạn chót',
                                color: Get.theme.colorScheme.onBackground,
                                letterSpacing: 0,
                                muted: true),
                            SizedBox(
                              width: 8,
                            ),
                            if (project.avatarUrl != null)
                              buildAvatarList(project.avatarUrl)
                          ],
                        ),
                      ),
                      appPriority(priority: project.priorityObj)
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
