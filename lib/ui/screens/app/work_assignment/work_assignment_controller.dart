import 'dart:async';

import 'package:core_packages/core_package.dart';
import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:serpapp/data/constant/constant.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/data/repositories/base-manager/base-category/base_category_repo.dart';
import 'package:serpapp/data/repositories/tree-management/project_repo.dart';
import 'package:serpapp/data/repositories/user/users_repo.dart';

class FilterProject {
  String searchText = "";
  DateTimeRange? startDate;
  DateTimeRange? endDate;
  List<SelectOptionItem> status = [];
  List<SelectOptionItem> leader = [];
  List<SelectOptionItem> group = [];
}

class WorkAssignmentController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  final BaseCategoryRepository baseCategoryRepository = Get.find();
  final UsersRepository usersRepository = Get.find();
  final ProjectRepository projectRepository = Get.find();
  final AuthenticationRepository authenticationRepository = Get.find();
  final EncryptedStorage encryptedStorage = Get.find();

  List<SelectOptionItem> groupData = [];
  List<SelectOptionItem> leaderData = [];
  List<SelectOptionItem> statusData = [];

  List listProjectTree = [];
  FilterProject filterProject = new FilterProject();

  List listProjectOnly = [];

  late AnimationController animationController;
  late Animation<double> fadeAnimation;

  DateTime? startDate = null;
  DateTime? endDate = null;
  int currentStep = 0;

  @override
  Future<void> onInit() async {
    animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    fadeAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );
    animationController.forward();
    super.onInit();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    await Future.wait([
      fetchListData(),
      fetchGroupData(),
      fetchLeaderData(),
      fetchStatusData(),
    ]);
    change(null, status: RxStatus.success());
  }

  String removeDiacritics(String str) {
    var withDia =
        'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var withoutDia =
        'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

    for (int i = 0; i < withDia.length; i++) {
      str = str.replaceAll(withDia[i], withoutDia[i]);
    }

    return str;
  }

  List filterData(list) {
    return list.where((element) {
      if (filterProject.searchText != "") {
        if (!removeDiacritics(element.name as String).toLowerCase().contains(
            removeDiacritics(filterProject.searchText).toLowerCase())) {
          return false;
        }
      }
      if (filterProject.status.length > 0) {
        if (filterProject.status
            .where((x) => x.value == element.statusObj)
            .isEmpty) {
          return false;
        }
      }
      if (filterProject.leader.length > 0) {
        if (filterProject.leader
            .where((x) =>
                (element.assigneeObj as List)
                    .where((y) => y == x.value)
                    .length >
                0)
            .isEmpty) {
          return false;
        }
      }

      if (filterProject.startDate != null) {
        if (element.fromDate == null) {
          return false;
        }
        var startDate = filterProject.startDate ??
            DateTimeRange(start: DateTime.now(), end: DateTime.now());
        if (startDate.start.isAfter(element.fromDate) ||
            startDate.end.isBefore(element.fromDate)) {}
        return false;
      }
      if (filterProject.endDate != null) {
        if (element.toDate == null) {
          return false;
        }
        var endDate = filterProject.endDate ??
            DateTimeRange(start: DateTime.now(), end: DateTime.now());
        if (endDate.start.isAfter(element.toDate) ||
            endDate.end.isBefore(element.toDate)) {}
        return false;
      }
      if (filterProject.group.length > 0) {
        var currentGroup = (element.metadataContentObjs as List)
            .where((x) => x.fieldName == "EcoparkGroup")
            .toList();
        if (currentGroup.length == 0) {
          return false;
        } else {
          if (filterProject.group
              .where((y) =>
                  (currentGroup[0].fieldValues as String).contains(y.value))
              .isEmpty) {
            return false;
          }
        }
      }
      return true;
    }).toList();
  }

  Future<List> fetchListData() async {
    // Get data
    var datas = await encryptedStorage.readDataObj("listProjectTree");
    if (datas != null) {
      listProjectTree = datas;
      return datas;
    }
    var res = await projectRepository.getAll(1, 10000, '{}');
    var response = res as List;

    // List project only
    listProjectOnly = response;

    // Get list user
    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }

    // add avt url to children
    for (var e in response) {
      List<String> assigneeObj = e.assigneeObj ?? [];
      e.avatarUrl =
          users.where((element) => assigneeObj.contains(element.id)).toList();
    }
    listProjectTree =
        response.where((element) => element.parentId == null).toList();

    var childrenListProjectTree = [];
    for (var parent in listProjectTree) {
      childrenListProjectTree = response
          .where(
              (child) => child.parentId != null && child.parentId == parent.id)
          .toList();
      parent.children = childrenListProjectTree;
    }

    encryptedStorage.writeDataObj("listProjectTree", listProjectTree);

    return listProjectTree;
  }

  Future<List> fetchGroupData() async {
    var datas = await encryptedStorage.readDataObj("listGroup");
    if (datas != null) {
      return datas;
    }
    var res = await baseCategoryRepository.getListCategoryGroups(
        'ecopark-group', 1, 10000, '{}');

    var response = res as List;
    groupData = response
        .map((item) =>
            SelectOptionItem(key: item.displayName, value: item.id, data: item))
        .toList();
    encryptedStorage.writeDataObj("listGroup", groupData);
    return groupData;
  }

  Future<List> fetchLeaderData() async {
    var datas = await encryptedStorage.readDataObj("listLeader");
    if (datas != null) {
      return datas;
    }
    var res = await usersRepository.getUsersRolesByCode('TO_TRUONG');

    var response = res as List;
    leaderData = response
        .map((item) =>
            SelectOptionItem(key: item.name, value: item.id, data: item))
        .toList();
    encryptedStorage.writeDataObj("listLeader", leaderData);
    return leaderData;
  }

  Future<List> fetchStatusData() async {
    statusData = listStatus;
    return statusData;
  }

  setDateRange(DateTimeRange? item, String Type) {
    switch (Type) {
      case "StartDate":
        filterProject.startDate = item;
        break;
      case "EndDate":
        filterProject.endDate = item;
        break;
      default:
        break;
    }
    change(null, status: RxStatus.success());
  }

  addChoice(SelectOptionItem item, String Type) {
    switch (Type) {
      case "Group":
        filterProject.group.add(item);
        break;
      case "Leader":
        filterProject.leader.add(item);
        break;
      case "Status":
        filterProject.status.add(item);
        break;
      default:
        break;
    }
    change(null, status: RxStatus.success());
  }

  removeChoice(SelectOptionItem item, String Type) {
    switch (Type) {
      case "Group":
        filterProject.group.remove(item);
        break;
      case "Leader":
        filterProject.leader.remove(item);
        break;
      case "Status":
        filterProject.status.remove(item);
        break;
      default:
        break;
    }
    change(null, status: RxStatus.success());
  }

  resetFilter() {
    filterProject = new FilterProject();
    change(null, status: RxStatus.success());
  }

  changeUI() {
    change(null, status: RxStatus.success());
  }
}
