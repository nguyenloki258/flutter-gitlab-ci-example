import 'dart:async';

import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class DashboardController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  AuthenticationRepository authenticationRepository = Get.find();

  String nameOfUser = "";
  late AnimationController animationController;
  late AnimationController bellController;
  late Animation<double> scaleAnimation,
      slideAnimation,
      fadeAnimation,
      bellAnimation;
  late Tween<Offset> offset;

  @override
  Future<void> onInit() async {
    initState();
    await fetchData();
    super.onInit();
  }

  void initState() {
    animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    bellController = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    );
    fadeAnimation = Tween<double>(begin: 0.0, end: 1).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );

    bellAnimation = Tween<double>(begin: 0.00, end: 0.00).animate(
      CurvedAnimation(
        parent: bellController,
        curve: Curves.linear,
      ),
    );

    offset = Tween<Offset>(begin: const Offset(1, 0), end: const Offset(0, 0));

    animationController.forward();
    bellController.repeat(reverse: true);
  }

  @override
  void dispose() {
    animationController.dispose();
    bellController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    final res = await authenticationRepository.getLocalUserData();
    if (res != null) {
      nameOfUser = res.name ?? "";
    }
    change(null, status: RxStatus.success());
  }
}
