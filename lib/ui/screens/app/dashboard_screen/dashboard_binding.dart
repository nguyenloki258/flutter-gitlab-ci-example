import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/dashboard_screen/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
  }
}
