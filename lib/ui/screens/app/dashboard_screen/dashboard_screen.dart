import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:serpapp/data/constant/images.dart';
import 'package:serpapp/ui/base/base_ui.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:serpapp/ui/screens/app/base_bottom_bar.dart';
import 'package:serpapp/ui/screens/app/dashboard_screen/dashboard_controller.dart';
import 'package:serpapp/ui/screens/app/notification/notification_screen.dart';
import 'package:serpapp/ui/screens/app/user_profile/user_profile_screen.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_screen.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_screen.dart';

class DashboardScreen extends BaseScreen {
  static const String routeName = '/dashboard';

  const DashboardScreen({Key? key}) : super(key: key);
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends BaseState<DashboardScreen>
    with BasicStateMixin {
  //late ThemeData theme;
  DashboardController dashboardController = Get.find();

  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
  }

  Widget _buildBlockItem(IconData icon, String text, Function callback) {
    return InkWell(
        onTap: () => callback(),
        child: Container(
          width: Get.width / 2 - 60,
          height: Get.width / 2 - 60,
          padding: FxSpacing.all(16),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.green, Colors.blue],
              ),
              color: Get.theme.colorScheme.primary,
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(icon, size: 34, color: Get.theme.colorScheme.onPrimary),
              FxSpacing.height(16),
              FxText.bodyMedium(
                text,
                textAlign: TextAlign.center,
                color: Get.theme.colorScheme.onPrimary,
              )
            ],
          ),
        ));
  }

  @override
  Widget createBody() {
    return dashboardController.obx((state) => Column(children: [
          Expanded(
              child: (ListView(
            padding: FxSpacing.fromLTRB(
                20, FxSpacing.safeAreaTop(context) + 20, 20, 0),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Hero(
                    tag: "splash_username",
                    child: FxText.titleLarge(
                      "Xin chào".tr + " " + dashboardController.nameOfUser,
                      fontWeight: 700,
                    ),
                  ),
                  RotationTransition(
                    turns: dashboardController.bellAnimation,
                    child: InkWell(
                      onTap: () {
                        Get.toNamed(NotificationScreen.routeName);
                      },
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Icon(
                            FeatherIcons.bell,
                            color: Get.theme.colorScheme.onBackground,
                            size: 20,
                          ),
                          // Positioned(
                          //   bottom: -2,
                          //   right: -2,
                          //   child: FxContainer.rounded(
                          //     paddingAll: 3,
                          //     color: Get.theme.colorScheme.primary,
                          //     child: Center(
                          //         child: FxText.bodySmall(
                          //       '2',
                          //       color: Get.theme.colorScheme.onPrimary,
                          //       fontSize: 8,
                          //     )),
                          //   ),
                          // )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              FxSpacing.height(4),
              FadeTransition(
                opacity: dashboardController.fadeAnimation,
                child: FxText.bodySmall(
                  'Chúc bạn một ngày làm việc hiệu quả !!',
                  xMuted: true,
                ),
              ),
              FxSpacing.height(20),
              FadeTransition(
                  opacity: dashboardController.fadeAnimation,
                  child: Container(
                      padding: FxSpacing.all(24),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Colors.green, Colors.blue],
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                              child: FxText.bodyLarge(
                            "Ecopark Tree Management System",
                            color: Get.theme.colorScheme.onPrimary,
                            fontWeight: 600,
                          )),
                          Hero(
                              tag: 'main-logo',
                              child:
                                  (SvgPicture.asset(Images.logo, width: 100))),
                        ],
                      ))),
              FxSpacing.height(20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildBlockItem(
                      FeatherIcons.checkSquare, "Phân công công việc", () {
                    Get.toNamed(WorkAssignmentScreen.routeName);
                  }),
                  _buildBlockItem(FeatherIcons.trello, "Theo dõi thực hiện",
                      () {
                    Get.toNamed(WorkTrackingScreen.routeName);
                  }),
                ],
              ),
              FxSpacing.height(20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildBlockItem(FeatherIcons.bell, "Thông báo", () {
                    Get.toNamed(NotificationScreen.routeName);
                  }),
                  _buildBlockItem(FeatherIcons.user, "Thông tin cá nhận", () {
                    Get.toNamed(UserProfileScreen.routeName);
                  }),
                ],
              )
            ],
          )))
        ]));
  }

  @override
  Widget? createBottomBar() {
    return baseBottomBar(ticker: dashboardController, indexHighlight: 0);
  }

  @override
  AppBar? createAppBar() => null;
}
