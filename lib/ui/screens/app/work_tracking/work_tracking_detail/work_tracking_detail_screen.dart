import 'dart:convert';

import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutx/flutx.dart';
import 'package:intl/intl.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_detail/work_tracking_detail_controller.dart';
import 'package:core_packages/core_package.dart';
import 'package:serpapp/widgets/app/app_tag.dart';

class WorkTrackingDetailScreen extends BaseScreen {
  static const String routeName = '/work_tracking_detail';

  const WorkTrackingDetailScreen({Key? key}) : super(key: key);
  @override
  _WorkTrackingDetailScreenState createState() =>
      _WorkTrackingDetailScreenState();
}

class _WorkTrackingDetailScreenState extends BaseState<WorkTrackingDetailScreen>
    with BasicStateMixin {
  //late ThemeData theme;
  WorkTrackingDetailController workTrackingDetailController = Get.find();
  var arguments = Get.arguments;

  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
    // workTrackingDetailController.taskData = arguments;
    _asyncMethod();
  }

  _asyncMethod() async {
    workTrackingDetailController.taskData =
        await workTrackingDetailController.getTaskById(arguments.id);
    await workTrackingDetailController.fetchData();
    await workTrackingDetailController.getInfoLeader();
  }

  Widget _buildLogs() {
    if (workTrackingDetailController.logData.isEmpty) {
      return Column(
        children: [
          FxSpacing.height(30),
          FxText.bodyMedium("Không có lịch sử"),
          FxSpacing.height(6),
          Icon(
            FeatherIcons.cloudOff,
            size: 20,
          )
        ],
      );
    }
    return Container(
      child: Column(children: [
        FxSpacing.height(16),
        for (var element in workTrackingDetailController.logData)
          buildLog(
              avatar: element["avatarUrl"],
              detailAction: element["message"],
              name: element["name"],
              action: element["name"],
              time: DateFormat('hh:mm,dd/MM/yyyy')
                  .format(element["createdOnDate"] ?? DateTime.now()))
      ]),
    );
  }

  Widget _buildComments() {
    if (workTrackingDetailController.commentData.isEmpty) {
      return Column(
        children: [
          FxSpacing.height(30),
          FxText.bodyMedium("Không có bình luận"),
          FxSpacing.height(6),
          Icon(
            FeatherIcons.cloudOff,
            size: 20,
          )
        ],
      );
    }
    return Container(
      child: Column(children: [
        for (var element in workTrackingDetailController.commentData)
          buildComment(
              avatar: element.avatarUrl ?? '',
              contentComment: element.message.toString(),
              name: element.name ?? '',
              time:
                  DateFormat('hh:mm,dd/MM/yyyy').format(element.createdOnDate))
      ]),
    );
  }

  Widget _buildActivity() {
    return Container(
      padding: FxSpacing.left(6),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: FxText.bodyMedium("Hoạt động"),
              ),
              InkWell(
                  onTap: () {
                    Get.bottomSheet(buildPickerSingleChoice(
                        title: "Chọn hoạt động",
                        value: workTrackingDetailController.activityState,
                        options: workTrackingDetailController.activityStateData,
                        onSelect: (p0) {
                          workTrackingDetailController.activityState = p0;
                          workTrackingDetailController.changeUI();
                        },
                        onSubmit: () {}));
                  },
                  child: Row(
                    children: [
                      FxText.bodySmall(
                          workTrackingDetailController.activityState.key),
                      Icon(FeatherIcons.chevronDown,
                          size: 20,
                          color:
                              Get.theme.colorScheme.onBackground.withAlpha(100))
                    ],
                  ))
            ],
          ),
          workTrackingDetailController.activityState.value == 1
              ? _buildComments()
              : _buildLogs()
        ],
      ),
    );
  }

  Future<void> _onRefresh() async {
    _asyncMethod();
    setState(() {});
  }

  @override
  Widget createBody() {
    return workTrackingDetailController.obx((state) => (RefreshIndicator(
        onRefresh: _onRefresh,
        color: Colors.white,
        backgroundColor: Get.theme.colorScheme.primary,
        child: Column(children: [
          Expanded(
              child: (ListView(
            padding: FxSpacing.fromLTRB(20, 0, 20, 20),
            children: [
              FxText.titleLarge(
                workTrackingDetailController.taskData.name,
                textAlign: TextAlign.center,
              ),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Trạng thái",
                  value: appTag(
                      status: workTrackingDetailController.taskData.statusObj),
                  icon: FeatherIcons.wifi,
                  callback: () => Get.bottomSheet(buildPickerSingleChoice(
                        title: 'Chọn trạng thái',
                        value: workTrackingDetailController
                            .taskEditData.statusSelectedData,
                        options: workTrackingDetailController.statusData,
                        onSelect: (p0) {
                          workTrackingDetailController
                              .taskEditData.statusSelectedData = p0;
                          workTrackingDetailController.changeUI();
                        },
                        onSubmit: () {
                          // ignore: todo
                          //TODO:
                          workTrackingDetailController.taskData.statusObj =
                              workTrackingDetailController
                                  .taskEditData.statusSelectedData?.value;
                          workTrackingDetailController.changeUI();
                        },
                        buildOption: (p0) {
                          return appTag(status: p0.value, isSelectedItem: true);
                        },
                      ))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Dự án",
                  value: FxText.bodyMedium(
                      workTrackingDetailController.getProjectName(
                          workTrackingDetailController.taskData.projectId)),
                  icon: FeatherIcons.trello,
                  callback: () => Get.bottomSheet(buildPickerSingleChoice(
                        title: 'Chọn dự án',
                        value: workTrackingDetailController
                            .taskEditData.projectSelectedData,
                        options: workTrackingDetailController.projectData,
                        onSelect: (p0) {
                          workTrackingDetailController
                              .taskEditData.projectSelectedData = p0;
                          workTrackingDetailController.changeUI();
                        },
                        onSubmit: () {
                          // ignore: todo
                          //TODO:
                          workTrackingDetailController.taskData.projectId =
                              workTrackingDetailController
                                  .taskEditData.projectSelectedData?.key;
                          workTrackingDetailController.changeUI();
                        },
                      ))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Tổ trưởng",
                  value: Row(
                    children: [
                      if (workTrackingDetailController.taskData.avatarUrl !=
                          null)
                        buildAvatarList(
                            workTrackingDetailController.taskData.avatarUrl),
                    ],
                  ),
                  icon: FeatherIcons.user,
                  iconAction: FeatherIcons.userPlus,
                  callback: () => Get.bottomSheet(buildPickerMultiChoice(
                        title: 'Chọn tổ trưởng',
                        value: workTrackingDetailController
                            .taskEditData.leaderSelectedData,
                        options: workTrackingDetailController.leaderData,
                        onSelect: (p0) {
                          workTrackingDetailController
                              .taskEditData.leaderSelectedData
                              .add(p0);

                          workTrackingDetailController.changeUI();
                        },
                        onRemove: (p0) {
                          workTrackingDetailController
                              .taskEditData.leaderSelectedData
                              .remove(p0);
                          workTrackingDetailController.changeUI();
                        },
                        onSubmit: () {
                          // ignore: todo
                          //TODO:
                          workTrackingDetailController.taskData.avatarUrl =
                              workTrackingDetailController
                                  .taskEditData.leaderSelectedData
                                  .map((e) => e.data);
                          workTrackingDetailController.changeUI();
                        },
                        buildOption: (p0) {
                          return buildCircleAvatar(p0.data?.name,
                              radius: 14, avatarUrl: p0.data?.avatarUrl);
                        },
                      ))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Tổ",
                  value: FxText.bodyMedium(
                      workTrackingDetailController.getGroupName()),
                  icon: FeatherIcons.users,
                  // iconAction: FeatherIcons.edit3,
                  callback: () => Get.bottomSheet(buildPickerMultiChoice(
                        title: 'Chọn tổ trưởng',
                        value: workTrackingDetailController
                            .taskEditData.groupSelectedData,
                        options: workTrackingDetailController.groupData,
                        onSelect: (p0) {
                          workTrackingDetailController
                              .taskEditData.groupSelectedData
                              .add(p0);

                          workTrackingDetailController.changeUI();
                        },
                        onRemove: (p0) {
                          workTrackingDetailController
                              .taskEditData.groupSelectedData
                              .remove(p0);
                          workTrackingDetailController.changeUI();
                        },
                        onSubmit: () {
                          // ignore: todo
                          //TODO:
                          var groupNameData = workTrackingDetailController
                              .taskEditData.groupSelectedData
                              .map((e) => e.key);
                          for (var item in (workTrackingDetailController
                              .taskData.metadataContentObjs as List)) {
                            if (item.fieldName == "EcoparkGroupName") {
                              item.fieldValues = jsonEncode(groupNameData);
                            }
                          }
                          workTrackingDetailController.changeUI();
                        },
                      ))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Người thực hiện",
                  value: FxText.bodyMedium(
                      workTrackingDetailController.getGroupName()),
                  icon: FeatherIcons.userCheck,
                  // iconAction: FeatherIcons.edit3,
                  callback: () => Get.bottomSheet(buildPickerTag(
                        title: 'Chọn người thực hiện',
                        value: workTrackingDetailController
                            .taskEditData.employeeSelectedData,
                        onAdd: (p0) {
                          workTrackingDetailController
                              .taskEditData.groupSelectedData
                              .add(p0);
                          workTrackingDetailController.changeUI();
                        },
                        onRemove: (p0) {
                          workTrackingDetailController
                              .taskEditData.groupSelectedData
                              .remove(p0);
                          workTrackingDetailController.changeUI();
                        },
                        onSubmit: () {
                          // ignore: todo
                          //TODO:
                          var groupNameData = workTrackingDetailController
                              .taskEditData.groupSelectedData
                              .map((e) => e.key);
                          for (var item in (workTrackingDetailController
                              .taskData.metadataContentObjs as List)) {
                            if (item.fieldName == "EcoparkGroupName") {
                              item.fieldValues = jsonEncode(groupNameData);
                            }
                          }
                          workTrackingDetailController.changeUI();
                        },
                      ))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Ngày bắt đầu",
                  value: FxText.bodyMedium(
                      workTrackingDetailController.taskData.fromDate != null
                          ? DateFormat("dd/MM/yyyy").format(
                              workTrackingDetailController.taskData.fromDate)
                          : "Trống"),
                  icon: FeatherIcons.calendar,
                  // iconAction: FeatherIcons.edit3,
                  callback: () => Get.bottomSheet(buildPickerDate(
                      title: 'Ngày bắt đầu',
                      value: workTrackingDetailController.taskData.fromDate,
                      onSelect: (p0) {
                        workTrackingDetailController.taskEditData.fromDate = p0;
                        workTrackingDetailController.changeUI();
                      },
                      onSubmit: () {
                        // ignore: todo
                        //TODO:
                        workTrackingDetailController.taskData.fromDate =
                            workTrackingDetailController.taskEditData.fromDate;
                        workTrackingDetailController.changeUI();
                      }))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Hạn chót",
                  value: FxText.bodyMedium(
                      workTrackingDetailController.taskData.toDate != null
                          ? DateFormat("dd/MM/yyyy").format(
                              workTrackingDetailController.taskData.toDate)
                          : "Trống"),
                  icon: FeatherIcons.calendar,
                  // iconAction: FeatherIcons.edit3,
                  callback: () => Get.bottomSheet(buildPickerDate(
                      title: 'Hạn chót',
                      value: workTrackingDetailController.taskData.toDate,
                      onSelect: (p0) {
                        workTrackingDetailController.taskEditData.toDate = p0;
                        workTrackingDetailController.changeUI();
                      },
                      onSubmit: () {
                        // ignore: todo
                        //TODO:
                        workTrackingDetailController.taskData.toDate =
                            workTrackingDetailController.taskEditData.toDate;
                        workTrackingDetailController.changeUI();
                      }))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Ngày hoàn thành",
                  value: FxText.bodyMedium(
                      workTrackingDetailController.taskData.finishDate != null
                          ? DateFormat("dd/MM/yyyy").format(
                              workTrackingDetailController.taskData.finishDate)
                          : "Trống"),
                  icon: FeatherIcons.calendar,
                  // iconAction: FeatherIcons.edit3,
                  callback: () => Get.bottomSheet(buildPickerDate(
                      title: 'Ngày bắt đầu',
                      value: workTrackingDetailController.taskData.finishDate,
                      onSelect: (p0) {
                        workTrackingDetailController.taskEditData.finishDate =
                            p0;
                        workTrackingDetailController.changeUI();
                      },
                      onSubmit: () {
                        // ignore: todo
                        //TODO:
                        workTrackingDetailController.taskData.finishDate =
                            workTrackingDetailController
                                .taskEditData.finishDate;
                        workTrackingDetailController.changeUI();
                      }))),
              FxSpacing.height(16),
              buildSettingItem(
                  title: "Mô tả",
                  longValue: Expanded(
                      child: FxText.bodyMedium(
                          workTrackingDetailController.taskData.description ??
                              "")),
                  icon: FeatherIcons.hash,
                  // iconAction: FeatherIcons.edit3,
                  callback: () => Get.bottomSheet(buildPickerText(
                      title: 'Nhập mô tả',
                      value: workTrackingDetailController.taskData.description,
                      onSelect: (p0) {
                        workTrackingDetailController.taskEditData.description =
                            p0;
                        workTrackingDetailController.changeUI();
                      },
                      onSubmit: () {
                        // ignore: todo
                        //TODO:
                        workTrackingDetailController.taskData.description =
                            workTrackingDetailController
                                .taskEditData.description;
                        workTrackingDetailController.changeUI();
                      }))),
              buildFileAttachment(workTrackingDetailController.fileData),
              FxSpacing.height(10),
              _buildActivity()
            ],
          )))
        ]))));
  }

  @override
  Widget? createBottomBar() {
    return workTrackingDetailController
        .obx((state) => workTrackingDetailController.activityState.value == 1
            ? FxContainer(
                marginAll: 24,
                paddingAll: 0,
                child: FxTextField(
                  onChanged: (value) =>
                      workTrackingDetailController.commentMessage = value,
                  focusedBorderColor: Get.theme.colorScheme.primary,
                  cursorColor: Get.theme.colorScheme.primary,
                  textFieldStyle: FxTextFieldStyle.outlined,
                  labelText: 'Thêm bình luận ...',
                  labelStyle: FxTextStyle.bodyMedium(
                      color: Get.theme.colorScheme.onBackground, xMuted: true),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  filled: true,
                  fillColor: Get.theme.colorScheme.primary.withAlpha(50),
                  style: TextStyle(fontSize: 16),
                  suffixIcon: InkWell(
                      onTap: () => workTrackingDetailController.postComment(),
                      child: Icon(
                        FeatherIcons.send,
                        color: Get.theme.colorScheme.primary,
                        size: 20,
                      )),
                ),
              )
            : SizedBox());
  }

  @override
  AppBar? createAppBar() => buildDefaultAppBar(
        title: "Chi tiết công việc",
      );
}
