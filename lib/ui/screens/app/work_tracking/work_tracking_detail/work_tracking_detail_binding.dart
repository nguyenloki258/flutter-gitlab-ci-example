import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_detail/work_tracking_detail_controller.dart';

class WorkTrackingDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WorkTrackingDetailController>(
        () => WorkTrackingDetailController());
  }
}
