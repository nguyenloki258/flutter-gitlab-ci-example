import 'dart:async';
import 'dart:convert';

import 'package:core_packages/core_package.dart';
import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:serpapp/data/constant/constant.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/data/repositories/base-manager/base-category/base_category_repo.dart';
import 'package:serpapp/data/repositories/communication/comment_repo.dart';
import 'package:serpapp/data/repositories/file_management/file_repo.dart';
import 'package:serpapp/data/repositories/logging/logging_repo.dart';
import 'package:serpapp/data/repositories/pm/task/tasks_repo.dart';
import 'package:serpapp/data/repositories/tree-management/project_repo.dart';
import 'package:serpapp/data/repositories/user/users_repo.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_controller.dart';
import 'package:serpapp/widgets/app/app_tag.dart';

import '../../../../../data/models/user/users_by_code_model.dart';

class TaskEditData {
  List<SelectOptionItem> groupSelectedData = [];
  List<SelectOptionItem> leaderSelectedData = [];
  List<SelectOptionItem> employeeSelectedData = [];
  SelectOptionItem? projectSelectedData;
  SelectOptionItem? statusSelectedData;
  DateTime? fromDate;
  DateTime? toDate;
  DateTime? finishDate;
  String? description;
}

class WorkTrackingDetailController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  final BaseCategoryRepository baseCategoryRepository = Get.find();
  final WorkAssignmentController workAssignmentController = Get.find();
  final UsersRepository usersRepository = Get.find();
  final ProjectRepository projectRepository = Get.find();
  final TasksRepository tasksRepository = Get.find();
  final AuthenticationRepository authenticationRepository = Get.find();
  final EncryptedStorage encryptedStorage = Get.find();
  final LoggingRepository logRepository = Get.find();
  final FileRepository fileRepository = Get.find();
  final CommentRepository commentRepository = Get.find();

  SelectOptionItem activityState =
      new SelectOptionItem(key: "Bình luận", value: 1);
  List<SelectOptionItem> activityStateData = [
    new SelectOptionItem(key: "Bình luận", value: 1),
    new SelectOptionItem(key: "Lịch sử", value: 2)
  ];
  dynamic taskData;
  List logData = [];
  List fileData = [];
  List commentData = [];
  TaskEditData taskEditData = new TaskEditData();
  List<SelectOptionItem> projectData = [];
  List<SelectOptionItem> priorityData = [];
  List<SelectOptionItem> statusData = [];
  List<SelectOptionItem> groupData = [];
  List<SelectOptionItem> leaderData = [];
  List<SelectOptionItem> employeeData = [];

  String commentMessage = "";
  late AnimationController animationController;
  late Animation<double> fadeAnimation;

  @override
  Future<void> onInit() async {
    animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    fadeAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );
    animationController.forward();
    super.onInit();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<Object> getTaskById(String id) async {
    // change(null, status: RxStatus.loading());
    taskData = await tasksRepository.getTasksById(id);
    // change(null, status: RxStatus.success());
    return taskData;
  }

  List usersAvatarUrl = [];
  getInfoLeader() async {
    usersAvatarUrl.length = 0;

    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }

    for (var i = 0; i < taskData.assigneeObj!.length; i++) {
      var currentUsers = users.where((x) => x.id == taskData.assigneeObj![i]);
      usersAvatarUrl.add(UsersByCodeModel(
          id: currentUsers.first.id!,
          name: currentUsers.first.name,
          avatarUrl: currentUsers.first.avatarUrl));
    }
    return usersAvatarUrl;
  }

  // Update trang thai
  Future<void> updateStatusTask(String id, statusObj) async {
    await tasksRepository.updateStateTasks(id, statusObj);
  }

  // Update nguoi thuc hien
  Future<void> updateUserActionTask(
      String id, List<dynamic> assigneeObj) async {
    await tasksRepository.updateUserTasks(id, assigneeObj);
  }

  // Update mo ta
  Future<void> updateDescriptionTask(String id, String description) async {
    await tasksRepository.updateDescriptionTasks(id, description);
  }

  // Update DateTime
  Future<void> updateDateTimeTask(
      String id, String key, String dateTime) async {
    await tasksRepository.updateDateTimeTasks(id, key, dateTime);
  }

  Future<void> fetchData() async {
    await Future.wait([
      fetchProjectData(),
      fetchStatusData(),
      fetchPriorityData(),
      fetchLogData(),
      fetchFile(),
      fetchComment(),
      fetchGroupData(),
      fetchLeaderData()
    ]);
    var currentStatus =
        statusData.where((element) => element.value == taskData.statusObj);
    if (currentStatus.length > 0) {
      taskEditData.statusSelectedData = currentStatus.toList()[0];
    }
    taskEditData.fromDate = taskData.fromDate;
    taskEditData.toDate = taskData.toDate;
    taskEditData.fromDate = taskData.fromDate;
    taskEditData.finishDate = taskData.finishDate;
    taskEditData.description = taskData.description;
    taskEditData.leaderSelectedData = leaderData
        .where((element) =>
            (taskData.assigneeObj != null ? taskData.assigneeObj as List : [])
                .where((y) => y == element.value)
                .length >
            0)
        .toList();
    // var currentGroup = (taskData.metadataContentObjs as List)
    //     .where((x) => x.fieldName == "EcoparkGroup")
    //     .toList();

    // if (currentGroup.length > 0) {
    //   var currentGroupObj = (jsonDecode(currentGroup[0].fieldValues) as List);
    //   taskEditData.projectSelectedData = taskData
    //       .where((element) =>
    //           (currentGroupObj).where((y) => y == element.value).length > 0)
    //       .toList();
    // }

    change(null, status: RxStatus.success());
  }

  Future<List> fetchProjectData() async {
    var treeProject = await workAssignmentController.fetchListData();
    projectData = [];
    for (var project in treeProject) {
      projectData.add(SelectOptionItem(
          key: project.name, value: project.id, data: project));
      for (var projectChild in project.children) {
        projectData.add(SelectOptionItem(
            key: projectChild.name,
            value: projectChild.id,
            data: projectChild));
      }
    }
    return projectData;
  }

  Future<List> fetchStatusData() async {
    statusData = listStatus;
    return statusData;
  }

  Future<List> fetchPriorityData() async {
    priorityData = listStatus;
    return priorityData;
  }

  Future<List> fetchLeaderData() async {
    var datas = await encryptedStorage.readDataObj("listLeader");
    if (datas != null) {
      return datas;
    }
    var res = await usersRepository.getUsersRolesByCode('TO_TRUONG');

    var response = res as List;
    leaderData = response
        .map((item) =>
            SelectOptionItem(key: item.name, value: item.id, data: item))
        .toList();
    encryptedStorage.writeDataObj("listLeader", leaderData);
    return leaderData;
  }

  Future<List> fetchGroupData() async {
    var datas = await encryptedStorage.readDataObj("listGroup");
    if (datas != null) {
      return datas;
    }
    var res = await baseCategoryRepository.getListCategoryGroups(
        'ecopark-group', 1, 10000, '{}');

    var response = res as List;
    groupData = response
        .map((item) =>
            SelectOptionItem(key: item.displayName, value: item.id, data: item))
        .toList();
    encryptedStorage.writeDataObj("listGroup", groupData);
    return groupData;
  }

  Future<List> fetchLogData() async {
    var res = await logRepository.postLogging("", 1, 9999, "", "", taskData.id);
    logData = res;
    // Get list user
    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }

    logData.forEach((log) {
      var name = log["fields"]["username"];
      var avatarUrl = users
          .where((user) => user.id == log["fields"]["userId"])
          .first
          .avatarUrl;
      log["message"] = "";
      log["avatarUrl"] = avatarUrl;
      var separators = ['\\[', '\\]'];
      var messageTemplate = log["messageTemplate"].toString();
      var logArgs = messageTemplate.split(new RegExp(separators.join('|')));

      var action = '';
      if (logArgs[0].toLowerCase().contains('update')) {
        action = "đã cập nhật";
      }
      if (logArgs[0].toLowerCase().contains('create')) {
        action = "đã tạo";
      }
      if (logArgs[0].toLowerCase().contains('delete')) {
        action = "đã xoá";
      }
      if (logArgs[0].toLowerCase().contains('loginsuccess')) {
        action = "đã đăng nhập";
      }
      log["name"] = name + " " + action;
      if (logArgs.length >= 2) {
        log["name"] = name + " " + action;
        // + " " + logArgs[1];
      }
      log["message"] = "";
      if (logArgs.length > 3) {
        var message = [];
        var actionDetail = [];
        for (var i = 2; i < logArgs.length; i++) {
          if ((i - 2) % 4 == 0 && (i - 2) + 3 <= logArgs.length - 1) {
            if (logArgs[i] == "ChangeStatus") {
              actionDetail.add("trạng thái");
              var getStatusValue = (status) {
                if (status == "") return "Trống";
                return appTagLabel(status: int.parse(status));
              };

              message.add(getStatusValue(logArgs[i + 1]) +
                  " => " +
                  getStatusValue(logArgs[i + 3]));
            }
            if (logArgs[i] == "ChangeFinishDate") {
              actionDetail.add("ngày HT thực tế");
              var getDateValue = (date) {
                if (date == "") return "Trống";
                return date;
                // var dateData = DateTime.parse(date);
                // return DateFormat('hh:mm,dd/MM/yyyy').format(dateData);
              };

              message.add(getDateValue(logArgs[i + 1]) +
                  " => " +
                  getDateValue(logArgs[i + 3]));
            }
          }
        }
        log["name"] = log["name"] + " " + actionDetail.join(", ");
        log["message"] = message.join("\n");
      }
    });

    return logData;
  }

  Future<List> fetchFile() async {
    fileData = await fileRepository.getFiles(taskData.id) as List;
    return fileData;
  }

  String _removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);

    return htmlText.replaceAll(exp, '');
  }

  Future<List> fetchComment() async {
    commentData = await commentRepository.getComment(
        taskData.id, 1, 999, '', '', '') as List;
    // Get list user
    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }
    commentData.forEach((cmt) {
      cmt.message = _removeAllHtmlTags(cmt.message);

      var name =
          users.where((user) => user.id == cmt.createdByUserId).first.name;
      var avatarUrl =
          users.where((user) => user.id == cmt.createdByUserId).first.avatarUrl;

      cmt.name = name;
      cmt.avatarUrl = avatarUrl;
    });
    return commentData;
  }

  getGroupName() {
    var groupName = "";
    if (taskData.metadataContentObjs != null) {
      var currentGroup = (taskData.metadataContentObjs as List)
          .where((x) => x.fieldName == "EcoparkGroupName")
          .toList();
      if (currentGroup.length > 0) {
        groupName =
            (jsonDecode(currentGroup[0].fieldValues) as List).join(", ");
      }
    }

    return groupName;
  }

  String getProjectName(String id) {
    var find = projectData.where((element) => element.value == id).toList();
    if (find.length > 0) {
      return find[0].key;
    }

    return "Không có dự án";
  }

  Future<void> postComment() async {
    var currentUser = await authenticationRepository.getLocalUserData();
    await commentRepository.postComment(
        taskData.id, "TASK", null, commentMessage, 0, null);
    commentData.add({
      "messages": commentMessage,
      "name": currentUser?.name ?? "",
      "avatarUrl": currentUser?.avatarUrl,
      "createdOnDate": DateTime.now()
    });
    changeUI();
  }

  changeUI() {
    change(null, status: RxStatus.success());
  }
}
