import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_controller.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_controller.dart';

class WorkTrackingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<WorkTrackingController>(() => WorkTrackingController());
    Get.lazyPut<WorkAssignmentController>(() => WorkAssignmentController());
  }
}
