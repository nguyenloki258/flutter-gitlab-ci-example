import 'dart:async';

import 'package:core_packages/core_package.dart';
import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:serpapp/data/constant/constant.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/data/repositories/base-manager/base-category/base_category_repo.dart';
import 'package:serpapp/data/repositories/pm/task/tasks_repo.dart';
import 'package:serpapp/data/repositories/tree-management/project_repo.dart';
import 'package:serpapp/data/repositories/user/users_repo.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_controller.dart';

class FilterTask {
  String searchText = "";
  DateTimeRange? startDate;
  DateTimeRange? endDate;
  List<SelectOptionItem> status = [];
  List<SelectOptionItem> priority = [];
  List<SelectOptionItem> project = [];
}

class WorkTrackingController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  final BaseCategoryRepository baseCategoryRepository = Get.find();
  final UsersRepository usersRepository = Get.find();
  final ProjectRepository projectRepository = Get.find();
  final AuthenticationRepository authenticationRepository = Get.find();
  final EncryptedStorage encryptedStorage = Get.find();
  final WorkAssignmentController workAssignmentController = Get.find();
  final TasksRepository tasksRepository = Get.find();
  List<SelectOptionItem> projectData = [];
  List<SelectOptionItem> priorityData = [];
  List<SelectOptionItem> statusData = [];

  List listTaskTree = [];
  FilterTask filterTask = new FilterTask();

  late AnimationController animationController;
  late Animation<double> fadeAnimation;

  DateTime? startDate = null;
  DateTime? endDate = null;
  int currentStep = 0;

  @override
  Future<void> onInit() async {
    animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    fadeAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );
    animationController.forward();
    super.onInit();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    await Future.wait([
      fetchListData(),
      fetchProjectData(),
      fetchStatusData(),
      fetchPriorityData(),
    ]);
    change(null, status: RxStatus.success());
  }

  String removeDiacritics(String str) {
    var withDia =
        'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var withoutDia =
        'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

    for (int i = 0; i < withDia.length; i++) {
      str = str.replaceAll(withDia[i], withoutDia[i]);
    }

    return str;
  }

  List filterData(list) {
    return list.where((element) {
      if (filterTask.searchText != "") {
        if (!removeDiacritics(element.name as String)
            .toLowerCase()
            .contains(removeDiacritics(filterTask.searchText).toLowerCase())) {
          return false;
        }
      }
      if (filterTask.status.length > 0) {
        if (filterTask.status
            .where((x) => x.value == element.statusObj)
            .isEmpty) {
          return false;
        }
      }
      if (filterTask.priority.length > 0) {
        if (filterTask.priority
            .where((x) => x.value == element.priorityObj)
            .isEmpty) {
          return false;
        }
      }
      if (filterTask.project.length > 0) {
        if (filterTask.project
            .where((x) => x.value == element.projectId)
            .isEmpty) {
          return false;
        }
      }

      if (filterTask.startDate != null) {
        if (element.fromDate == null) {
          return false;
        }
        var startDate = filterTask.startDate ??
            DateTimeRange(start: DateTime.now(), end: DateTime.now());
        if (startDate.start.isAfter(element.fromDate) ||
            startDate.end.isBefore(element.fromDate)) {}
        return false;
      }
      if (filterTask.endDate != null) {
        if (element.toDate == null) {
          return false;
        }
        var endDate = filterTask.endDate ??
            DateTimeRange(start: DateTime.now(), end: DateTime.now());
        if (endDate.start.isAfter(element.toDate) ||
            endDate.end.isBefore(element.toDate)) {}
        return false;
      }
      return true;
    }).toList();
  }

  Future<List> fetchListData() async {
    // Get data
    var datas = await encryptedStorage.readDataObj("listTaskTree");
    if (datas != null) {
      listTaskTree = datas;
      return datas;
    }
    var res = await tasksRepository.getAllTasksFilters(1, 999, '', '{}');
    var listDataFollowTask = res as List;
    listTaskTree = [];
    // Get list user
    var users = await encryptedStorage.readDataObj("listUsers");
    if (users == null) {
      users = await usersRepository.getAllUsersByApplication();
      encryptedStorage.writeDataObj("listUsers", users);
    }

    // add avt url to children
    for (var task in listDataFollowTask) {
      List<String> assigneeObj = task.assigneeObj ?? [];
      task.avatarUrl =
          users.where((element) => assigneeObj.contains(element.id)).toList();
      if (task.parentId == null) {
        var children = listDataFollowTask
            .where((child) => child.parentId == task.id)
            .toList();
        if (children.length > 0) {
          task.children = children;
        }
        listTaskTree.add(task);
      }
    }

    encryptedStorage.writeDataObj("listTaskTree", listTaskTree);
    return listTaskTree;
  }

  Future<List> fetchProjectData() async {
    var treeProject = await workAssignmentController.fetchListData();
    projectData = [];
    for (var project in treeProject) {
      projectData.add(SelectOptionItem(
          key: project.name, value: project.id, data: project));
      for (var projectChild in project.children) {
        projectData.add(SelectOptionItem(
            key: projectChild.name,
            value: projectChild.id,
            data: projectChild));
      }
    }
    return projectData;
  }

  Future<List> fetchStatusData() async {
    statusData = listStatus;
    return statusData;
  }

  Future<List> fetchPriorityData() async {
    priorityData = listPriority;
    return priorityData;
  }

  String getProjectName(String id) {
    var find = projectData.where((element) => element.value == id).toList();
    if (find.length > 0) {
      return find[0].key;
    }

    return "Không có dự án";
  }

  setDateRange(DateTimeRange? item, String Type) {
    switch (Type) {
      case "StartDate":
        filterTask.startDate = item;
        break;
      case "EndDate":
        filterTask.endDate = item;
        break;
      default:
        break;
    }
    change(null, status: RxStatus.success());
  }

  addChoice(SelectOptionItem item, String Type) {
    switch (Type) {
      case "Priority":
        filterTask.priority.add(item);
        break;
      case "Project":
        filterTask.project.add(item);
        break;
      case "Status":
        filterTask.status.add(item);
        break;
      default:
        break;
    }
    change(null, status: RxStatus.success());
  }

  removeChoice(SelectOptionItem item, String Type) {
    switch (Type) {
      case "Priority":
        filterTask.priority.remove(item);
        break;
      case "Project":
        filterTask.project.remove(item);
        break;
      case "Status":
        filterTask.status.remove(item);
        break;
      default:
        break;
    }
    change(null, status: RxStatus.success());
  }

  resetFilter(arguments) {
    filterTask = new FilterTask();
    if (arguments != null) {
      filterTask.project = [];
      filterTask.project.add(new SelectOptionItem(
          key: arguments.name, value: arguments.id, data: arguments));
    }
    change(null, status: RxStatus.success());
  }

  changeUI() {
    change(null, status: RxStatus.success());
  }
}
