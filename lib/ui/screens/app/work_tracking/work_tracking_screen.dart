import 'package:core_packages/core_package.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:intl/intl.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/app/base_bottom_bar.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_detail/work_assignment_detail_screen.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_controller.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_detail/work_tracking_detail_screen.dart';
import 'package:serpapp/utils/format_datetime.dart';
import 'package:serpapp/widgets/app/app_priority.dart';
import 'package:serpapp/widgets/app/app_tag.dart';

class WorkTrackingScreen extends BaseScreen {
  static const String routeName = '/work_tracking';

  const WorkTrackingScreen({Key? key, String project = ""}) : super(key: key);
  @override
  _WorkTrackingScreenState createState() => _WorkTrackingScreenState();
}

class _WorkTrackingScreenState extends BaseState<WorkTrackingScreen>
    with BasicStateMixin {
  //late ThemeData theme;
  WorkTrackingController workTrackingController = Get.find();
  var arguments = Get.arguments;
  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
    if (arguments != null) {
      workTrackingController.filterTask.project = [];
      workTrackingController.filterTask.project.add(new SelectOptionItem(
          key: arguments.name, value: arguments.id, data: arguments));
    }
    workTrackingController.fetchData();
  }

  Widget _buildSearchMenu() {
    return Column(children: [
      Row(
        children: [
          Expanded(
            child: TextFormField(
              onChanged: (value) {
                workTrackingController.filterTask.searchText = value;
                workTrackingController.changeUI();
              },
              style: FxTextStyle.bodyMedium(),
              cursorColor: Get.theme.colorScheme.primary,
              decoration: InputDecoration(
                hintText: "Tìm kiếm theo dõi thực hiện ...",
                hintStyle: FxTextStyle.bodySmall(
                    color: Get.theme.colorScheme.onBackground),
                border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                    borderSide: BorderSide.none),
                enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                    borderSide: BorderSide.none),
                focusedBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                    borderSide: BorderSide.none),
                filled: true,
                fillColor: Get.theme.cardTheme.color,
                prefixIcon: Icon(
                  FeatherIcons.search,
                  color: Get.theme.colorScheme.onBackground.withAlpha(150),
                ),
                isDense: true,
              ),
              textCapitalization: TextCapitalization.sentences,
            ),
          ),
          FxSpacing.width(6),
          FxContainer(
            paddingAll: 12,
            borderRadiusAll: 4,
            onTap: () {
              Get.bottomSheet(_buildFilter(),
                  isScrollControlled: true, isDismissible: false);
            },
            color: Get.theme.colorScheme.primary,
            child: Icon(
              FeatherIcons.sliders,
              color: Get.theme.colorScheme.onPrimary,
              size: 20,
            ),
          ),
        ],
      ),
      FxSpacing.height(4),
      Row(
        children: [
          Expanded(
              child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      buildQuickSelectOptions(
                          title: "Trạng thái",
                          selectedOptions:
                              workTrackingController.filterTask.status,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      FxSpacing.width(10),
                      buildQuickSelectOptions(
                          title: "Độ ưu tiên",
                          selectedOptions:
                              workTrackingController.filterTask.priority,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      arguments == null
                          ? FxSpacing.width(10)
                          : FxSpacing.width(0),
                      arguments == null
                          ? buildQuickSelectOptions(
                              title: "Dự án",
                              selectedOptions:
                                  workTrackingController.filterTask.project,
                              onClick: () {
                                Get.bottomSheet(_buildFilter(),
                                    isScrollControlled: true,
                                    isDismissible: false);
                              })
                          : FxSpacing.width(0),
                      FxSpacing.width(10),
                      buildQuickDateRangeOptions(
                          title: "Ngày bắt đầu",
                          dateTime: workTrackingController.filterTask.startDate,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                      FxSpacing.width(10),
                      buildQuickDateRangeOptions(
                          title: "Ngày HT hợp đồng",
                          dateTime: workTrackingController.filterTask.endDate,
                          onClick: () {
                            Get.bottomSheet(_buildFilter(),
                                isScrollControlled: true, isDismissible: false);
                          }),
                    ],
                  ))),
        ],
      ),
    ]);
  }

  Widget _buildFilter() {
    return workTrackingController.obx((state) => Container(
          height: Get.height - 50,
          margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8), bottom: Radius.circular(0)),
            color: Get.theme.scaffoldBackgroundColor,
          ),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Drawer(
            child: Container(
              color: Get.theme.scaffoldBackgroundColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: FxSpacing.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () {
                            Get.back();
                          },
                          child: SizedBox(
                            width: 100,
                            child: FxText.bodyMedium(
                              'Huỷ',
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        FxText.bodyLarge(
                          'Tìm kiếm',
                          textAlign: TextAlign.center,
                          fontWeight: 600,
                        ),
                        InkWell(
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onTap: () {
                              workTrackingController.resetFilter(arguments);
                            },
                            child: SizedBox(
                              width: 100,
                              child: FxText.bodyMedium(
                                'Đặt lại',
                                textAlign: TextAlign.right,
                              ),
                            ))
                      ],
                    ),
                  ),
                  FxSpacing.height(8),
                  Expanded(
                      child: ListView(
                    padding: FxSpacing.all(20),
                    children: [
                      buildSelectOptions(
                          "Trạng thái",
                          workTrackingController.statusData,
                          workTrackingController.filterTask.status,
                          (p0) =>
                              workTrackingController.addChoice(p0, "Status"),
                          (p0) =>
                              workTrackingController.removeChoice(p0, "Status"),
                          (p0) => SizedBox()),
                      FxSpacing.height(8),
                      buildSelectOptions(
                          "Độ ưu tiên",
                          workTrackingController.priorityData,
                          workTrackingController.filterTask.priority,
                          (p0) =>
                              workTrackingController.addChoice(p0, "Priority"),
                          (p0) => workTrackingController.removeChoice(
                              p0, "Priority"),
                          (p0) => Row(children: [
                                appPriority(
                                    priority: p0.value as int,
                                    showLabel: false),
                                FxSpacing.width(6),
                              ])),
                      arguments == null
                          ? FxSpacing.height(8)
                          : FxSpacing.height(0),
                      arguments == null
                          ? buildSelectOptions(
                              "Dự án",
                              workTrackingController.projectData,
                              workTrackingController.filterTask.project,
                              (p0) => workTrackingController.addChoice(
                                  p0, "Project"),
                              (p0) => workTrackingController.removeChoice(
                                  p0, "Project"),
                              (p0) => SizedBox())
                          : FxSpacing.height(0),
                      FxSpacing.height(8),
                      _buildDateRangeOptions(
                          "Ngày bắt đầu",
                          workTrackingController.filterTask.startDate,
                          (p0) => workTrackingController.setDateRange(
                              p0, "StartDate")),
                      FxSpacing.height(8),
                      _buildDateRangeOptions(
                          "Hạn chót",
                          workTrackingController.filterTask.endDate,
                          (p0) => workTrackingController.setDateRange(
                              p0, "EndDate")),
                    ],
                  )),
                  FxSpacing.height(8),
                  Padding(
                    padding: FxSpacing.horizontal(24),
                    child: FxButton.block(
                      borderRadiusAll: 8,
                      onPressed: () {
                        workTrackingController.changeUI();
                        Get.back();
                      },
                      backgroundColor: Get.theme.colorScheme.primary,
                      child: FxText.titleSmall(
                        "Tìm kiếm",
                        fontWeight: 700,
                        color: Get.theme.colorScheme.onPrimary,
                        letterSpacing: 0.4,
                      ),
                    ),
                  ),
                  FxSpacing.height(8),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _buildDateRangeOptions(String title, DateTimeRange? dateTime,
      Function(DateTimeRange?) onChange) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            FxText.bodyMedium(
              title,
              color: Get.theme.colorScheme.onBackground,
              fontWeight: 600,
            ),
          ],
        ),
        FxSpacing.height(8),
        Row(children: [
          FxContainer.none(
            color: dateTime != null
                ? Get.theme.colorScheme.primary.withAlpha(28)
                : Get.theme.cardTheme.color,
            borderRadiusAll: 20,
            padding: FxSpacing.xy(12, 8),
            onTap: () {
              workTrackingController.currentStep = 0;
              if (dateTime != null) {
                workTrackingController.startDate = dateTime.start;
                workTrackingController.endDate = dateTime.end;
              } else {
                workTrackingController.startDate = null;
                workTrackingController.endDate = null;
              }
              workTrackingController.changeUI();

              Get.bottomSheet(
                  workTrackingController.obx((state) => Container(
                        height: 500,
                        margin: FxSpacing.only(
                            bottom: 0, top: 0, right: 0, left: 0),
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(8),
                              bottom: Radius.circular(0)),
                          color: Get.theme.scaffoldBackgroundColor,
                        ),
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Drawer(
                          child: Container(
                            color: Get.theme.scaffoldBackgroundColor,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: FxSpacing.all(12),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      InkWell(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        onTap: () {
                                          Get.back();
                                        },
                                        child: SizedBox(
                                          width: 100,
                                          child: FxText.bodyMedium(
                                            'Huỷ',
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ),
                                      FxText.bodyMedium(
                                        'Chọn: ' + title,
                                        fontWeight: 600,
                                      ),
                                      InkWell(
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        onTap: () {
                                          onChange(new DateTimeRange(
                                              start: workTrackingController
                                                      .startDate ??
                                                  DateTime.now(),
                                              end: workTrackingController
                                                      .endDate ??
                                                  DateTime.now()));
                                          Get.back();
                                        },
                                        child: SizedBox(
                                          width: 100,
                                          child: FxText.bodyMedium(
                                            'Xác nhận',
                                            color:
                                                Get.theme.colorScheme.primary,
                                            textAlign: TextAlign.right,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                FxSpacing.height(8),
                                Expanded(
                                    child: ListView(
                                  padding: FxSpacing.all(20),
                                  children: [
                                    Stepper(
                                        controlsBuilder: (BuildContext context,
                                            ControlsDetails details) {
                                          return SizedBox();
                                        },
                                        currentStep:
                                            workTrackingController.currentStep,
                                        onStepTapped: (pos) {
                                          if (pos == 1) {
                                            workTrackingController.endDate =
                                                workTrackingController.startDate
                                                    ?.add(Duration(days: 1));
                                          } else {
                                            workTrackingController.startDate =
                                                workTrackingController.endDate
                                                    ?.subtract(
                                                        Duration(days: 1));
                                          }
                                          workTrackingController.currentStep =
                                              pos;

                                          workTrackingController.changeUI();
                                        },
                                        steps: <Step>[
                                          Step(
                                            isActive: true,
                                            title: FxText.bodyMedium(
                                                'Từ ngày: ' +
                                                    (workTrackingController
                                                                .startDate !=
                                                            null
                                                        ? DateFormat(
                                                                "dd/MM/yyyy")
                                                            .format(workTrackingController
                                                                    .startDate ??
                                                                DateTime.now())
                                                        : ""),
                                                fontWeight: 600),
                                            content: workTrackingController
                                                        .currentStep ==
                                                    0
                                                ? Container(
                                                    height: 150,
                                                    child: CupertinoDatePicker(
                                                        initialDateTime:
                                                            workTrackingController
                                                                .startDate,
                                                        maximumDate:
                                                            workTrackingController
                                                                .endDate,
                                                        mode:
                                                            CupertinoDatePickerMode
                                                                .date,
                                                        onDateTimeChanged:
                                                            (dateTime) {
                                                          workTrackingController
                                                                  .startDate =
                                                              dateTime;
                                                          workTrackingController
                                                              .changeUI();
                                                        }),
                                                  )
                                                : SizedBox(),
                                          ),
                                          Step(
                                            isActive: true,
                                            title: FxText.bodyMedium(
                                                (workTrackingController
                                                            .endDate !=
                                                        null
                                                    ? DateFormat("dd/MM/yyyy")
                                                        .format(
                                                            workTrackingController
                                                                    .endDate ??
                                                                DateTime.now())
                                                    : ""),
                                                fontWeight: 600),
                                            content: workTrackingController
                                                        .currentStep ==
                                                    1
                                                ? Container(
                                                    height: 150,
                                                    child: CupertinoDatePicker(
                                                        initialDateTime:
                                                            workTrackingController
                                                                .endDate,
                                                        minimumDate:
                                                            workTrackingController
                                                                .startDate,
                                                        mode:
                                                            CupertinoDatePickerMode
                                                                .date,
                                                        onDateTimeChanged:
                                                            (dateTime) {
                                                          workTrackingController
                                                                  .endDate =
                                                              dateTime;
                                                          workTrackingController
                                                              .changeUI();
                                                        }),
                                                  )
                                                : SizedBox(),
                                          ),
                                        ]),
                                  ],
                                )),
                                FxSpacing.height(16),
                              ],
                            ),
                          ),
                        ),
                      )),
                  isScrollControlled: true,
                  isDismissible: false);
            },
            child: FxText.bodyMedium(
              dateTime == null
                  ? "Từ ngày - Đến Ngày"
                  : DateFormat("dd/MM/yyyy").format(dateTime.start) +
                      " - " +
                      DateFormat("dd/MM/yyyy").format(dateTime.end),
              color: dateTime == null
                  ? Get.theme.colorScheme.onBackground
                  : Get.theme.colorScheme.primary,
            ),
          ),
        ]),
      ],
    );
  }

  Future<void> _onRefresh() async {
    workTrackingController.fetchData();
    print('refresh');
    setState(() {});
  }

  @override
  Widget createBody() {
    return workTrackingController.obx((state) => RefreshIndicator(
          onRefresh: _onRefresh,
          color: Colors.white,
          backgroundColor: Get.theme.colorScheme.primary,
          child: Column(children: [
            Expanded(
                child: (ListView(
              padding: FxSpacing.fromLTRB(20, 20, 20, 20),
              children: [
                _buildSearchMenu(),
                FxSpacing.height(20),
                SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildTaskList(workTrackingController.listTaskTree),
                    ],
                  ),
                )
              ],
            )))
          ]),
        ));
  }

  @override
  Widget? createBottomBar() {
    return baseBottomBar(ticker: workTrackingController, indexHighlight: 2);
  }

  @override
  AppBar? createAppBar() {
    if (arguments != null) {
      return buildDefaultAppBar(
          title: "Dự án: " + arguments?.name,
          icon: FeatherIcons.info,
          callback: (i) => {
                Get.toNamed(WorkAssignmentDetailScreen.routeName,
                    arguments: arguments)
              });
    }
    return null;
  }

  Widget _buildTaskList(List data) {
    var taskList = workTrackingController.filterData(data);
    List<Widget> list = [];
    for (var item in taskList) {
      list.add(_buildTaskItem(item, null));
      if (item.children != null && item.children.length > 0) {
        var children = workTrackingController.filterData(item.children);
        for (var child in children) {
          list.add(_buildTaskItem(child, item));
        }
      }
    }
    return Column(
      children: list,
    );
  }

  Widget _buildTaskItem(task, parentApp) {
    return FadeTransition(
      opacity: workTrackingController.fadeAnimation,
      child: FxContainer(
        onTap: () {
          Get.toNamed(WorkTrackingDetailScreen.routeName, arguments: task);
        },
        // borderRadiusAll: 0,
        borderRadius: parentApp != null
            ? BorderRadius.all(Radius.circular(8.0))
            : BorderRadius.only(
                topLeft: const Radius.circular(8.0),
                topRight: const Radius.circular(8.0),
              ),
        paddingAll: parentApp != null ? 0 : 2,
        margin: parentApp != null
            ? FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0)
            : FxSpacing.only(bottom: 0, top: 10, right: 0, left: 0),
        child: FxContainer(
          color: parentApp != null
              ? Get.theme.colorScheme.primary.withAlpha(50)
              : Colors.transparent,
          borderRadiusAll: 0,
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        parentApp != null
                            ? FxSpacing.width(0)
                            : FxSpacing.width(0),
                        Container(
                          decoration: BoxDecoration(
                              color: parentApp != null
                                  ? Get.theme.colorScheme.secondary
                                  : Get.theme.colorScheme.primary,
                              borderRadius: BorderRadius.circular(5)),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5),
                          child: Icon(
                            parentApp != null
                                ? FeatherIcons.layers
                                : FeatherIcons.checkSquare,
                            color: Get.theme.colorScheme.onPrimary,
                            size: 14,
                          ),
                        ),
                        parentApp != null
                            ? FxSpacing.width(4)
                            : FxSpacing.width(10),
                        Expanded(
                            child: FxText.bodyMedium(
                          task.name,
                          softWrap: false,
                          overflow: TextOverflow.ellipsis,
                          color: Get.theme.colorScheme.onBackground,
                          fontWeight: 600,
                        )),
                      ],
                    ),
                    FxSpacing.height(4),
                    Row(
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              FxSpacing.width(0),
                              Icon(
                                FeatherIcons.trello,
                                color: Get.theme.colorScheme.primary,
                                size: 14,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              FxText.labelMedium(
                                  workTrackingController
                                      .getProjectName(task.projectId),
                                  color: Get.theme.colorScheme.onBackground,
                                  letterSpacing: 0,
                                  muted: true),
                              SizedBox(
                                width: 8,
                              ),
                            ],
                          ),
                        ),
                        appTag(status: task.statusObj)
                      ],
                    ),
                    FxSpacing.height(4),
                    Row(
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              FxSpacing.width(0),
                              Icon(
                                FeatherIcons.calendar,
                                color: Get.theme.colorScheme.onBackground
                                    .withAlpha(80),
                                size: 14,
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              FxText.labelMedium(
                                  task.toDate != null
                                      ? getFormatedDate(task.toDate.toString())
                                      : 'Hạn chót',
                                  color: Get.theme.colorScheme.onBackground,
                                  letterSpacing: 0,
                                  muted: true),
                              SizedBox(
                                width: 8,
                              ),
                            ],
                          ),
                        ),
                        appPriority(priority: task.priorityObj)
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
