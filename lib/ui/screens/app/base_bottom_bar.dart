import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/dashboard_screen/dashboard_screen.dart';
import 'package:serpapp/ui/screens/app/user_profile/user_profile_screen.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_screen.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_screen.dart';

class NavItem {
  final String title;
  final IconData iconData;
  final Function? callback;

  NavItem(this.title, this.iconData, this.callback);
}

Widget baseBottomBar({int indexHighlight = 0, required TickerProvider ticker}) {
  TabController tabController =
      TabController(length: 4, vsync: ticker, initialIndex: indexHighlight);

  var navItems = [
    NavItem('Trang chủ', FeatherIcons.home, () {
      Get.toNamed(DashboardScreen.routeName);
    }),
    NavItem('Phân công công việc', FeatherIcons.share2, () {
      Get.toNamed(WorkAssignmentScreen.routeName);
    }),
    NavItem('Theo dõi thực hiện', FeatherIcons.list, () {
      Get.toNamed(WorkTrackingScreen.routeName);
    }),
    NavItem('Người dùng', FeatherIcons.user, () {
      Get.toNamed(UserProfileScreen.routeName);
    }),
  ];
  List<Widget> tabs = [];

  for (int i = 0; i < navItems.length; i++) {
    tabs.add(Hero(
        tag: "bottom_bar_item_$i",
        child: Icon(
          navItems[i].iconData,
          size: (indexHighlight == i) ? 25 : 20,
          color: (indexHighlight == i)
              ? Get.theme.colorScheme.primary
              : Get.theme.colorScheme.onBackground,
        )));
  }
  return FxContainer(
    bordered: true,
    enableBorderRadius: false,
    border: Border(
        top: BorderSide(
            color: Get.theme.dividerColor, width: 1, style: BorderStyle.solid)),
    padding: FxSpacing.xy(12, 16),
    color: Get.theme.scaffoldBackgroundColor,
    child: TabBar(
      controller: tabController,
      onTap: (value) => {navItems[value].callback!()},
      indicator: FxTabIndicator(
          indicatorColor: Get.theme.colorScheme.primary,
          indicatorHeight: 3,
          radius: 3,
          indicatorStyle: FxTabIndicatorStyle.rectangle,
          yOffset: -18),
      indicatorSize: TabBarIndicatorSize.tab,
      indicatorColor: Get.theme.colorScheme.primary,
      tabs: tabs,
    ),
  );
}
