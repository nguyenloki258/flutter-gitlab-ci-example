import 'dart:async';

import 'package:data_module/data_module.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:serpapp/data/repositories/notification/notification_repo.dart';

class NotificationController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  final NotificationRepository notificationRepo = Get.find();

  List listNotification = [];

  bool showLoading = true, uiLoading = true;
  late BuildContext context;
  @override
  Future<void> onInit() async {
    await fetchData();
    super.onInit();
  }

  void goBack() {
    Get.back();
  }

  Future<void> fetchData() async {
    await Future.delayed(const Duration(seconds: 1));
    showLoading = false;
    uiLoading = false;
    change(null, status: RxStatus.success());
  }

  Future<void> getNotification(List<String>? modules) async {
    var res = await notificationRepo.getAllNotification(1, 999, '{}', modules!);
    if (kDebugMode) {
      print(res);
    }

    //if response error
    if (res is ResponseError) {
      // return ActionModel(
      //   state: ActionState.Fail,
      // );
    }

    listNotification = res as List;

    listNotification.sort(
      (a, b) {
        int aDate = DateTime.parse(a.createdAt.toString()).day;
        int bDate = DateTime.parse(b.createdAt.toString()).day;
        return bDate.compareTo(aDate);
      },
    );

    // return ActionModel(
    //   state: ActionState.Success,
    // );
  }
}
