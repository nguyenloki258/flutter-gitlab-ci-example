import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutx/flutx.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/app/notification/notification_controller.dart';

class NotificationScreen extends BaseScreen {
  static const String routeName = '/notification';

  const NotificationScreen({Key? key}) : super(key: key);
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends BaseState<NotificationScreen>
    with BasicStateMixin {
  //late ThemeData theme;
  NotificationController notificationController = Get.find();

  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
    _asyncMethod();
  }

  void _asyncMethod() async {
    await notificationController.getNotification(['project', 'task']);
    setState(() {});
  }

  Widget _noNotification() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        FxSpacing.height(150),
        FxText.bodyMedium("Không có thông báo!"),
        FxSpacing.height(10),
        Icon(
          FeatherIcons.bellOff,
          size: 40,
        )
      ],
    );
  }

  Widget _buildSingleNotification(String message, String time, String module) {
    return FxContainer(
      padding: FxSpacing.xy(0, 20),
      margin: FxSpacing.bottom(20),
      borderRadiusAll: 4,
      child: Row(
        children: [
          FxSpacing.width(20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FxText.bodyMedium(
                  message,
                  fontWeight: 700,
                ),
                FxSpacing.height(8),
                Row(
                  children: [
                    Expanded(
                      child: FxText.bodySmall(
                        time,
                        muted: true,
                      ),
                    ),

                    // FxContainer(
                    //   width: 100,
                    //   borderRadiusAll: 4,
                    //   padding: FxSpacing.xy(8, 4),
                    //   color: Get.theme.colorScheme.primary.withAlpha(40),
                    //   child: Row(
                    //     children: [
                    //       module == 'task'
                    //           ? Icon(FeatherIcons.checkSquare,
                    //               color: Get.theme.colorScheme.primary)
                    //           : Icon(
                    //               FeatherIcons.trello,
                    //               color: Get.theme.colorScheme.primary,
                    //             ),
                    //       Padding(
                    //         padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    //         child: FxText.bodySmall(
                    //           module,
                    //           color: Get.theme.colorScheme.primary,
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
                FxSpacing.height(2),
              ],
            ),
          ),
          FxSpacing.width(20),
        ],
      ),
    );
  }

  Future<void> _onRefresh() async {
    _asyncMethod();
    setState(() {});
  }

  @override
  Widget createBody() {
    TabController tabController = TabController(
        length: 2, vsync: notificationController, initialIndex: 0);
    var listNotificationTask = notificationController.listNotification
        .where((element) => element.module == "task");
    var listNotificationProject = notificationController.listNotification
        .where((element) => element.module == "task");
    return RefreshIndicator(
      onRefresh: _onRefresh,
      color: Colors.white,
      backgroundColor: Get.theme.colorScheme.primary,
      child: Column(children: [
        FxSpacing.height(10),
        Container(
            child: TabBar(
          controller: tabController,
          indicator: FxTabIndicator(
              indicatorColor: Get.theme.colorScheme.primary,
              indicatorHeight: 3,
              radius: 3,
              indicatorStyle: FxTabIndicatorStyle.rectangle,
              yOffset: 20),
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorColor: Get.theme.colorScheme.primary,
          tabs: [
            FxText.bodyMedium(
              'Phân công công việc',
              color: Get.theme.colorScheme.primary,
            ),
            FxText.bodyMedium(
              'Theo dõi thực hiện',
              color: Get.theme.colorScheme.primary,
            ),
          ],
        )),
        FxSpacing.height(20),
        Expanded(
            child: TabBarView(controller: tabController, children: [
          ListView(
            padding: FxSpacing.nTop(20),
            children: [
              if (listNotificationTask.isEmpty) _noNotification(),
              for (var item in listNotificationTask)
                _buildSingleNotification(
                    item.title, getDate(item.createdAt), item.module),
            ],
          ),
          ListView(
            padding: FxSpacing.nTop(20),
            children: [
              if (listNotificationTask.isEmpty) _noNotification(),
              for (var item in listNotificationProject)
                _buildSingleNotification(
                    item.title, getDate(item.createdAt), item.module),
            ],
          )
        ]))
      ]),
    );
  }

  var dayFinal;
  String getDate(DateTime time) {
    DateTime now = DateTime.now();
    String formattedTime = DateFormat.H().format(now);
    String formattedTimeMinutes = DateFormat.m().format(now);
    String today = DateFormat.d().format(now);

    DateTime timeRecord = time;
    String formattedTimeRecord = DateFormat.H().format(timeRecord);
    String formattedTimeRecordMinutes = DateFormat.m().format(timeRecord);
    String dayRecord = DateFormat.d().format(time);

    var timeFinal = int.parse(formattedTime) - int.parse(formattedTimeRecord);
    dayFinal = int.parse(today) - int.parse(dayRecord);
    var timeFinalMinutes =
        int.parse(formattedTimeMinutes) - int.parse(formattedTimeRecordMinutes);

    if (dayFinal > 0) {
      return '$dayFinal Ngày trước ';
    } else if (timeFinal > 0) {
      return 'Khoảng $timeFinal giờ trước ';
    }

    return 'Khoảng $timeFinalMinutes phút trước';
  }

  @override
  Widget? createBottomBar() {
    return null;
  }

  @override
  AppBar? createAppBar() => buildDefaultAppBar(title: 'Thông báo', icon: null);
}
