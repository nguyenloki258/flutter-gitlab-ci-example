import 'package:get/get.dart';
import 'package:serpapp/ui/screens/app/dashboard_screen/dashboard_binding.dart';
import 'package:serpapp/ui/screens/app/dashboard_screen/dashboard_screen.dart';
import 'package:serpapp/ui/screens/app/user_profile/user_profile_binding.dart';
import 'package:serpapp/ui/screens/app/user_profile/user_profile_screen.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_binding.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_detail/work_assignment_detail_binding.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_detail/work_assignment_detail_screen.dart';
import 'package:serpapp/ui/screens/app/work_assignment/work_assignment_screen.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_binding.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_detail/work_tracking_detail_binding.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_detail/work_tracking_detail_screen.dart';
import 'package:serpapp/ui/screens/app/work_tracking/work_tracking_screen.dart';
import 'package:serpapp/ui/screens/app/notification/notification_binding.dart';
import 'package:serpapp/ui/screens/app/notification/notification_screen.dart';

List<GetPage> appRoutes = [
  GetPage(
      name: DashboardScreen.routeName,
      page: () => const DashboardScreen(),
      binding: DashboardBinding()),
  GetPage(
      name: WorkAssignmentScreen.routeName,
      page: () => const WorkAssignmentScreen(),
      binding: WorkAssignmentBinding()),
  GetPage(
      name: WorkAssignmentDetailScreen.routeName,
      page: () => const WorkAssignmentDetailScreen(),
      binding: WorkAssignmentDetailBinding()),
  GetPage(
      name: WorkTrackingScreen.routeName,
      page: () => const WorkTrackingScreen(),
      binding: WorkTrackingBinding()),
  GetPage(
      name: NotificationScreen.routeName,
      page: () => const NotificationScreen(),
      binding: NotificationBinding()),
  GetPage(
      name: UserProfileScreen.routeName,
      page: () => const UserProfileScreen(),
      binding: UserProfileBinding()),
  GetPage(
      name: WorkTrackingDetailScreen.routeName,
      page: () => const WorkTrackingDetailScreen(),
      binding: WorkTrackingDetailBinding()),
];
