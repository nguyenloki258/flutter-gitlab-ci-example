import 'package:get/get.dart';
import 'package:serpapp/ui/screens/auth/login/login_binding.dart';
import 'package:serpapp/ui/screens/auth/login/login_screen.dart';
import 'package:serpapp/ui/screens/auth/slash/slash_binding.dart';
import 'package:serpapp/ui/screens/auth/slash/slash_screen.dart';

List<GetPage> authRoutes = [
  GetPage(
      name: SlashScreen.routeName,
      page: () => const SlashScreen(),
      binding: SlashBinding()),
  GetPage(
      name: LogInScreen.routeName,
      page: () => const LogInScreen(),
      binding: LogInBinding()),
];
