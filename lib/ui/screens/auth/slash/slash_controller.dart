import 'package:get/get.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/ui/screens/app/dashboard_screen/dashboard_screen.dart';
import 'package:serpapp/ui/screens/auth/login/login_screen.dart';

class SlashController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  AuthenticationRepository authenticationRepository = Get.find();

  String message = "";
  String nameOfUser = "";

  @override
  Future<void> onInit() async {
    change(null, status: RxStatus.success());
    await fetchData();
    super.onInit();
  }

  Future fetchData() async {
    message = "Đang xử lý ...";
    await Future.delayed(const Duration(seconds: 1));
    final res = await authenticationRepository.getLocalUserData();
    if (res != null) {
      message = "Xin chào";
      nameOfUser = res.name ?? "";
      change(null, status: RxStatus.success());
      Get.offAndToNamed(DashboardScreen.routeName);
    } else {
      change(null, status: RxStatus.success());
      Get.offAndToNamed(LogInScreen.routeName);
    }
  }
}
