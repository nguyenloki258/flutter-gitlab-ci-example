import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutx/flutx.dart';
import 'package:get/get.dart';
import 'package:serpapp/data/constant/images.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/auth/slash/slash_controller.dart';

class SlashScreen extends BaseScreen {
  static const String routeName = '/slash-screen';

  const SlashScreen({Key? key}) : super(key: key);
  @override
  _SlashScreenState createState() => _SlashScreenState();
}

class _SlashScreenState extends BaseState<SlashScreen> with BasicStateMixin {
  SlashController slashController = Get.find();
  //late ThemeData theme;
  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
  }

  @override
  Widget createBody() {
    return Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.green, Colors.blue],
          ),
        ),
        child: ListView(
          padding: FxSpacing.fromLTRB(
              20, FxSpacing.safeAreaTop(context) + 50, 20, 20),
          children: [
            Hero(
                tag: 'main-logo',
                child: (SvgPicture.asset(Images.logo, height: 300))),
            renderSlash(),
          ],
        ));
  }

  Widget renderSlash() {
    return slashController.obx((state) => Container(
          child: (Column(
            children: [
              Hero(
                  tag: "splash_username",
                  child: FxText.titleSmall(
                    slashController.message + " " + slashController.nameOfUser,
                    color: Get.theme.colorScheme.onPrimary,
                  ))
            ],
          )),
        ));
  }

  @override
  AppBar? createAppBar() => null;
}
