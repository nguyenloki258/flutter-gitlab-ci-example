import 'package:get/get.dart';
import 'package:serpapp/ui/screens/auth/slash/slash_controller.dart';

class SlashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SlashController>(() => SlashController());
  }
}
