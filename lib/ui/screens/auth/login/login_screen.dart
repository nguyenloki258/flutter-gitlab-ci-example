import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:serpapp/data/constant/images.dart';
import 'package:serpapp/ui/base/base_ui.dart';
import 'package:serpapp/ui/screens/auth/login/login_controller.dart';

class LogInScreen extends BaseScreen {
  static const String routeName = '/log-in';

  const LogInScreen({Key? key}) : super(key: key);
  @override
  _LogInScreenState createState() => _LogInScreenState();
}

class _LogInScreenState extends BaseState<LogInScreen> with BasicStateMixin {
  //late ThemeData theme;
  LogInController logInController = Get.find();
  bool isVisiblePass = false;
  late OutlineInputBorder outlineInputBorder;
  @override
  void initState() {
    super.initState();
    //theme = AppTheme.estateTheme;
    outlineInputBorder = const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(4)),
      borderSide: BorderSide(
        color: Colors.transparent,
      ),
    );
  }

  @override
  Widget createBody() {
    return Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.green, Colors.blue],
          ),
        ),
        child: ListView(
          padding: FxSpacing.fromLTRB(
              20, FxSpacing.safeAreaTop(context) + 50, 20, 20),
          children: [
            Hero(
                tag: 'main-logo',
                child: (SvgPicture.asset(Images.logo, height: 300))),
            renderLoginForm(),
          ],
        ));
  }

  @override
  AppBar? createAppBar() => null;

  Widget renderLoginForm() {
    return logInController.obx(
        (state) => Form(
              key: logInController.formKey,
              child: Column(
                children: [
                  SlideTransition(
                    position: logInController.userNameAnimation,
                    child: TextFormField(
                      style: FxTextStyle.bodyMedium(),
                      decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          filled: true,
                          isDense: true,
                          fillColor: Get.theme.cardTheme.color,
                          prefixIcon: Icon(
                            FeatherIcons.user,
                            color: Get.theme.colorScheme.onBackground,
                          ),
                          hintText: "Tài khoản",
                          enabledBorder: outlineInputBorder,
                          focusedBorder: outlineInputBorder,
                          border: outlineInputBorder,
                          contentPadding: FxSpacing.all(16),
                          hintStyle: FxTextStyle.bodyMedium(),
                          isCollapsed: true),
                      maxLines: 1,
                      controller: logInController.userNameTE,
                      validator: logInController.validateUserName,
                      cursorColor: Get.theme.colorScheme.onBackground,
                    ),
                  ),
                  FxSpacing.height(20),
                  SlideTransition(
                    position: logInController.passwordAnimation,
                    child: TextFormField(
                      obscureText: !isVisiblePass,
                      style: FxTextStyle.bodyMedium(),
                      decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          filled: true,
                          isDense: true,
                          fillColor: Get.theme.cardTheme.color,
                          prefixIcon: Icon(
                            FeatherIcons.lock,
                            color: Get.theme.colorScheme.onBackground,
                          ),
                          suffixIcon: InkWell(
                              child: Icon(
                                isVisiblePass
                                    ? FeatherIcons.eye
                                    : FeatherIcons.eyeOff,
                                color: Get.theme.colorScheme.onBackground,
                              ),
                              onTap: () {
                                setState(() {
                                  isVisiblePass = !isVisiblePass;
                                });
                              }),
                          hintText: "Mật khẩu",
                          enabledBorder: outlineInputBorder,
                          focusedBorder: outlineInputBorder,
                          border: outlineInputBorder,
                          contentPadding: FxSpacing.all(16),
                          hintStyle: FxTextStyle.bodyMedium(),
                          isCollapsed: true),
                      maxLines: 1,
                      controller: logInController.passwordTE,
                      validator: logInController.validatePassword,
                      cursorColor: Get.theme.colorScheme.onBackground,
                    ),
                  ),
                  // FxSpacing.height(20),
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: FxButton.text(
                  //       onPressed: () {
                  //         logInController.goToForgotPasswordScreen();
                  //       },
                  //       padding: FxSpacing.zero,
                  //       splashColor: Get.theme.colorScheme.primary.withAlpha(40),
                  //       child: Row(
                  //         children: [
                  //           Text(
                  //             "Nhớ mật khẩu",
                  //             style: FxTextStyle.bodySmall(
                  //                 color: Get.theme.colorScheme.onPrimary),
                  //           ),
                  //         ],
                  //       )),
                  // ),
                  FxSpacing.height(20),
                  FxButton.block(
                    elevation: 0,
                    borderRadiusAll: 4,
                    onPressed: () {
                      logInController.login();
                    },
                    splashColor: Get.theme.colorScheme.onPrimary.withAlpha(28),
                    backgroundColor: Get.theme.colorScheme.primary,
                    child: Container(
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      decoration: const BoxDecoration(),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          FxText.labelLarge(
                            "Đăng nhập",
                            fontWeight: 600,
                            color: Get.theme.colorScheme.onPrimary,
                            letterSpacing: 0.4,
                          ),
                          FxSpacing.width(8),
                          SlideTransition(
                              position: logInController.arrowAnimation,
                              child: Icon(
                                FeatherIcons.arrowRight,
                                color: Get.theme.colorScheme.onPrimary,
                                size: 20,
                              )),
                        ],
                      ),
                    ),
                  ),
                  FxSpacing.height(20),
                  // renderOtherLogin(),
                  //   FxSpacing.height(20),
                  //   Center(
                  //     child: FxButton.text(
                  //         padding: FxSpacing.zero,
                  //         onPressed: () {
                  //           logInController.goToRegisterScreen();
                  //         },
                  //         child: FxText.labelLarge("Tạo tài khoản",
                  //             decoration: TextDecoration.underline,
                  //             color: Get.theme.colorScheme.onPrimary)),
                  //   )
                ],
              ),
            ),
        onLoading: renderLoading());
  }

  Widget renderOtherLogin() {
    return Column(
      children: [
        Row(
          children: [
            const Expanded(child: Divider()),
            Padding(
              padding: FxSpacing.x(16),
              child: FxText.bodySmall(
                'Hoặc đăng nhập bằng',
                muted: true,
                fontSize: 14,
                fontWeight: 600,
                color: Get.theme.colorScheme.onPrimary,
              ),
            ),
            const Expanded(child: Divider()),
          ],
        ),
        FxSpacing.height(20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FxContainer.bordered(
              padding: FxSpacing.all(16),
              borderRadiusAll: 4,
              child: Image(
                height: 20,
                width: 20,
                image: AssetImage(Images.google),
              ),
            ),
            FxContainer.bordered(
              padding: FxSpacing.all(16),
              borderRadiusAll: 4,
              child: Image(
                height: 20,
                width: 20,
                image: AssetImage(Images.apple),
              ),
            ),
            FxContainer.bordered(
              padding: FxSpacing.all(16),
              borderRadiusAll: 4,
              child: Image(
                height: 20,
                width: 20,
                image: AssetImage(Images.facebook),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget renderLoading() {
    return Container(
      child: (Column(
        children: [
          Hero(
              tag: "splash_username",
              child: FxText.titleSmall(
                "Đang xử lý ...",
                color: Get.theme.colorScheme.onPrimary,
              ))
        ],
      )),
    );
  }
}
