import 'package:data_module/data_module.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/ui/screens/auth/slash/slash_screen.dart';

class LogInController extends GetxController
    with GetTickerProviderStateMixin, StateMixin {
  AuthenticationRepository authenticationRepository = Get.find();
  late TextEditingController userNameTE, passwordTE;
  GlobalKey<FormState> formKey = GlobalKey(debugLabel: '_registerForm');
  late AnimationController arrowController,
      userNameController,
      passwordController;
  late Animation<Offset> arrowAnimation, userNameAnimation, passwordAnimation;
  int userNameCounter = 0;
  int passwordCounter = 0;
  @override
  Future<void> onInit() async {
    userNameTE = TextEditingController(text: '');
    passwordTE = TextEditingController(text: '');
    arrowController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    userNameController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 50));
    passwordController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 50));

    arrowAnimation =
        Tween<Offset>(begin: const Offset(0, 0), end: const Offset(8, 0))
            .animate(CurvedAnimation(
      parent: arrowController,
      curve: Curves.easeIn,
    ));
    userNameAnimation =
        Tween<Offset>(begin: const Offset(0.00, 0), end: const Offset(0.01, 0))
            .animate(CurvedAnimation(
      parent: userNameController,
      curve: Curves.easeIn,
    ));
    passwordAnimation =
        Tween<Offset>(begin: const Offset(0.00, 0), end: const Offset(0.01, 0))
            .animate(CurvedAnimation(
      parent: passwordController,
      curve: Curves.easeIn,
    ));

    userNameController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        userNameController.reverse();
      }
      if (status == AnimationStatus.dismissed && userNameCounter < 2) {
        userNameController.forward();
        userNameCounter++;
      }
    });

    passwordController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        passwordController.reverse();
      }
      if (status == AnimationStatus.dismissed && passwordCounter < 2) {
        passwordController.forward();
        passwordCounter++;
      }
    });
    change(null, status: RxStatus.success());
    super.onInit();
  }

  @override
  void dispose() {
    arrowController.dispose();
    userNameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  String? validateUserName(String? text) {
    if (text == null || text.isEmpty) {
      userNameController.forward();
      return "Vui lòng nhập tài khoản";
    }
    return null;
  }

  String? validatePassword(String? text) {
    if (text == null || text.isEmpty) {
      passwordController.forward();

      return "Vui lòng nhập mật khẩu";
    }
    // else if (!FxStringValidator.validateStringRange(
    //   text,
    // )) {
    //   passwordController.forward();
    //   return "Mật khẩu phải chứa từ 8-20 ký tự";
    // }
    return null;
  }

  void goToForgotPasswordScreen() {}

  Future<void> login() async {
    userNameCounter = 0;
    passwordCounter = 0;
    change(null, status: RxStatus.success());
    if (formKey.currentState!.validate()) {
      arrowController.forward();
      await Future.delayed(const Duration(milliseconds: 500));
      change(null, status: RxStatus.loading());
      var res = await authenticationRepository.login(
          Environment.ApplicationId, userNameTE.text, passwordTE.text);
      if (res is ResponseError) {
        arrowController.reverse();
        change(null, status: RxStatus.success());
      }

      if (res.forceChangePassword == true) {
        arrowController.reverse();
        change(null, status: RxStatus.success());
        // GOTO change password
      }
      Get.offAllNamed(SlashScreen.routeName);
    }
  }

  void goToRegisterScreen() {}
}
