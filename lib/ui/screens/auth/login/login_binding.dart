import 'package:get/get.dart';
import 'package:serpapp/ui/screens/auth/login/login_controller.dart';

class LogInBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LogInController>(() => LogInController());
  }
}
