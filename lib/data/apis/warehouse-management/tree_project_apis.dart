import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class TreeProjectAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> getAllTreeProject(String applicationId, String token,
      dynamic page, dynamic size, String filter, String type) async {
    final param = '?page=' +
        page.toString() +
        '&size=' +
        size.toString() +
        '&type=' +
        type +
        '&filter=' +
        filter;
    final getUrl =
        APIAddress.WAREHOUSE_SERVICE_API + 'v1/stock-location' + param;

    return _fetchManager.get(
        url: getUrl,
        // params: param,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
