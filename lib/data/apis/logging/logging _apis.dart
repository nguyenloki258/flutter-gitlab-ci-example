import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class LoggingAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> postLogging(
      String applicationId,
      String token,
      String fullTextSearch,
      int currentPage,
      int pageSize,
      String module,
      String objectType,
      String objectId) async {
    const postUrl = APIAddress.LOGGING_SERVICE_API + 'v1/logs';
    final body = {
      // 'FullTextSearch': fullTextSearch,
      // 'currentPage': currentPage,
      // 'module': module,
      // 'objectId': objectId,
      // 'objectType': objectType,
      // 'pageSize': pageSize,
      "currentPage": '1',
      "pageSize": '999',
      "fullTextSearch": "",
      "objectId": "17b78f5c-7260-b9d3-f9f4-aa96128fb062",
      "objectType": "",
      "module": ""
    };

    return _fetchManager.post(
        url: postUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
