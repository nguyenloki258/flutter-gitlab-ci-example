import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class FileAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> getFiles(
    dynamic applicationId,
    dynamic token,
    String relatedObjectId,
  ) async {
    final param = '?relatedObjectId=' + relatedObjectId;
    final getUrl = "${APIAddress.TREE_MANAGEMENT_SERVICE_API}v1/files/$param";
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> uploadFiles(
    String applicationId,
    String userToken,
    String fileUrl, {
    String? name,
    String? type,
    String? description,
    String? parentId,
    String? objectId,
    String? objectType,
    String? relatedProjectId,
    String? relatedTaskId,
  }) async {
    const url = "${APIAddress.FILE_SERVICE_API}v1/upload/image";

    var body = <String, String>{};
    if (name != null && name.isNotEmpty) body['name'] = name;
    if (type != null && type.isNotEmpty) body['type'] = type;
    if (description != null && description.isNotEmpty) {
      body['description'] = description;
    }
    if (parentId != null && parentId.isNotEmpty) body['parentId'] = parentId;
    if (objectId != null && objectId.isNotEmpty) body['objectId'] = objectId;
    if (objectType != null && objectType.isNotEmpty) {
      body['objectType'] = objectType;
    }
    if (relatedProjectId != null && relatedProjectId.isNotEmpty) {
      body['relatedProjectId'] = relatedProjectId;
    }
    if (relatedTaskId != null && relatedTaskId.isNotEmpty) {
      body['relatedTaskId'] = relatedTaskId;
    }

    final res = await _fetchManager.uploadFile(
      url: url,
      fileKey: 'Files',
      fileUrl: fileUrl,
      //filename: fileName,
      headers: await _headerRepository.getHeaders(applicationId, userToken),
    );
    return res;
  }

  Future<ResponseBase> uploadImage(String applicationId, String userToken,
      String fileUrl, String fileName) async {
    const url = "${APIAddress.FILE_SERVICE_API}v1/upload/image";

    final res = await _fetchManager.uploadFile(
      url: url,
      fileKey: 'file',
      fileUrl: fileUrl,
      //filename: fileName,
      headers: await _headerRepository.getHeaders(applicationId, userToken),
    );
    return res;
  }
}
