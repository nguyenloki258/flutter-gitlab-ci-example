// ignore_for_file: unused_field

import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class ProjectAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> getAll(
    String applicationId,
    String token,
    dynamic page,
    dynamic size,
    String filter,
  ) async {
    final param = '?page=' +
        page.toString() +
        '&size=' +
        size.toString() +
        '&filter=' +
        filter;
    final getUrl =
        APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/projects' + param;

    return _fetchManager.get(
        url: getUrl,
        // params: param,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  
  Future<ResponseBase> getProjectById(
      String applicationId, String token, String id) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/projects/' + id;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

    Future<ResponseBase> updateStateProject(
      String applicationId, String token, String id, statusObj) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/projects/' + id;

    final body = {
      'statusObj': statusObj,
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateDescriptionProjects(
      String applicationId, String token, String id, String description) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/projects/' + id;

    final body = {
      'description': description,
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateAssigneeObjProject(String applicationId,
      String token, String id, List<dynamic> assigneeObj) async {
    final getUrl =
        APIAddress.PROJECT_MANAGEMENT_SERVICE_API + 'v1/projects/' + id;

    final body = {
      'assigneeObj': assigneeObj.toList(),
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateMetadataContentObjs(String applicationId,
      String token, String id, List<dynamic> metadataContentObjs) async {
    final getUrl =
        APIAddress.PROJECT_MANAGEMENT_SERVICE_API + 'v1/projects/' + id;

    final body = {
      'metadataContentObjs': metadataContentObjs.toList(),
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
