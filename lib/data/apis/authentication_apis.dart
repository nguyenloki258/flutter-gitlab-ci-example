import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class AuthenticationAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> register(String applicationId, String username,
      String firstName, String lastName, String phoneNumber) async {
    const postUrl = APIAddress.AUTHENTICATION_SERVICE_API;
    final body = {
      'username': username,
      'firstName': firstName,
      'lastName': lastName,
      'phoneNumber': phoneNumber,
    };

    return _fetchManager.post(
        url: postUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, ''));
  }

  Future<ResponseBase> login(
      String applicationId, String username, String password,
      {String? location}) async {
    var body = {
      'username': username,
      'password': password,
    };
    if (location != null) body['location'] = location;

    const postUrl = APIAddress.AUTHENTICATION_SERVICE_API + 'v1/auth/login';

    return _fetchManager.post(
        url: postUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, ''));
  }

  Future<ResponseBase> changePassword(String applicationId, String userToken,
      String oldPassword, String newPassword, String confirmPassword,
      {String? location}) async {
    var body = {
      'oldPassword': oldPassword,
      'newPassword': newPassword,
      'confirmPassword': confirmPassword,
    };
    if (location != null) body['location'] = location;

    const postUrl =
        "${APIAddress.AUTHENTICATION_SERVICE_API}v1/account/password";

    return _fetchManager.put(
        url: postUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, userToken));
  }

  Future<ResponseBase> updateUserProfile(String applicationId, String userToken,
      {String? phoneNumber, String? email, String? avatarUrl}) async {
    const patchUrl = "${APIAddress.AUTHENTICATION_SERVICE_API}v1/account";

    var body = <String, String>{};
    if (email != null && email.isNotEmpty) {
      body['email'] = email;
    }
    if (phoneNumber != null && phoneNumber.isNotEmpty) {
      body['phoneNumber'] = phoneNumber;
    }
    if (avatarUrl != null && avatarUrl.isNotEmpty) {
      body['avatarUrl'] = avatarUrl;
    }

    return _fetchManager.patch(
        url: patchUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, userToken));
  }
}
