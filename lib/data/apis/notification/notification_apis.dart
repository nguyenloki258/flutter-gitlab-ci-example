import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class NotificationAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();
  Future<ResponseBase> getAllNotification(dynamic applicationId, dynamic token,
      List<String> modules, dynamic page, dynamic size, dynamic filter) async {
    var query = '?pageSize=' +
        size.toString() +
        '&currentPage=' +
        page.toString() +
        '&filter=' +
        filter;
    if (filter != null) {
      query += '&filter=${filter}';
    }
    if (modules.length > 0) {
      modules.forEach((x) => (query += '&modules=${x}'));
    }
    final getUrl =
        APIAddress.NOTIFICATION_SERVICE_API + 'v1/notifications' + query;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
