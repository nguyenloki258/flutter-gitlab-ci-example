import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class TaskAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();
  Future<ResponseBase> getAllTasksFilters(dynamic applicationId, dynamic token,
      dynamic page, dynamic size, dynamic sort, dynamic filter) async {
    final param = '?page=' +
        page.toString() +
        '&size=' +
        size.toString() +
        '&filter=' +
        filter;
    final getUrl =
        APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks' + param;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> getTasksByProjectId(
    String applicationId,
    String token,
    String projectId,
    dynamic page,
    dynamic size,
    dynamic sort,
    dynamic filter,
  ) async {
    final param = '?projectId=' +
        projectId +
        '&page=' +
        page.toString() +
        '&size=' +
        size.toString() +
        '&filter=' +
        filter;
    final getUrl =
        APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks' + param;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> getTasksById(
      String applicationId, String token, String id) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks/' + id;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateStateTasks(
      String applicationId, String token, String id, statusObj) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks/' + id;

    final body = {
      'statusObj': statusObj,
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateUserActionTasks(String applicationId, String token,
      String id, List<dynamic> assigneeObj) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks/' + id;

    final body = {
      'assigneeObj': assigneeObj.toList(),
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateDescriptionTasks(
      String applicationId, String token, String id, String description) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks/' + id;

    final body = {
      'description': description,
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> updateDateTimeTasks(String applicationId, String token,
      String id, String key, String dateTime) async {
    final getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API + 'v1/tasks/' + id;

    final body = {
      // ignore: unnecessary_string_interpolations
      '$key': dateTime.toString(),
    };
    return _fetchManager.patch(
        url: getUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
