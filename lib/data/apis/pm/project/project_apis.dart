import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class PMProjectAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<dynamic> getAllProjectFilters(String applicationId, String token,
      dynamic page, dynamic size, String sort, dynamic filter) async {
    const getUrl = APIAddress.TREE_MANAGEMENT_SERVICE_API +
        'v1/projects?page=1&size=20&filter={}';
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
