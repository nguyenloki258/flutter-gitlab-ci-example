import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class UsersAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> getRolesByCode(
    dynamic applicationId,
    dynamic token,
    String code,
  ) async {
    final getUrl =
        APIAddress.USERMNG_SERVICE_API + 'v1/roles/bycode/' + code + '/users';
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> getUserByApplication(
    dynamic applicationId,
    dynamic token,
  ) async {
    final getUrl = APIAddress.USERMNG_SERVICE_API +
        'v1/users/all/' +
        '?applicationId=' +
        applicationId;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
