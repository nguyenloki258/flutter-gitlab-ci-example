import 'package:data_module/data_module.dart';

import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class CommentAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> postComment(
      String applicationId,
      String token,
      String objectId,
      String objectType,
      String? ref,
      String message,
      statusObj,
      String? attachment) async {
    var body = {
      'objectId': objectId,
      'objectType': objectType,
      'ref': ref,
      'message': message,
      'statusObj': statusObj,
      'attachment': attachment,
    };

    const postUrl = APIAddress.COMMUNICATION_SERVICE_API + 'v1/comment';

    return _fetchManager.post(
        url: postUrl,
        body: body,
        headers: await _headerRepository.getHeaders(applicationId, token));
  }

  Future<ResponseBase> getComment(
      String applicationId,
      String token,
      String objectId,
      int page,
      int size,
      dynamic sort,
      dynamic queryString,
      dynamic filter) async {
    final param = '?objectId=' +
        objectId +
        '&page=' +
        page.toString() +
        '&size=' +
        size.toString() +
        '&filter=' +
        filter;
    final getUrl = APIAddress.COMMUNICATION_SERVICE_API + 'v1/comment' + param;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
