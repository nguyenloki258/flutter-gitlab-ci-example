// ignore_for_file: unused_field

import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/api_address.dart';

class BaseCategoryAPI {
  final HeaderRepository _headerRepository = Get.find();
  final FetchManager _fetchManager = Get.find();

  Future<ResponseBase> getListCategoryGroups(String applicationId, String token,
      String type, page, size, queryString) async {
    final getUrl = APIAddress.BASE_SERVICE_API +
        'v1/category/' +
        type +
        '?page=' +
        page.toString() +
        '&size=' +
        size.toString() +
        '&filter=' +
        queryString;
    return _fetchManager.get(
        url: getUrl,
        // params: Uri.parse(param),
        headers: await _headerRepository.getHeaders(applicationId, token));
  }
}
