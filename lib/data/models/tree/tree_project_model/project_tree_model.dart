import 'dart:convert';

import 'package:collection/collection.dart';

import 'labels_obj_model.dart';
import 'metadata_obj_model.dart';

class ProjectTreeModel {
  String? createdByUserId;
  String? lastModifiedByUserId;
  DateTime? lastModifiedOnDate;
  DateTime? createdOnDate;
  String? applicationId;
  String id;
  String? name;
  String? type;
  String? completePath;
  String? completeCode;
  String? completeName;
  String? usage;
  String? parentId;
  String? parentName;
  String? partnerId;
  String? partnerName;
  String? comment;
  String? posx;
  String? posy;
  String? posz;
  String? ownerId;
  String? companyId;
  bool? scrapLocation;
  bool? returnLocation;
  String? removalStrategyId;
  String? putawayStrategyId;
  String? integratedStatus;
  String? intergratedMessage;
  List<MetadataContentObjModel>? metadataContentObjs;
  String? metadataContent;
  String? code;
  bool? isActive;
  int? order;
  List<LabelsObjModel>? labelsObjs;
  String? labelsJson;
  int? cLeft;
  int? cRight;
  int? cLevel;
  ProjectTreeModel({
    this.createdByUserId,
    this.lastModifiedByUserId,
    this.lastModifiedOnDate,
    this.createdOnDate,
    this.applicationId,
    required this.id,
    this.name,
    this.type,
    this.completePath,
    this.completeCode,
    this.completeName,
    this.usage,
    this.parentId,
    this.parentName,
    this.partnerId,
    this.partnerName,
    this.comment,
    this.posx,
    this.posy,
    this.posz,
    this.ownerId,
    this.companyId,
    this.scrapLocation,
    this.returnLocation,
    this.removalStrategyId,
    this.putawayStrategyId,
    this.integratedStatus,
    this.intergratedMessage,
    this.metadataContentObjs,
    this.metadataContent,
    this.code,
    this.isActive,
    this.order,
    this.labelsObjs,
    this.labelsJson,
    this.cLeft,
    this.cRight,
    this.cLevel,
  });

  ProjectTreeModel copyWith({
    String? createdByUserId,
    String? lastModifiedByUserId,
    DateTime? lastModifiedOnDate,
    DateTime? createdOnDate,
    String? applicationId,
    String? id,
    String? name,
    String? type,
    String? completePath,
    String? completeCode,
    String? completeName,
    String? usage,
    String? parentId,
    String? parentName,
    String? partnerId,
    String? partnerName,
    String? comment,
    String? posx,
    String? posy,
    String? posz,
    String? ownerId,
    String? companyId,
    bool? scrapLocation,
    bool? returnLocation,
    String? removalStrategyId,
    String? putawayStrategyId,
    String? integratedStatus,
    String? intergratedMessage,
    List<MetadataContentObjModel>? metadataContentObjs,
    String? metadataContent,
    String? code,
    bool? isActive,
    int? order,
    List<LabelsObjModel>? labelsObjs,
    String? labelsJson,
    int? cLeft,
    int? cRight,
    int? cLevel,
  }) {
    return ProjectTreeModel(
      createdByUserId: createdByUserId ?? this.createdByUserId,
      lastModifiedByUserId: lastModifiedByUserId ?? this.lastModifiedByUserId,
      lastModifiedOnDate: lastModifiedOnDate ?? this.lastModifiedOnDate,
      createdOnDate: createdOnDate ?? this.createdOnDate,
      applicationId: applicationId ?? this.applicationId,
      id: id ?? this.id,
      name: name ?? this.name,
      type: type ?? this.type,
      completePath: completePath ?? this.completePath,
      completeCode: completeCode ?? this.completeCode,
      completeName: completeName ?? this.completeName,
      usage: usage ?? this.usage,
      parentId: parentId ?? this.parentId,
      parentName: parentName ?? this.parentName,
      partnerId: partnerId ?? this.partnerId,
      partnerName: partnerName ?? this.partnerName,
      comment: comment ?? this.comment,
      posx: posx ?? this.posx,
      posy: posy ?? this.posy,
      posz: posz ?? this.posz,
      ownerId: ownerId ?? this.ownerId,
      companyId: companyId ?? this.companyId,
      scrapLocation: scrapLocation ?? this.scrapLocation,
      returnLocation: returnLocation ?? this.returnLocation,
      removalStrategyId: removalStrategyId ?? this.removalStrategyId,
      putawayStrategyId: putawayStrategyId ?? this.putawayStrategyId,
      integratedStatus: integratedStatus ?? this.integratedStatus,
      intergratedMessage: intergratedMessage ?? this.intergratedMessage,
      metadataContentObjs: metadataContentObjs ?? this.metadataContentObjs,
      metadataContent: metadataContent ?? this.metadataContent,
      code: code ?? this.code,
      isActive: isActive ?? this.isActive,
      order: order ?? this.order,
      labelsObjs: labelsObjs ?? this.labelsObjs,
      labelsJson: labelsJson ?? this.labelsJson,
      cLeft: cLeft ?? this.cLeft,
      cRight: cRight ?? this.cRight,
      cLevel: cLevel ?? this.cLevel,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (createdByUserId != null) {
      result.addAll({'createdByUserId': createdByUserId});
    }
    if (lastModifiedByUserId != null) {
      result.addAll({'lastModifiedByUserId': lastModifiedByUserId});
    }
    if (lastModifiedOnDate != null) {
      result.addAll(
          {'lastModifiedOnDate': lastModifiedOnDate!.millisecondsSinceEpoch});
    }
    if (createdOnDate != null) {
      result.addAll({'createdOnDate': createdOnDate!.millisecondsSinceEpoch});
    }
    if (applicationId != null) {
      result.addAll({'applicationId': applicationId});
    }
    result.addAll({'id': id});
    if (name != null) {
      result.addAll({'name': name});
    }
    if (type != null) {
      result.addAll({'type': type});
    }
    if (completePath != null) {
      result.addAll({'completePath': completePath});
    }
    if (completeCode != null) {
      result.addAll({'completeCode': completeCode});
    }
    if (completeName != null) {
      result.addAll({'completeName': completeName});
    }
    if (usage != null) {
      result.addAll({'usage': usage});
    }
    if (parentId != null) {
      result.addAll({'parentId': parentId});
    }
    if (parentName != null) {
      result.addAll({'parentName': parentName});
    }
    if (partnerId != null) {
      result.addAll({'partnerId': partnerId});
    }
    if (partnerName != null) {
      result.addAll({'partnerName': partnerName});
    }
    if (comment != null) {
      result.addAll({'comment': comment});
    }
    if (posx != null) {
      result.addAll({'posx': posx});
    }
    if (posy != null) {
      result.addAll({'posy': posy});
    }
    if (posz != null) {
      result.addAll({'posz': posz});
    }
    if (ownerId != null) {
      result.addAll({'ownerId': ownerId});
    }
    if (companyId != null) {
      result.addAll({'companyId': companyId});
    }
    if (scrapLocation != null) {
      result.addAll({'scrapLocation': scrapLocation});
    }
    if (returnLocation != null) {
      result.addAll({'returnLocation': returnLocation});
    }
    if (removalStrategyId != null) {
      result.addAll({'removalStrategyId': removalStrategyId});
    }
    if (putawayStrategyId != null) {
      result.addAll({'putawayStrategyId': putawayStrategyId});
    }
    if (integratedStatus != null) {
      result.addAll({'integratedStatus': integratedStatus});
    }
    if (intergratedMessage != null) {
      result.addAll({'intergratedMessage': intergratedMessage});
    }
    if (metadataContentObjs != null) {
      result.addAll({
        'metadataContentObjs':
            metadataContentObjs!.map((x) => x.toMap()).toList()
      });
    }
    if (metadataContent != null) {
      result.addAll({'metadataContent': metadataContent});
    }
    if (code != null) {
      result.addAll({'code': code});
    }
    if (isActive != null) {
      result.addAll({'isActive': isActive});
    }
    if (order != null) {
      result.addAll({'order': order});
    }
    if (labelsObjs != null) {
      result.addAll({'labelsObjs': labelsObjs!.map((x) => x.toMap()).toList()});
    }
    if (labelsJson != null) {
      result.addAll({'labelsJson': labelsJson});
    }
    if (cLeft != null) {
      result.addAll({'cLeft': cLeft});
    }
    if (cRight != null) {
      result.addAll({'cRight': cRight});
    }
    if (cLevel != null) {
      result.addAll({'cLevel': cLevel});
    }

    return result;
  }

  factory ProjectTreeModel.fromMap(Map<String, dynamic> map) {
    return ProjectTreeModel(
      createdByUserId: map['createdByUserId'],
      lastModifiedByUserId: map['lastModifiedByUserId'],
      lastModifiedOnDate: map['lastModifiedOnDate'] != null
          ? DateTime.parse(map['lastModifiedOnDate'])
          : null,
      createdOnDate: map['createdOnDate'] != null
          ? DateTime.parse(map['createdOnDate'])
          : null,
      applicationId: map['applicationId'],
      id: map['id'] ?? '',
      name: map['name'],
      type: map['type'],
      completePath: map['completePath'],
      completeCode: map['completeCode'],
      completeName: map['completeName'],
      usage: map['usage'],
      parentId: map['parentId'],
      parentName: map['parentName'],
      partnerId: map['partnerId'],
      partnerName: map['partnerName'],
      comment: map['comment'],
      posx: map['posx'],
      posy: map['posy'],
      posz: map['posz'],
      ownerId: map['ownerId'],
      companyId: map['companyId'],
      scrapLocation: map['scrapLocation'],
      returnLocation: map['returnLocation'],
      removalStrategyId: map['removalStrategyId'],
      putawayStrategyId: map['putawayStrategyId'],
      integratedStatus: map['integratedStatus'],
      intergratedMessage: map['intergratedMessage'],
      metadataContentObjs: map['metadataContentObjs'] != null
          ? List<MetadataContentObjModel>.from(map['metadataContentObjs']
              ?.map((x) => MetadataContentObjModel.fromMap(x)))
          : null,
      metadataContent: map['metadataContent'],
      code: map['code'],
      isActive: map['isActive'],
      order: map['order']?.toInt(),
      labelsObjs: map['labelsObjs'] != null
          ? List<LabelsObjModel>.from(
              map['labelsObjs']?.map((x) => LabelsObjModel.fromMap(x)))
          : null,
      labelsJson: map['labelsJson'],
      cLeft: map['cLeft']?.toInt(),
      cRight: map['cRight']?.toInt(),
      cLevel: map['cLevel']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectTreeModel.fromJson(String source) =>
      ProjectTreeModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProjectTreeModel(createdByUserId: $createdByUserId, lastModifiedByUserId: $lastModifiedByUserId, lastModifiedOnDate: $lastModifiedOnDate, createdOnDate: $createdOnDate, applicationId: $applicationId, id: $id, name: $name, type: $type, completePath: $completePath, completeCode: $completeCode, completeName: $completeName, usage: $usage, parentId: $parentId, parentName: $parentName, partnerId: $partnerId, partnerName: $partnerName, comment: $comment, posx: $posx, posy: $posy, posz: $posz, ownerId: $ownerId, companyId: $companyId, scrapLocation: $scrapLocation, returnLocation: $returnLocation, removalStrategyId: $removalStrategyId, putawayStrategyId: $putawayStrategyId, integratedStatus: $integratedStatus, intergratedMessage: $intergratedMessage, metadataContentObjs: $metadataContentObjs, metadataContent: $metadataContent, code: $code, isActive: $isActive, order: $order, labelsObjs: $labelsObjs, labelsJson: $labelsJson, cLeft: $cLeft, cRight: $cRight, cLevel: $cLevel)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is ProjectTreeModel &&
        other.createdByUserId == createdByUserId &&
        other.lastModifiedByUserId == lastModifiedByUserId &&
        other.lastModifiedOnDate == lastModifiedOnDate &&
        other.createdOnDate == createdOnDate &&
        other.applicationId == applicationId &&
        other.id == id &&
        other.name == name &&
        other.type == type &&
        other.completePath == completePath &&
        other.completeCode == completeCode &&
        other.completeName == completeName &&
        other.usage == usage &&
        other.parentId == parentId &&
        other.parentName == parentName &&
        other.partnerId == partnerId &&
        other.partnerName == partnerName &&
        other.comment == comment &&
        other.posx == posx &&
        other.posy == posy &&
        other.posz == posz &&
        other.ownerId == ownerId &&
        other.companyId == companyId &&
        other.scrapLocation == scrapLocation &&
        other.returnLocation == returnLocation &&
        other.removalStrategyId == removalStrategyId &&
        other.putawayStrategyId == putawayStrategyId &&
        other.integratedStatus == integratedStatus &&
        other.intergratedMessage == intergratedMessage &&
        listEquals(other.metadataContentObjs, metadataContentObjs) &&
        other.metadataContent == metadataContent &&
        other.code == code &&
        other.isActive == isActive &&
        other.order == order &&
        listEquals(other.labelsObjs, labelsObjs) &&
        other.labelsJson == labelsJson &&
        other.cLeft == cLeft &&
        other.cRight == cRight &&
        other.cLevel == cLevel;
  }

  @override
  int get hashCode {
    return createdByUserId.hashCode ^
        lastModifiedByUserId.hashCode ^
        lastModifiedOnDate.hashCode ^
        createdOnDate.hashCode ^
        applicationId.hashCode ^
        id.hashCode ^
        name.hashCode ^
        type.hashCode ^
        completePath.hashCode ^
        completeCode.hashCode ^
        completeName.hashCode ^
        usage.hashCode ^
        parentId.hashCode ^
        parentName.hashCode ^
        partnerId.hashCode ^
        partnerName.hashCode ^
        comment.hashCode ^
        posx.hashCode ^
        posy.hashCode ^
        posz.hashCode ^
        ownerId.hashCode ^
        companyId.hashCode ^
        scrapLocation.hashCode ^
        returnLocation.hashCode ^
        removalStrategyId.hashCode ^
        putawayStrategyId.hashCode ^
        integratedStatus.hashCode ^
        intergratedMessage.hashCode ^
        metadataContentObjs.hashCode ^
        metadataContent.hashCode ^
        code.hashCode ^
        isActive.hashCode ^
        order.hashCode ^
        labelsObjs.hashCode ^
        labelsJson.hashCode ^
        cLeft.hashCode ^
        cRight.hashCode ^
        cLevel.hashCode;
  }
}
