import 'dart:convert';

class FieldSelectionValueModel {
  String? key;
  String? value;
  int? order;
  FieldSelectionValueModel({
    this.key,
    this.value,
    this.order,
  });

  FieldSelectionValueModel copyWith({
    String? key,
    String? value,
    int? order,
  }) {
    return FieldSelectionValueModel(
      key: key ?? this.key,
      value: value ?? this.value,
      order: order ?? this.order,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (key != null) {
      result.addAll({'key': key});
    }
    if (value != null) {
      result.addAll({'value': value});
    }
    if (order != null) {
      result.addAll({'order': order});
    }

    return result;
  }

  factory FieldSelectionValueModel.fromMap(Map<String, dynamic> map) {
    return FieldSelectionValueModel(
      key: map['key'],
      value: map['value'],
      order: map['order']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory FieldSelectionValueModel.fromJson(String source) =>
      FieldSelectionValueModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'FieldSelectionValue(key: $key, value: $value, order: $order)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is FieldSelectionValueModel &&
        other.key == key &&
        other.value == value &&
        other.order == order;
  }

  @override
  int get hashCode => key.hashCode ^ value.hashCode ^ order.hashCode;
}
