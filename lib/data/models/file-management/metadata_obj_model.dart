import 'dart:convert';

import 'package:collection/collection.dart';

import 'field_selection_value_model.dart';

class MetadataObjModel {
  String? id;
  String? fieldName;
  String? displayName;
  int? fieldType;
  String? defaultValue;
  String? fieldValues;
  List<FieldSelectionValueModel>? fieldSelectionValues;
  bool? allowNulls;
  int? sortOrder;
  MetadataObjModel({
    this.id,
    this.fieldName,
    this.displayName,
    this.fieldType,
    this.defaultValue,
    this.fieldValues,
    this.fieldSelectionValues,
    this.allowNulls,
    this.sortOrder,
  });

  MetadataObjModel copyWith({
    String? id,
    String? fieldName,
    String? displayName,
    int? fieldType,
    String? defaultValue,
    String? fieldValues,
    List<FieldSelectionValueModel>? fieldSelectionValues,
    bool? allowNulls,
    int? sortOrder,
  }) {
    return MetadataObjModel(
      id: id ?? this.id,
      fieldName: fieldName ?? this.fieldName,
      displayName: displayName ?? this.displayName,
      fieldType: fieldType ?? this.fieldType,
      defaultValue: defaultValue ?? this.defaultValue,
      fieldValues: fieldValues ?? this.fieldValues,
      fieldSelectionValues: fieldSelectionValues ?? this.fieldSelectionValues,
      allowNulls: allowNulls ?? this.allowNulls,
      sortOrder: sortOrder ?? this.sortOrder,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (id != null) {
      result.addAll({'id': id});
    }
    if (fieldName != null) {
      result.addAll({'fieldName': fieldName});
    }
    if (displayName != null) {
      result.addAll({'displayName': displayName});
    }
    if (fieldType != null) {
      result.addAll({'fieldType': fieldType});
    }
    if (defaultValue != null) {
      result.addAll({'defaultValue': defaultValue});
    }
    if (fieldValues != null) {
      result.addAll({'fieldValues': fieldValues});
    }
    if (fieldSelectionValues != null) {
      result.addAll({
        'fieldSelectionValues':
            fieldSelectionValues!.map((x) => x.toMap()).toList()
      });
    }
    if (allowNulls != null) {
      result.addAll({'allowNulls': allowNulls});
    }
    if (sortOrder != null) {
      result.addAll({'sortOrder': sortOrder});
    }

    return result;
  }

  factory MetadataObjModel.fromMap(Map<String, dynamic> map) {
    return MetadataObjModel(
      id: map['id'],
      fieldName: map['fieldName'],
      displayName: map['displayName'],
      fieldType: map['fieldType']?.toInt(),
      defaultValue: map['defaultValue'],
      fieldValues: map['fieldValues'],
      fieldSelectionValues: map['fieldSelectionValues'] != null
          ? List<FieldSelectionValueModel>.from(map['fieldSelectionValues']
              ?.map((x) => FieldSelectionValueModel.fromMap(x)))
          : null,
      allowNulls: map['allowNulls'],
      sortOrder: map['sortOrder']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory MetadataObjModel.fromJson(String source) =>
      MetadataObjModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MetadataObjModel(id: $id, fieldName: $fieldName, displayName: $displayName, fieldType: $fieldType, defaultValue: $defaultValue, fieldValues: $fieldValues, fieldSelectionValues: $fieldSelectionValues, allowNulls: $allowNulls, sortOrder: $sortOrder)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is MetadataObjModel &&
        other.id == id &&
        other.fieldName == fieldName &&
        other.displayName == displayName &&
        other.fieldType == fieldType &&
        other.defaultValue == defaultValue &&
        other.fieldValues == fieldValues &&
        listEquals(other.fieldSelectionValues, fieldSelectionValues) &&
        other.allowNulls == allowNulls &&
        other.sortOrder == sortOrder;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        fieldName.hashCode ^
        displayName.hashCode ^
        fieldType.hashCode ^
        defaultValue.hashCode ^
        fieldValues.hashCode ^
        fieldSelectionValues.hashCode ^
        allowNulls.hashCode ^
        sortOrder.hashCode;
  }
}
