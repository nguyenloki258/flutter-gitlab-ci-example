import 'dart:convert';

class MetadataModel {
  String? fileId;
  String? filePath;
  String? fileName;
  String? fileType;
  String? type;
  int? fileSize;
  String? absoluteUri;
  String? container;
  MetadataModel({
    this.fileId,
    this.filePath,
    this.fileName,
    this.fileType,
    this.type,
    this.fileSize,
    this.absoluteUri,
    this.container,
  });

  MetadataModel copyWith({
    String? fileId,
    String? filePath,
    String? fileName,
    String? fileType,
    String? type,
    int? fileSize,
    String? absoluteUri,
    String? container,
  }) {
    return MetadataModel(
      fileId: fileId ?? this.fileId,
      filePath: filePath ?? this.filePath,
      fileName: fileName ?? this.fileName,
      fileType: fileType ?? this.fileType,
      type: type ?? this.type,
      fileSize: fileSize ?? this.fileSize,
      absoluteUri: absoluteUri ?? this.absoluteUri,
      container: container ?? this.container,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (fileId != null) {
      result.addAll({'fileId': fileId});
    }
    if (filePath != null) {
      result.addAll({'filePath': filePath});
    }
    if (fileName != null) {
      result.addAll({'fileName': fileName});
    }
    if (fileType != null) {
      result.addAll({'fileType': fileType});
    }
    if (type != null) {
      result.addAll({'type': type});
    }
    if (fileSize != null) {
      result.addAll({'fileSize': fileSize});
    }
    if (absoluteUri != null) {
      result.addAll({'absoluteUri': absoluteUri});
    }
    if (container != null) {
      result.addAll({'container': container});
    }

    return result;
  }

  factory MetadataModel.fromMap(Map<String, dynamic> map) {
    return MetadataModel(
      fileId: map['fileId'],
      filePath: map['filePath'],
      fileName: map['fileName'],
      fileType: map['fileType'],
      type: map['type'],
      fileSize: map['fileSize']?.toInt(),
      absoluteUri: map['absoluteUri'],
      container: map['container'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MetadataModel.fromJson(String source) =>
      MetadataModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MetadataModel(fileId: $fileId, filePath: $filePath, fileName: $fileName, fileType: $fileType, type: $type, fileSize: $fileSize, absoluteUri: $absoluteUri, container: $container)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MetadataModel &&
        other.fileId == fileId &&
        other.filePath == filePath &&
        other.fileName == fileName &&
        other.fileType == fileType &&
        other.type == type &&
        other.fileSize == fileSize &&
        other.absoluteUri == absoluteUri &&
        other.container == container;
  }

  @override
  int get hashCode {
    return fileId.hashCode ^
        filePath.hashCode ^
        fileName.hashCode ^
        fileType.hashCode ^
        type.hashCode ^
        fileSize.hashCode ^
        absoluteUri.hashCode ^
        container.hashCode;
  }
}
