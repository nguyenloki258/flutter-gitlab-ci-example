import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:serpapp/data/models/file-management/metadata_model.dart';
import 'package:serpapp/data/models/file-management/metadata_obj_model.dart';

import 'acl_obj_model.dart';
import 'ancestor_model.dart';

class FileModel {
  String? objectId;
  String? objectType;
  List<String>? relatedObjectIdList;
  String? relatedBaseCategory;
  String? relatedFondId;
  String? relatedArchiveTypeId;
  String? relatedRecordCollectionId;
  String? relatedRecordId;
  String? relatedProjectId;
  String? relatedTaskId;
  String? relatedRespartnerId;
  String? relatedObject1;
  String? relatedObject1Type;
  String? relatedObject2;
  String? relatedObject2Type;
  String? id;
  String? name;
  String? imageUrl;
  List<AclObjectModel>? labelsObjs;
  String? description;
  int? nodeTypeObj;
  String? parentId;
  String? metaContentType;
  String? type;
  List<MetadataObjModel>? metadataContentObj;
  List<MetadataObjModel>? metadataObj;
  String? ownerId;
  bool? isDeleted;
  bool? isPublish;
  bool? isSearchable;
  List<AncestorModel>? ancestors;
  List<AncestorModel>? descendents;
  AclObjectModel? aclObject;
  String? relatedFileId;
  List<String>? relatedRoleCodeObjects;
  String? relatedRoleCode;
  bool? hasChildren;
  bool? allowToView;
  bool? allowToDelete;
  bool? allowToMove;
  bool? allowToSharing;
  bool? allowToModify;
  bool? allowToSetPermission;
  bool? allowToSetRoleMapping;
  bool? allowToChangeOwner;
  String? createdByUserId;
  String? lastModifiedByUserId;
  DateTime? lastModifiedOnDate;
  DateTime? createdOnDate;
  String? applicationId;
  int? cLeft;
  int? cRight;
  int? cLevel;
  String? completePath;
  String? completeName;
  String? fileUrl;
  MetadataModel? metadata;
  String? metaContent;
  FileModel({
    this.objectId,
    this.objectType,
    this.relatedObjectIdList,
    this.relatedBaseCategory,
    this.relatedFondId,
    this.relatedArchiveTypeId,
    this.relatedRecordCollectionId,
    this.relatedRecordId,
    this.relatedProjectId,
    this.relatedTaskId,
    this.relatedRespartnerId,
    this.relatedObject1,
    this.relatedObject1Type,
    this.relatedObject2,
    this.relatedObject2Type,
    this.id,
    this.name,
    this.imageUrl,
    this.labelsObjs,
    this.description,
    this.nodeTypeObj,
    this.parentId,
    this.metaContentType,
    this.type,
    this.metadataContentObj,
    this.metadataObj,
    this.ownerId,
    this.isDeleted,
    this.isPublish,
    this.isSearchable,
    this.ancestors,
    this.descendents,
    this.aclObject,
    this.relatedFileId,
    this.relatedRoleCodeObjects,
    this.relatedRoleCode,
    this.hasChildren,
    this.allowToView,
    this.allowToDelete,
    this.allowToMove,
    this.allowToSharing,
    this.allowToModify,
    this.allowToSetPermission,
    this.allowToSetRoleMapping,
    this.allowToChangeOwner,
    this.createdByUserId,
    this.lastModifiedByUserId,
    this.lastModifiedOnDate,
    this.createdOnDate,
    this.applicationId,
    this.cLeft,
    this.cRight,
    this.cLevel,
    this.completePath,
    this.completeName,
    this.fileUrl,
    this.metadata,
    this.metaContent,
  });

  FileModel copyWith({
    String? objectId,
    String? objectType,
    List<String>? relatedObjectIdList,
    String? relatedBaseCategory,
    String? relatedFondId,
    String? relatedArchiveTypeId,
    String? relatedRecordCollectionId,
    String? relatedRecordId,
    String? relatedProjectId,
    String? relatedTaskId,
    String? relatedRespartnerId,
    String? relatedObject1,
    String? relatedObject1Type,
    String? relatedObject2,
    String? relatedObject2Type,
    String? id,
    String? name,
    String? imageUrl,
    List<AclObjectModel>? labelsObjs,
    String? description,
    int? nodeTypeObj,
    String? parentId,
    String? metaContentType,
    String? type,
    List<MetadataObjModel>? metadataContentObj,
    List<MetadataObjModel>? metadataObj,
    String? ownerId,
    bool? isDeleted,
    bool? isPublish,
    bool? isSearchable,
    List<AncestorModel>? ancestors,
    List<AncestorModel>? descendents,
    AclObjectModel? aclObject,
    String? relatedFileId,
    List<String>? relatedRoleCodeObjects,
    String? relatedRoleCode,
    bool? hasChildren,
    bool? allowToView,
    bool? allowToDelete,
    bool? allowToMove,
    bool? allowToSharing,
    bool? allowToModify,
    bool? allowToSetPermission,
    bool? allowToSetRoleMapping,
    bool? allowToChangeOwner,
    String? createdByUserId,
    String? lastModifiedByUserId,
    DateTime? lastModifiedOnDate,
    DateTime? createdOnDate,
    String? applicationId,
    int? cLeft,
    int? cRight,
    int? cLevel,
    String? completePath,
    String? completeName,
    String? fileUrl,
    MetadataModel? metadata,
    String? metaContent,
  }) {
    return FileModel(
      objectId: objectId ?? this.objectId,
      objectType: objectType ?? this.objectType,
      relatedObjectIdList: relatedObjectIdList ?? this.relatedObjectIdList,
      relatedBaseCategory: relatedBaseCategory ?? this.relatedBaseCategory,
      relatedFondId: relatedFondId ?? this.relatedFondId,
      relatedArchiveTypeId: relatedArchiveTypeId ?? this.relatedArchiveTypeId,
      relatedRecordCollectionId:
          relatedRecordCollectionId ?? this.relatedRecordCollectionId,
      relatedRecordId: relatedRecordId ?? this.relatedRecordId,
      relatedProjectId: relatedProjectId ?? this.relatedProjectId,
      relatedTaskId: relatedTaskId ?? this.relatedTaskId,
      relatedRespartnerId: relatedRespartnerId ?? this.relatedRespartnerId,
      relatedObject1: relatedObject1 ?? this.relatedObject1,
      relatedObject1Type: relatedObject1Type ?? this.relatedObject1Type,
      relatedObject2: relatedObject2 ?? this.relatedObject2,
      relatedObject2Type: relatedObject2Type ?? this.relatedObject2Type,
      id: id ?? this.id,
      name: name ?? this.name,
      imageUrl: imageUrl ?? this.imageUrl,
      labelsObjs: labelsObjs ?? this.labelsObjs,
      description: description ?? this.description,
      nodeTypeObj: nodeTypeObj ?? this.nodeTypeObj,
      parentId: parentId ?? this.parentId,
      metaContentType: metaContentType ?? this.metaContentType,
      type: type ?? this.type,
      metadataContentObj: metadataContentObj ?? this.metadataContentObj,
      metadataObj: metadataObj ?? this.metadataObj,
      ownerId: ownerId ?? this.ownerId,
      isDeleted: isDeleted ?? this.isDeleted,
      isPublish: isPublish ?? this.isPublish,
      isSearchable: isSearchable ?? this.isSearchable,
      ancestors: ancestors ?? this.ancestors,
      descendents: descendents ?? this.descendents,
      aclObject: aclObject ?? this.aclObject,
      relatedFileId: relatedFileId ?? this.relatedFileId,
      relatedRoleCodeObjects:
          relatedRoleCodeObjects ?? this.relatedRoleCodeObjects,
      relatedRoleCode: relatedRoleCode ?? this.relatedRoleCode,
      hasChildren: hasChildren ?? this.hasChildren,
      allowToView: allowToView ?? this.allowToView,
      allowToDelete: allowToDelete ?? this.allowToDelete,
      allowToMove: allowToMove ?? this.allowToMove,
      allowToSharing: allowToSharing ?? this.allowToSharing,
      allowToModify: allowToModify ?? this.allowToModify,
      allowToSetPermission: allowToSetPermission ?? this.allowToSetPermission,
      allowToSetRoleMapping:
          allowToSetRoleMapping ?? this.allowToSetRoleMapping,
      allowToChangeOwner: allowToChangeOwner ?? this.allowToChangeOwner,
      createdByUserId: createdByUserId ?? this.createdByUserId,
      lastModifiedByUserId: lastModifiedByUserId ?? this.lastModifiedByUserId,
      lastModifiedOnDate: lastModifiedOnDate ?? this.lastModifiedOnDate,
      createdOnDate: createdOnDate ?? this.createdOnDate,
      applicationId: applicationId ?? this.applicationId,
      cLeft: cLeft ?? this.cLeft,
      cRight: cRight ?? this.cRight,
      cLevel: cLevel ?? this.cLevel,
      completePath: completePath ?? this.completePath,
      completeName: completeName ?? this.completeName,
      fileUrl: fileUrl ?? this.fileUrl,
      metadata: metadata ?? this.metadata,
      metaContent: metaContent ?? this.metaContent,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectType != null) {
      result.addAll({'objectType': objectType});
    }
    if (relatedObjectIdList != null) {
      result.addAll({'relatedObjectIdList': relatedObjectIdList});
    }
    if (relatedBaseCategory != null) {
      result.addAll({'relatedBaseCategory': relatedBaseCategory});
    }
    if (relatedFondId != null) {
      result.addAll({'relatedFondId': relatedFondId});
    }
    if (relatedArchiveTypeId != null) {
      result.addAll({'relatedArchiveTypeId': relatedArchiveTypeId});
    }
    if (relatedRecordCollectionId != null) {
      result.addAll({'relatedRecordCollectionId': relatedRecordCollectionId});
    }
    if (relatedRecordId != null) {
      result.addAll({'relatedRecordId': relatedRecordId});
    }
    if (relatedProjectId != null) {
      result.addAll({'relatedProjectId': relatedProjectId});
    }
    if (relatedTaskId != null) {
      result.addAll({'relatedTaskId': relatedTaskId});
    }
    if (relatedRespartnerId != null) {
      result.addAll({'relatedRespartnerId': relatedRespartnerId});
    }
    if (relatedObject1 != null) {
      result.addAll({'relatedObject1': relatedObject1});
    }
    if (relatedObject1Type != null) {
      result.addAll({'relatedObject1Type': relatedObject1Type});
    }
    if (relatedObject2 != null) {
      result.addAll({'relatedObject2': relatedObject2});
    }
    if (relatedObject2Type != null) {
      result.addAll({'relatedObject2Type': relatedObject2Type});
    }
    if (id != null) {
      result.addAll({'id': id});
    }
    if (name != null) {
      result.addAll({'name': name});
    }
    if (imageUrl != null) {
      result.addAll({'imageUrl': imageUrl});
    }
    if (labelsObjs != null) {
      result.addAll({'labelsObjs': labelsObjs!.map((x) => x.toMap()).toList()});
    }
    if (description != null) {
      result.addAll({'description': description});
    }
    if (nodeTypeObj != null) {
      result.addAll({'nodeTypeObj': nodeTypeObj});
    }
    if (parentId != null) {
      result.addAll({'parentId': parentId});
    }
    if (metaContentType != null) {
      result.addAll({'metaContentType': metaContentType});
    }
    if (type != null) {
      result.addAll({'type': type});
    }
    if (metadataContentObj != null) {
      result.addAll({
        'metadataContentObj': metadataContentObj!.map((x) => x.toMap()).toList()
      });
    }
    if (metadataObj != null) {
      result
          .addAll({'metadataObj': metadataObj!.map((x) => x.toMap()).toList()});
    }
    if (ownerId != null) {
      result.addAll({'ownerId': ownerId});
    }
    if (isDeleted != null) {
      result.addAll({'isDeleted': isDeleted});
    }
    if (isPublish != null) {
      result.addAll({'isPublish': isPublish});
    }
    if (isSearchable != null) {
      result.addAll({'isSearchable': isSearchable});
    }
    if (ancestors != null) {
      result.addAll({'ancestors': ancestors!.map((x) => x.toMap()).toList()});
    }
    if (descendents != null) {
      result
          .addAll({'descendents': descendents!.map((x) => x.toMap()).toList()});
    }
    if (aclObject != null) {
      result.addAll({'aclObject': aclObject!.toMap()});
    }
    if (relatedFileId != null) {
      result.addAll({'relatedFileId': relatedFileId});
    }
    if (relatedRoleCodeObjects != null) {
      result.addAll({'relatedRoleCodeObjects': relatedRoleCodeObjects});
    }
    if (relatedRoleCode != null) {
      result.addAll({'relatedRoleCode': relatedRoleCode});
    }
    if (hasChildren != null) {
      result.addAll({'hasChildren': hasChildren});
    }
    if (allowToView != null) {
      result.addAll({'allowToView': allowToView});
    }
    if (allowToDelete != null) {
      result.addAll({'allowToDelete': allowToDelete});
    }
    if (allowToMove != null) {
      result.addAll({'allowToMove': allowToMove});
    }
    if (allowToSharing != null) {
      result.addAll({'allowToSharing': allowToSharing});
    }
    if (allowToModify != null) {
      result.addAll({'allowToModify': allowToModify});
    }
    if (allowToSetPermission != null) {
      result.addAll({'allowToSetPermission': allowToSetPermission});
    }
    if (allowToSetRoleMapping != null) {
      result.addAll({'allowToSetRoleMapping': allowToSetRoleMapping});
    }
    if (allowToChangeOwner != null) {
      result.addAll({'allowToChangeOwner': allowToChangeOwner});
    }
    if (createdByUserId != null) {
      result.addAll({'createdByUserId': createdByUserId});
    }
    if (lastModifiedByUserId != null) {
      result.addAll({'lastModifiedByUserId': lastModifiedByUserId});
    }
    if (lastModifiedOnDate != null) {
      result.addAll(
          {'lastModifiedOnDate': lastModifiedOnDate!.millisecondsSinceEpoch});
    }
    if (createdOnDate != null) {
      result.addAll({'createdOnDate': createdOnDate!.millisecondsSinceEpoch});
    }
    if (applicationId != null) {
      result.addAll({'applicationId': applicationId});
    }
    if (cLeft != null) {
      result.addAll({'cLeft': cLeft});
    }
    if (cRight != null) {
      result.addAll({'cRight': cRight});
    }
    if (cLevel != null) {
      result.addAll({'cLevel': cLevel});
    }
    if (completePath != null) {
      result.addAll({'completePath': completePath});
    }
    if (completeName != null) {
      result.addAll({'completeName': completeName});
    }
    if (fileUrl != null) {
      result.addAll({'fileUrl': fileUrl});
    }
    if (metadata != null) {
      result.addAll({'metadata': metadata!.toMap()});
    }
    if (metaContent != null) {
      result.addAll({'metaContent': metaContent});
    }

    return result;
  }

  factory FileModel.fromMap(Map<String, dynamic> map) {
    return FileModel(
      objectId: map['objectId'],
      objectType: map['objectType'],
      relatedObjectIdList: List<String>.from(map['relatedObjectIdList']),
      relatedBaseCategory: map['relatedBaseCategory'],
      relatedFondId: map['relatedFondId'],
      relatedArchiveTypeId: map['relatedArchiveTypeId'],
      relatedRecordCollectionId: map['relatedRecordCollectionId'],
      relatedRecordId: map['relatedRecordId'],
      relatedProjectId: map['relatedProjectId'],
      relatedTaskId: map['relatedTaskId'],
      relatedRespartnerId: map['relatedRespartnerId'],
      relatedObject1: map['relatedObject1'],
      relatedObject1Type: map['relatedObject1Type'],
      relatedObject2: map['relatedObject2'],
      relatedObject2Type: map['relatedObject2Type'],
      id: map['id'],
      name: map['name'],
      imageUrl: map['imageUrl'],
      labelsObjs: map['labelsObjs'] != null
          ? List<AclObjectModel>.from(
              map['labelsObjs']?.map((x) => AclObjectModel.fromMap(x)))
          : null,
      description: map['description'],
      nodeTypeObj: map['nodeTypeObj']?.toInt(),
      parentId: map['parentId'],
      metaContentType: map['metaContentType'],
      type: map['type'],
      metadataContentObj: map['metadataContentObj'] != null
          ? List<MetadataObjModel>.from(map['metadataContentObj']
              ?.map((x) => MetadataObjModel.fromMap(x)))
          : null,
      metadataObj: map['metadataObj'] != null
          ? List<MetadataObjModel>.from(
              map['metadataObj']?.map((x) => MetadataObjModel.fromMap(x)))
          : null,
      ownerId: map['ownerId'],
      isDeleted: map['isDeleted'],
      isPublish: map['isPublish'],
      isSearchable: map['isSearchable'],
      ancestors: map['ancestors'] != null
          ? List<AncestorModel>.from(
              map['ancestors']?.map((x) => AncestorModel.fromMap(x)))
          : null,
      descendents: map['descendents'] != null
          ? List<AncestorModel>.from(
              map['descendents']?.map((x) => AncestorModel.fromMap(x)))
          : null,
      aclObject: map['aclObject'] != null
          ? AclObjectModel.fromMap(map['aclObject'])
          : null,
      relatedFileId: map['relatedFileId'],
      relatedRoleCodeObjects: map['relatedRoleCodeObjects'] != null
          ? List<String>.from(map['relatedRoleCodeObjects'])
          : null,
      relatedRoleCode: map['relatedRoleCode'],
      hasChildren: map['hasChildren'],
      allowToView: map['allowToView'],
      allowToDelete: map['allowToDelete'],
      allowToMove: map['allowToMove'],
      allowToSharing: map['allowToSharing'],
      allowToModify: map['allowToModify'],
      allowToSetPermission: map['allowToSetPermission'],
      allowToSetRoleMapping: map['allowToSetRoleMapping'],
      allowToChangeOwner: map['allowToChangeOwner'],
      createdByUserId: map['createdByUserId'],
      lastModifiedByUserId: map['lastModifiedByUserId'],
      lastModifiedOnDate: map['lastModifiedOnDate'] != null
          ? DateTime.parse(map['lastModifiedOnDate'])
          : null,
      createdOnDate: map['createdOnDate'] != null
          ? DateTime.parse(map['createdOnDate'])
          : null,
      applicationId: map['applicationId'],
      cLeft: map['cLeft']?.toInt(),
      cRight: map['cRight']?.toInt(),
      cLevel: map['cLevel']?.toInt(),
      completePath: map['completePath'],
      completeName: map['completeName'],
      fileUrl: map['fileUrl'],
      metadata: map['metadata'] != null
          ? MetadataModel.fromMap(map['metadata'])
          : null,
      metaContent: map['metaContent'],
    );
  }

  String toJson() => json.encode(toMap());

  factory FileModel.fromJson(String source) =>
      FileModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'FileModel(objectId: $objectId, objectType: $objectType, relatedObjectIdList: $relatedObjectIdList, relatedBaseCategory: $relatedBaseCategory, relatedFondId: $relatedFondId, relatedArchiveTypeId: $relatedArchiveTypeId, relatedRecordCollectionId: $relatedRecordCollectionId, relatedRecordId: $relatedRecordId, relatedProjectId: $relatedProjectId, relatedTaskId: $relatedTaskId, relatedRespartnerId: $relatedRespartnerId, relatedObject1: $relatedObject1, relatedObject1Type: $relatedObject1Type, relatedObject2: $relatedObject2, relatedObject2Type: $relatedObject2Type, id: $id, name: $name, imageUrl: $imageUrl, labelsObjs: $labelsObjs, description: $description, nodeTypeObj: $nodeTypeObj, parentId: $parentId, metaContentType: $metaContentType, type: $type, metadataContentObj: $metadataContentObj, metadataObj: $metadataObj, ownerId: $ownerId, isDeleted: $isDeleted, isPublish: $isPublish, isSearchable: $isSearchable, ancestors: $ancestors, descendents: $descendents, aclObject: $aclObject, relatedFileId: $relatedFileId, relatedRoleCodeObjects: $relatedRoleCodeObjects, relatedRoleCode: $relatedRoleCode, hasChildren: $hasChildren, allowToView: $allowToView, allowToDelete: $allowToDelete, allowToMove: $allowToMove, allowToSharing: $allowToSharing, allowToModify: $allowToModify, allowToSetPermission: $allowToSetPermission, allowToSetRoleMapping: $allowToSetRoleMapping, allowToChangeOwner: $allowToChangeOwner, createdByUserId: $createdByUserId, lastModifiedByUserId: $lastModifiedByUserId, lastModifiedOnDate: $lastModifiedOnDate, createdOnDate: $createdOnDate, applicationId: $applicationId, cLeft: $cLeft, cRight: $cRight, cLevel: $cLevel, completePath: $completePath, completeName: $completeName, fileUrl: $fileUrl, metadata: $metadata, metaContent: $metaContent)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is FileModel &&
        other.objectId == objectId &&
        other.objectType == objectType &&
        listEquals(other.relatedObjectIdList, relatedObjectIdList) &&
        other.relatedBaseCategory == relatedBaseCategory &&
        other.relatedFondId == relatedFondId &&
        other.relatedArchiveTypeId == relatedArchiveTypeId &&
        other.relatedRecordCollectionId == relatedRecordCollectionId &&
        other.relatedRecordId == relatedRecordId &&
        other.relatedProjectId == relatedProjectId &&
        other.relatedTaskId == relatedTaskId &&
        other.relatedRespartnerId == relatedRespartnerId &&
        other.relatedObject1 == relatedObject1 &&
        other.relatedObject1Type == relatedObject1Type &&
        other.relatedObject2 == relatedObject2 &&
        other.relatedObject2Type == relatedObject2Type &&
        other.id == id &&
        other.name == name &&
        other.imageUrl == imageUrl &&
        listEquals(other.labelsObjs, labelsObjs) &&
        other.description == description &&
        other.nodeTypeObj == nodeTypeObj &&
        other.parentId == parentId &&
        other.metaContentType == metaContentType &&
        other.type == type &&
        listEquals(other.metadataContentObj, metadataContentObj) &&
        listEquals(other.metadataObj, metadataObj) &&
        other.ownerId == ownerId &&
        other.isDeleted == isDeleted &&
        other.isPublish == isPublish &&
        other.isSearchable == isSearchable &&
        listEquals(other.ancestors, ancestors) &&
        listEquals(other.descendents, descendents) &&
        other.aclObject == aclObject &&
        other.relatedFileId == relatedFileId &&
        listEquals(other.relatedRoleCodeObjects, relatedRoleCodeObjects) &&
        other.relatedRoleCode == relatedRoleCode &&
        other.hasChildren == hasChildren &&
        other.allowToView == allowToView &&
        other.allowToDelete == allowToDelete &&
        other.allowToMove == allowToMove &&
        other.allowToSharing == allowToSharing &&
        other.allowToModify == allowToModify &&
        other.allowToSetPermission == allowToSetPermission &&
        other.allowToSetRoleMapping == allowToSetRoleMapping &&
        other.allowToChangeOwner == allowToChangeOwner &&
        other.createdByUserId == createdByUserId &&
        other.lastModifiedByUserId == lastModifiedByUserId &&
        other.lastModifiedOnDate == lastModifiedOnDate &&
        other.createdOnDate == createdOnDate &&
        other.applicationId == applicationId &&
        other.cLeft == cLeft &&
        other.cRight == cRight &&
        other.cLevel == cLevel &&
        other.completePath == completePath &&
        other.completeName == completeName &&
        other.fileUrl == fileUrl &&
        other.metadata == metadata &&
        other.metaContent == metaContent;
  }

  @override
  int get hashCode {
    return objectId.hashCode ^
        objectType.hashCode ^
        relatedObjectIdList.hashCode ^
        relatedBaseCategory.hashCode ^
        relatedFondId.hashCode ^
        relatedArchiveTypeId.hashCode ^
        relatedRecordCollectionId.hashCode ^
        relatedRecordId.hashCode ^
        relatedProjectId.hashCode ^
        relatedTaskId.hashCode ^
        relatedRespartnerId.hashCode ^
        relatedObject1.hashCode ^
        relatedObject1Type.hashCode ^
        relatedObject2.hashCode ^
        relatedObject2Type.hashCode ^
        id.hashCode ^
        name.hashCode ^
        imageUrl.hashCode ^
        labelsObjs.hashCode ^
        description.hashCode ^
        nodeTypeObj.hashCode ^
        parentId.hashCode ^
        metaContentType.hashCode ^
        type.hashCode ^
        metadataContentObj.hashCode ^
        metadataObj.hashCode ^
        ownerId.hashCode ^
        isDeleted.hashCode ^
        isPublish.hashCode ^
        isSearchable.hashCode ^
        ancestors.hashCode ^
        descendents.hashCode ^
        aclObject.hashCode ^
        relatedFileId.hashCode ^
        relatedRoleCodeObjects.hashCode ^
        relatedRoleCode.hashCode ^
        hasChildren.hashCode ^
        allowToView.hashCode ^
        allowToDelete.hashCode ^
        allowToMove.hashCode ^
        allowToSharing.hashCode ^
        allowToModify.hashCode ^
        allowToSetPermission.hashCode ^
        allowToSetRoleMapping.hashCode ^
        allowToChangeOwner.hashCode ^
        createdByUserId.hashCode ^
        lastModifiedByUserId.hashCode ^
        lastModifiedOnDate.hashCode ^
        createdOnDate.hashCode ^
        applicationId.hashCode ^
        cLeft.hashCode ^
        cRight.hashCode ^
        cLevel.hashCode ^
        completePath.hashCode ^
        completeName.hashCode ^
        fileUrl.hashCode ^
        metadata.hashCode ^
        metaContent.hashCode;
  }
}
