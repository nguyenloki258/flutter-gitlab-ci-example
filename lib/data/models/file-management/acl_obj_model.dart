import 'dart:convert';

import 'package:collection/collection.dart';

import 'acl_item_model.dart';

class AclObjectModel {
  String? objectId;
  List<AclItemModel>? aclItems;
  AclObjectModel({
    this.objectId,
    this.aclItems,
  });

  AclObjectModel copyWith({
    String? objectId,
    List<AclItemModel>? aclItems,
  }) {
    return AclObjectModel(
      objectId: objectId ?? this.objectId,
      aclItems: aclItems ?? this.aclItems,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (aclItems != null) {
      result.addAll({'aclItems': aclItems!.map((x) => x.toMap()).toList()});
    }

    return result;
  }

  factory AclObjectModel.fromMap(Map<String, dynamic> map) {
    return AclObjectModel(
      objectId: map['objectId'],
      aclItems: map['aclItems'] != null
          ? List<AclItemModel>.from(
              map['aclItems']?.map((x) => AclItemModel.fromMap(x)))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory AclObjectModel.fromJson(String source) =>
      AclObjectModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'AclObjectModel(objectId: $objectId, aclItems: $aclItems)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is AclObjectModel &&
        other.objectId == objectId &&
        listEquals(other.aclItems, aclItems);
  }

  @override
  int get hashCode => objectId.hashCode ^ aclItems.hashCode;
}
