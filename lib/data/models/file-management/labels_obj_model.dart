import 'dart:convert';

class LabelsObjModel {
  String? objectId;
  String? objectCode;
  String? objectName;
  String? color;
  LabelsObjModel({
    this.objectId,
    this.objectCode,
    this.objectName,
    this.color,
  });

  LabelsObjModel copyWith({
    String? objectId,
    String? objectCode,
    String? objectName,
    String? color,
  }) {
    return LabelsObjModel(
      objectId: objectId ?? this.objectId,
      objectCode: objectCode ?? this.objectCode,
      objectName: objectName ?? this.objectName,
      color: color ?? this.color,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectCode != null) {
      result.addAll({'objectCode': objectCode});
    }
    if (objectName != null) {
      result.addAll({'objectName': objectName});
    }
    if (color != null) {
      result.addAll({'color': color});
    }

    return result;
  }

  factory LabelsObjModel.fromMap(Map<String, dynamic> map) {
    return LabelsObjModel(
      objectId: map['objectId'],
      objectCode: map['objectCode'],
      objectName: map['objectName'],
      color: map['color'],
    );
  }

  String toJson() => json.encode(toMap());

  factory LabelsObjModel.fromJson(String source) =>
      LabelsObjModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'LabelsObjModel(objectId: $objectId, objectCode: $objectCode, objectName: $objectName, color: $color)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is LabelsObjModel &&
        other.objectId == objectId &&
        other.objectCode == objectCode &&
        other.objectName == objectName &&
        other.color == color;
  }

  @override
  int get hashCode {
    return objectId.hashCode ^
        objectCode.hashCode ^
        objectName.hashCode ^
        color.hashCode;
  }
}
