import 'dart:convert';

class AncestorModel {
  String id;
  String? name;
  String? imageUrl;
  String? parentId;
  int? nodeTypeObj;
  String? createdByUserId;
  String? lastModifiedByUserId;
  DateTime? lastModifiedOnDate;
  DateTime? createdOnDate;
  int? cLeft;
  int? cRight;
  int? cLevel;
  String? completePath;
  String? completeName;
  AncestorModel({
    required this.id,
    this.name,
    this.imageUrl,
    this.parentId,
    this.nodeTypeObj,
    this.createdByUserId,
    this.lastModifiedByUserId,
    this.lastModifiedOnDate,
    this.createdOnDate,
    this.cLeft,
    this.cRight,
    this.cLevel,
    this.completePath,
    this.completeName,
  });

  AncestorModel copyWith({
    String? id,
    String? name,
    String? imageUrl,
    String? parentId,
    int? nodeTypeObj,
    String? createdByUserId,
    String? lastModifiedByUserId,
    DateTime? lastModifiedOnDate,
    DateTime? createdOnDate,
    int? cLeft,
    int? cRight,
    int? cLevel,
    String? completePath,
    String? completeName,
  }) {
    return AncestorModel(
      id: id ?? this.id,
      name: name ?? this.name,
      imageUrl: imageUrl ?? this.imageUrl,
      parentId: parentId ?? this.parentId,
      nodeTypeObj: nodeTypeObj ?? this.nodeTypeObj,
      createdByUserId: createdByUserId ?? this.createdByUserId,
      lastModifiedByUserId: lastModifiedByUserId ?? this.lastModifiedByUserId,
      lastModifiedOnDate: lastModifiedOnDate ?? this.lastModifiedOnDate,
      createdOnDate: createdOnDate ?? this.createdOnDate,
      cLeft: cLeft ?? this.cLeft,
      cRight: cRight ?? this.cRight,
      cLevel: cLevel ?? this.cLevel,
      completePath: completePath ?? this.completePath,
      completeName: completeName ?? this.completeName,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'id': id});
    if (name != null) {
      result.addAll({'name': name});
    }
    if (imageUrl != null) {
      result.addAll({'imageUrl': imageUrl});
    }
    if (parentId != null) {
      result.addAll({'parentId': parentId});
    }
    if (nodeTypeObj != null) {
      result.addAll({'nodeTypeObj': nodeTypeObj});
    }
    if (createdByUserId != null) {
      result.addAll({'createdByUserId': createdByUserId});
    }
    if (lastModifiedByUserId != null) {
      result.addAll({'lastModifiedByUserId': lastModifiedByUserId});
    }
    if (lastModifiedOnDate != null) {
      result.addAll(
          {'lastModifiedOnDate': lastModifiedOnDate!.millisecondsSinceEpoch});
    }
    if (createdOnDate != null) {
      result.addAll({'createdOnDate': createdOnDate!.millisecondsSinceEpoch});
    }
    if (cLeft != null) {
      result.addAll({'cLeft': cLeft});
    }
    if (cRight != null) {
      result.addAll({'cRight': cRight});
    }
    if (cLevel != null) {
      result.addAll({'cLevel': cLevel});
    }
    if (completePath != null) {
      result.addAll({'completePath': completePath});
    }
    if (completeName != null) {
      result.addAll({'completeName': completeName});
    }

    return result;
  }

  factory AncestorModel.fromMap(Map<String, dynamic> map) {
    return AncestorModel(
      id: map['id'] ?? '',
      name: map['name'],
      imageUrl: map['imageUrl'],
      parentId: map['parentId'],
      nodeTypeObj: map['nodeTypeObj']?.toInt(),
      createdByUserId: map['createdByUserId'],
      lastModifiedByUserId: map['lastModifiedByUserId'],
      lastModifiedOnDate: map['lastModifiedOnDate'] != null
          ? DateTime.fromMillisecondsSinceEpoch(map['lastModifiedOnDate'])
          : null,
      createdOnDate: map['createdOnDate'] != null
          ? DateTime.fromMillisecondsSinceEpoch(map['createdOnDate'])
          : null,
      cLeft: map['cLeft']?.toInt(),
      cRight: map['cRight']?.toInt(),
      cLevel: map['cLevel']?.toInt(),
      completePath: map['completePath'],
      completeName: map['completeName'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AncestorModel.fromJson(String source) =>
      AncestorModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AncestorModel(id: $id, name: $name, imageUrl: $imageUrl, parentId: $parentId, nodeTypeObj: $nodeTypeObj, createdByUserId: $createdByUserId, lastModifiedByUserId: $lastModifiedByUserId, lastModifiedOnDate: $lastModifiedOnDate, createdOnDate: $createdOnDate, cLeft: $cLeft, cRight: $cRight, cLevel: $cLevel, completePath: $completePath, completeName: $completeName)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AncestorModel &&
        other.id == id &&
        other.name == name &&
        other.imageUrl == imageUrl &&
        other.parentId == parentId &&
        other.nodeTypeObj == nodeTypeObj &&
        other.createdByUserId == createdByUserId &&
        other.lastModifiedByUserId == lastModifiedByUserId &&
        other.lastModifiedOnDate == lastModifiedOnDate &&
        other.createdOnDate == createdOnDate &&
        other.cLeft == cLeft &&
        other.cRight == cRight &&
        other.cLevel == cLevel &&
        other.completePath == completePath &&
        other.completeName == completeName;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        imageUrl.hashCode ^
        parentId.hashCode ^
        nodeTypeObj.hashCode ^
        createdByUserId.hashCode ^
        lastModifiedByUserId.hashCode ^
        lastModifiedOnDate.hashCode ^
        createdOnDate.hashCode ^
        cLeft.hashCode ^
        cRight.hashCode ^
        cLevel.hashCode ^
        completePath.hashCode ^
        completeName.hashCode;
  }
}
