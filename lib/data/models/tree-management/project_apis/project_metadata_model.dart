import 'dart:convert';

class ProjectMetadataModel {
  String? id;
  String? fieldName;
  String? displayName;
  int? fieldType;
  String? defaultValue;
  String? fieldValues;
  // List<FieldSelectionValue>? fieldSelectionValues;
  bool? allowNulls;
  int? sortOrder;
  ProjectMetadataModel({
    this.id,
    this.fieldName,
    this.displayName,
    this.fieldType,
    this.defaultValue,
    this.fieldValues,
    this.allowNulls,
    this.sortOrder,
  });

  ProjectMetadataModel copyWith({
    String? id,
    String? fieldName,
    String? displayName,
    int? fieldType,
    String? defaultValue,
    String? fieldValues,
    bool? allowNulls,
    int? sortOrder,
  }) {
    return ProjectMetadataModel(
      id: id ?? this.id,
      fieldName: fieldName ?? this.fieldName,
      displayName: displayName ?? this.displayName,
      fieldType: fieldType ?? this.fieldType,
      defaultValue: defaultValue ?? this.defaultValue,
      fieldValues: fieldValues ?? this.fieldValues,
      allowNulls: allowNulls ?? this.allowNulls,
      sortOrder: sortOrder ?? this.sortOrder,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (id != null) {
      result.addAll({'id': id});
    }
    if (fieldName != null) {
      result.addAll({'fieldName': fieldName});
    }
    if (displayName != null) {
      result.addAll({'displayName': displayName});
    }
    if (fieldType != null) {
      result.addAll({'fieldType': fieldType});
    }
    if (defaultValue != null) {
      result.addAll({'defaultValue': defaultValue});
    }
    if (fieldValues != null) {
      result.addAll({'fieldValues': fieldValues});
    }
    if (allowNulls != null) {
      result.addAll({'allowNulls': allowNulls});
    }
    if (sortOrder != null) {
      result.addAll({'sortOrder': sortOrder});
    }

    return result;
  }

  factory ProjectMetadataModel.fromMap(Map<String, dynamic> map) {
    return ProjectMetadataModel(
      id: map['id'],
      fieldName: map['fieldName'],
      displayName: map['displayName'],
      fieldType: map['fieldType']?.toInt(),
      defaultValue: map['defaultValue'],
      fieldValues: map['fieldValues'],
      allowNulls: map['allowNulls'],
      sortOrder: map['sortOrder']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectMetadataModel.fromJson(String source) =>
      ProjectMetadataModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProjectMetadataModel(id: $id, fieldName: $fieldName, displayName: $displayName, fieldType: $fieldType, defaultValue: $defaultValue, fieldValues: $fieldValues, allowNulls: $allowNulls, sortOrder: $sortOrder)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProjectMetadataModel &&
        other.id == id &&
        other.fieldName == fieldName &&
        other.displayName == displayName &&
        other.fieldType == fieldType &&
        other.defaultValue == defaultValue &&
        other.fieldValues == fieldValues &&
        other.allowNulls == allowNulls &&
        other.sortOrder == sortOrder;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        fieldName.hashCode ^
        displayName.hashCode ^
        fieldType.hashCode ^
        defaultValue.hashCode ^
        fieldValues.hashCode ^
        allowNulls.hashCode ^
        sortOrder.hashCode;
  }
}
