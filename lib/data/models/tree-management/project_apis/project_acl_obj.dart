import 'dart:convert';

class ProjectAclObjModel {
  String? objectId;
  String? objectCode;
  String? objectName;
  int? objectType;
  int? accessType;
  ProjectAclObjModel({
    this.objectId,
    this.objectCode,
    this.objectName,
    this.objectType,
    this.accessType,
  });

  ProjectAclObjModel copyWith({
    String? objectId,
    String? objectCode,
    String? objectName,
    int? objectType,
    int? accessType,
  }) {
    return ProjectAclObjModel(
      objectId: objectId ?? this.objectId,
      objectCode: objectCode ?? this.objectCode,
      objectName: objectName ?? this.objectName,
      objectType: objectType ?? this.objectType,
      accessType: accessType ?? this.accessType,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectCode != null) {
      result.addAll({'objectCode': objectCode});
    }
    if (objectName != null) {
      result.addAll({'objectName': objectName});
    }
    if (objectType != null) {
      result.addAll({'objectType': objectType});
    }
    if (accessType != null) {
      result.addAll({'accessType': accessType});
    }

    return result;
  }

  factory ProjectAclObjModel.fromMap(Map<String, dynamic> map) {
    return ProjectAclObjModel(
      objectId: map['objectId'],
      objectCode: map['objectCode'],
      objectName: map['objectName'],
      objectType: map['objectType']?.toInt(),
      accessType: map['accessType']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectAclObjModel.fromJson(String source) =>
      ProjectAclObjModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProjectAclObjModel(objectId: $objectId, objectCode: $objectCode, objectName: $objectName, objectType: $objectType, accessType: $accessType)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProjectAclObjModel &&
        other.objectId == objectId &&
        other.objectCode == objectCode &&
        other.objectName == objectName &&
        other.objectType == objectType &&
        other.accessType == accessType;
  }

  @override
  int get hashCode {
    return objectId.hashCode ^
        objectCode.hashCode ^
        objectName.hashCode ^
        objectType.hashCode ^
        accessType.hashCode;
  }
}
