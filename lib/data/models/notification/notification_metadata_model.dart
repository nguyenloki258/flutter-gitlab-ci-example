import 'dart:convert';

class NotificationMetadataModel {
  String? additionalProp1;
  String? additionalProp2;
  String? additionalProp3;
  NotificationMetadataModel({
    this.additionalProp1,
    this.additionalProp2,
    this.additionalProp3,
  });

  NotificationMetadataModel copyWith({
    String? additionalProp1,
    String? additionalProp2,
    String? additionalProp3,
  }) {
    return NotificationMetadataModel(
      additionalProp1: additionalProp1 ?? this.additionalProp1,
      additionalProp2: additionalProp2 ?? this.additionalProp2,
      additionalProp3: additionalProp3 ?? this.additionalProp3,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (additionalProp1 != null) {
      result.addAll({'additionalProp1': additionalProp1});
    }
    if (additionalProp2 != null) {
      result.addAll({'additionalProp2': additionalProp2});
    }
    if (additionalProp3 != null) {
      result.addAll({'additionalProp3': additionalProp3});
    }

    return result;
  }

  factory NotificationMetadataModel.fromMap(Map<String, dynamic> map) {
    return NotificationMetadataModel(
      additionalProp1: map['additionalProp1'],
      additionalProp2: map['additionalProp2'],
      additionalProp3: map['additionalProp3'],
    );
  }

  String toJson() => json.encode(toMap());

  factory NotificationMetadataModel.fromJson(String source) =>
      NotificationMetadataModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'NotificationMetadataModel(additionalProp1: $additionalProp1, additionalProp2: $additionalProp2, additionalProp3: $additionalProp3)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is NotificationMetadataModel &&
        other.additionalProp1 == additionalProp1 &&
        other.additionalProp2 == additionalProp2 &&
        other.additionalProp3 == additionalProp3;
  }

  @override
  int get hashCode =>
      additionalProp1.hashCode ^
      additionalProp2.hashCode ^
      additionalProp3.hashCode;
}
