import 'dart:convert';

import 'notification_metadata_model.dart';

class NotificationContentModel {
  String applicationId;
  String id;
  String? ownerId;
  String? title;
  String? message;
  String? icon;
  int? state;
  bool? isSentToDevices;
  DateTime? createdAt;
  String? objectId;
  String? objectCode;
  String? module;
  bool? isRead;
  NotificationMetadataModel? metadata;
  String? metadataContent;
  String? metaContentType;
  String? searchKeywords;
  NotificationContentModel({
    required this.applicationId,
    required this.id,
    this.ownerId,
    this.title,
    this.message,
    this.icon,
    this.state,
    this.isSentToDevices,
    this.createdAt,
    this.objectId,
    this.objectCode,
    this.module,
    this.isRead,
    this.metadata,
    this.metadataContent,
    this.metaContentType,
    this.searchKeywords,
  });

  NotificationContentModel copyWith({
    String? applicationId,
    String? id,
    String? ownerId,
    String? title,
    String? message,
    String? icon,
    int? state,
    bool? isSentToDevices,
    DateTime? createdAt,
    String? objectId,
    String? objectCode,
    String? module,
    bool? isRead,
    NotificationMetadataModel? metadata,
    String? metadataContent,
    String? metaContentType,
    String? searchKeywords,
  }) {
    return NotificationContentModel(
      applicationId: applicationId ?? this.applicationId,
      id: id ?? this.id,
      ownerId: ownerId ?? this.ownerId,
      title: title ?? this.title,
      message: message ?? this.message,
      icon: icon ?? this.icon,
      state: state ?? this.state,
      isSentToDevices: isSentToDevices ?? this.isSentToDevices,
      createdAt: createdAt ?? this.createdAt,
      objectId: objectId ?? this.objectId,
      objectCode: objectCode ?? this.objectCode,
      module: module ?? this.module,
      isRead: isRead ?? this.isRead,
      metadata: metadata ?? this.metadata,
      metadataContent: metadataContent ?? this.metadataContent,
      metaContentType: metaContentType ?? this.metaContentType,
      searchKeywords: searchKeywords ?? this.searchKeywords,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'applicationId': applicationId});
    result.addAll({'id': id});
    if (ownerId != null) {
      result.addAll({'ownerId': ownerId});
    }
    if (title != null) {
      result.addAll({'title': title});
    }
    if (message != null) {
      result.addAll({'message': message});
    }
    if (icon != null) {
      result.addAll({'icon': icon});
    }
    if (state != null) {
      result.addAll({'state': state});
    }
    if (isSentToDevices != null) {
      result.addAll({'isSentToDevices': isSentToDevices});
    }
    if (createdAt != null) {
      result.addAll({'createdAt': createdAt!.millisecondsSinceEpoch});
    }
    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectCode != null) {
      result.addAll({'objectCode': objectCode});
    }
    if (module != null) {
      result.addAll({'module': module});
    }
    if (isRead != null) {
      result.addAll({'isRead': isRead});
    }
    if (metadata != null) {
      result.addAll({'metadata': metadata!.toMap()});
    }
    if (metadataContent != null) {
      result.addAll({'metadataContent': metadataContent});
    }
    if (metaContentType != null) {
      result.addAll({'metaContentType': metaContentType});
    }
    if (searchKeywords != null) {
      result.addAll({'searchKeywords': searchKeywords});
    }

    return result;
  }

  factory NotificationContentModel.fromMap(Map<String, dynamic> map) {
    return NotificationContentModel(
      applicationId: map['applicationId'] ?? '',
      id: map['id'] ?? '',
      ownerId: map['ownerId'],
      title: map['title'],
      message: map['message'],
      icon: map['icon'],
      state: map['state']?.toInt(),
      isSentToDevices: map['isSentToDevices'],
      createdAt:
          map['createdAt'] != null ? DateTime.parse(map['createdAt']) : null,
      objectId: map['objectId'],
      objectCode: map['objectCode'],
      module: map['module'],
      isRead: map['isRead'],
      metadata: map['metadata'] != null
          ? NotificationMetadataModel.fromMap(map['metadata'])
          : null,
      metadataContent: map['metadataContent'],
      metaContentType: map['metaContentType'],
      searchKeywords: map['searchKeywords'],
    );
  }

  String toJson() => json.encode(toMap());

  factory NotificationContentModel.fromJson(String source) =>
      NotificationContentModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'NotificationContentModel(applicationId: $applicationId, id: $id, ownerId: $ownerId, title: $title, message: $message, icon: $icon, state: $state, isSentToDevices: $isSentToDevices, createdAt: $createdAt, objectId: $objectId, objectCode: $objectCode, module: $module, isRead: $isRead, metadata: $metadata, metadataContent: $metadataContent, metaContentType: $metaContentType, searchKeywords: $searchKeywords)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is NotificationContentModel &&
        other.applicationId == applicationId &&
        other.id == id &&
        other.ownerId == ownerId &&
        other.title == title &&
        other.message == message &&
        other.icon == icon &&
        other.state == state &&
        other.isSentToDevices == isSentToDevices &&
        other.createdAt == createdAt &&
        other.objectId == objectId &&
        other.objectCode == objectCode &&
        other.module == module &&
        other.isRead == isRead &&
        other.metadata == metadata &&
        other.metadataContent == metadataContent &&
        other.metaContentType == metaContentType &&
        other.searchKeywords == searchKeywords;
  }

  @override
  int get hashCode {
    return applicationId.hashCode ^
        id.hashCode ^
        ownerId.hashCode ^
        title.hashCode ^
        message.hashCode ^
        icon.hashCode ^
        state.hashCode ^
        isSentToDevices.hashCode ^
        createdAt.hashCode ^
        objectId.hashCode ^
        objectCode.hashCode ^
        module.hashCode ^
        isRead.hashCode ^
        metadata.hashCode ^
        metadataContent.hashCode ^
        metaContentType.hashCode ^
        searchKeywords.hashCode;
  }
}
