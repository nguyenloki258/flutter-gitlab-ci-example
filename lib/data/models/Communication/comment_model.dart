import 'dart:convert';

class CommentModel {
  String id;
  String? name;
  String? avatarUrl;
  String? createdByUserId;
  String? lastModifiedByUserId;
  DateTime? lastModifiedOnDate;
  DateTime? createdOnDate;
  String? applicationId;
  String? objectId;
  String? objectType;
  String? ref;
  String? message;
  int? statusObj;
  int? status;
  String? attachment;
  String? username;
  String? userdisplay;
  CommentModel({
    required this.id,
    this.name,
    this.avatarUrl,
    this.createdByUserId,
    this.lastModifiedByUserId,
    this.lastModifiedOnDate,
    this.createdOnDate,
    this.applicationId,
    this.objectId,
    this.objectType,
    this.ref,
    this.message,
    this.statusObj,
    this.status,
    this.attachment,
    this.username,
    this.userdisplay,
  });

  CommentModel copyWith({
    String? id,
    String? name,
    String? avatarUrl,
    String? createdByUserId,
    String? lastModifiedByUserId,
    DateTime? lastModifiedOnDate,
    DateTime? createdOnDate,
    String? applicationId,
    String? objectId,
    String? objectType,
    String? ref,
    String? message,
    int? statusObj,
    int? status,
    String? attachment,
    String? username,
    String? userdisplay,
  }) {
    return CommentModel(
      id: id ?? this.id,
      name: name ?? this.name,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      createdByUserId: createdByUserId ?? this.createdByUserId,
      lastModifiedByUserId: lastModifiedByUserId ?? this.lastModifiedByUserId,
      lastModifiedOnDate: lastModifiedOnDate ?? this.lastModifiedOnDate,
      createdOnDate: createdOnDate ?? this.createdOnDate,
      applicationId: applicationId ?? this.applicationId,
      objectId: objectId ?? this.objectId,
      objectType: objectType ?? this.objectType,
      ref: ref ?? this.ref,
      message: message ?? this.message,
      statusObj: statusObj ?? this.statusObj,
      status: status ?? this.status,
      attachment: attachment ?? this.attachment,
      username: username ?? this.username,
      userdisplay: userdisplay ?? this.userdisplay,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'id': id});
    if (name != null) {
      result.addAll({'name': name});
    }
    if (avatarUrl != null) {
      result.addAll({'avatarUrl': avatarUrl});
    }
    if (createdByUserId != null) {
      result.addAll({'createdByUserId': createdByUserId});
    }
    if (lastModifiedByUserId != null) {
      result.addAll({'lastModifiedByUserId': lastModifiedByUserId});
    }
    if (lastModifiedOnDate != null) {
      result.addAll(
          {'lastModifiedOnDate': lastModifiedOnDate!.millisecondsSinceEpoch});
    }
    if (createdOnDate != null) {
      result.addAll({'createdOnDate': createdOnDate!.millisecondsSinceEpoch});
    }
    if (applicationId != null) {
      result.addAll({'applicationId': applicationId});
    }
    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectType != null) {
      result.addAll({'objectType': objectType});
    }
    if (ref != null) {
      result.addAll({'ref': ref});
    }
    if (message != null) {
      result.addAll({'message': message});
    }
    if (statusObj != null) {
      result.addAll({'statusObj': statusObj});
    }
    if (status != null) {
      result.addAll({'status': status});
    }
    if (attachment != null) {
      result.addAll({'attachment': attachment});
    }
    if (username != null) {
      result.addAll({'username': username});
    }
    if (userdisplay != null) {
      result.addAll({'userdisplay': userdisplay});
    }

    return result;
  }

  factory CommentModel.fromMap(Map<String, dynamic> map) {
    return CommentModel(
      id: map['id'] ?? '',
      name: map['name'],
      avatarUrl: map['avatarUrl'],
      createdByUserId: map['createdByUserId'],
      lastModifiedByUserId: map['lastModifiedByUserId'],
      lastModifiedOnDate: map['lastModifiedOnDate'] != null
          ? DateTime.parse(map['lastModifiedOnDate'])
          : null,
      createdOnDate: map['createdOnDate'] != null
          ? DateTime.parse(map['createdOnDate'])
          : null,
      applicationId: map['applicationId'],
      objectId: map['objectId'],
      objectType: map['objectType'],
      ref: map['ref'],
      message: map['message'],
      statusObj: map['statusObj']?.toInt(),
      status: map['status']?.toInt(),
      attachment: map['attachment'],
      username: map['username'],
      userdisplay: map['userdisplay'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CommentModel.fromJson(String source) =>
      CommentModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CommentModel(id: $id, name: $name, avatarUrl: $avatarUrl, createdByUserId: $createdByUserId, lastModifiedByUserId: $lastModifiedByUserId, lastModifiedOnDate: $lastModifiedOnDate, createdOnDate: $createdOnDate, applicationId: $applicationId, objectId: $objectId, objectType: $objectType, ref: $ref, message: $message, statusObj: $statusObj, status: $status, attachment: $attachment, username: $username, userdisplay: $userdisplay)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CommentModel &&
        other.id == id &&
        other.name == name &&
        other.avatarUrl == avatarUrl &&
        other.createdByUserId == createdByUserId &&
        other.lastModifiedByUserId == lastModifiedByUserId &&
        other.lastModifiedOnDate == lastModifiedOnDate &&
        other.createdOnDate == createdOnDate &&
        other.applicationId == applicationId &&
        other.objectId == objectId &&
        other.objectType == objectType &&
        other.ref == ref &&
        other.message == message &&
        other.statusObj == statusObj &&
        other.status == status &&
        other.attachment == attachment &&
        other.username == username &&
        other.userdisplay == userdisplay;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        avatarUrl.hashCode ^
        createdByUserId.hashCode ^
        lastModifiedByUserId.hashCode ^
        lastModifiedOnDate.hashCode ^
        createdOnDate.hashCode ^
        applicationId.hashCode ^
        objectId.hashCode ^
        objectType.hashCode ^
        ref.hashCode ^
        message.hashCode ^
        statusObj.hashCode ^
        status.hashCode ^
        attachment.hashCode ^
        username.hashCode ^
        userdisplay.hashCode;
  }
}
