import 'dart:convert';

import 'package:collection/collection.dart';

import 'base_category_content_model.dart';

class BaseCategoryModel {
  int? currentPage;
  int? totalPages;
  int? pageSize;
  int? numberOfRecords;
  int? totalRecords;
  List<ContentCategoryModel>? content;
  BaseCategoryModel({
    this.currentPage,
    this.totalPages,
    this.pageSize,
    this.numberOfRecords,
    this.totalRecords,
    this.content,
  });

  BaseCategoryModel copyWith({
    int? currentPage,
    int? totalPages,
    int? pageSize,
    int? numberOfRecords,
    int? totalRecords,
    List<ContentCategoryModel>? content,
  }) {
    return BaseCategoryModel(
      currentPage: currentPage ?? this.currentPage,
      totalPages: totalPages ?? this.totalPages,
      pageSize: pageSize ?? this.pageSize,
      numberOfRecords: numberOfRecords ?? this.numberOfRecords,
      totalRecords: totalRecords ?? this.totalRecords,
      content: content ?? this.content,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (currentPage != null) {
      result.addAll({'currentPage': currentPage});
    }
    if (totalPages != null) {
      result.addAll({'totalPages': totalPages});
    }
    if (pageSize != null) {
      result.addAll({'pageSize': pageSize});
    }
    if (numberOfRecords != null) {
      result.addAll({'numberOfRecords': numberOfRecords});
    }
    if (totalRecords != null) {
      result.addAll({'totalRecords': totalRecords});
    }
    if (content != null) {
      result.addAll({'content': content!.map((x) => x.toMap()).toList()});
    }

    return result;
  }

  factory BaseCategoryModel.fromMap(Map<String, dynamic> map) {
    return BaseCategoryModel(
      currentPage: map['currentPage']?.toInt(),
      totalPages: map['totalPages']?.toInt(),
      pageSize: map['pageSize']?.toInt(),
      numberOfRecords: map['numberOfRecords']?.toInt(),
      totalRecords: map['totalRecords']?.toInt(),
      content: map['content'] != null
          ? List<ContentCategoryModel>.from(
              map['content']?.map((x) => ContentCategoryModel.fromMap(x)))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory BaseCategoryModel.fromJson(String source) =>
      BaseCategoryModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'BaseCategoryModel(currentPage: $currentPage, totalPages: $totalPages, pageSize: $pageSize, numberOfRecords: $numberOfRecords, totalRecords: $totalRecords, content: $content)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is BaseCategoryModel &&
        other.currentPage == currentPage &&
        other.totalPages == totalPages &&
        other.pageSize == pageSize &&
        other.numberOfRecords == numberOfRecords &&
        other.totalRecords == totalRecords &&
        listEquals(other.content, content);
  }

  @override
  int get hashCode {
    return currentPage.hashCode ^
        totalPages.hashCode ^
        pageSize.hashCode ^
        numberOfRecords.hashCode ^
        totalRecords.hashCode ^
        content.hashCode;
  }
}
