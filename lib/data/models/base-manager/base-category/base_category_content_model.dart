import 'dart:convert';

import 'package:collection/collection.dart';

import 'base_category_metadata_model.dart';

class ContentCategoryModel {
  String? id;
  String? categoryId;
  String? parentId;
  String? parentPath;
  String? name;
  String? displayName;
  String? type;
  String? alias;
  String? detail;
  String? description;
  int? order;
  bool? isActive;
  int? status;
  String? imageUrl;
  List<BaseCategoryMetadataModel>? metadataContentObjs;
  String? parent;
  List<String>? children;
  ContentCategoryModel({
    this.id,
    this.categoryId,
    this.parentId,
    this.parentPath,
    this.name,
    this.displayName,
    this.type,
    this.alias,
    this.detail,
    this.description,
    this.order,
    this.isActive,
    this.status,
    this.imageUrl,
    this.metadataContentObjs,
    this.parent,
    this.children,
  });

  ContentCategoryModel copyWith({
    String? id,
    String? categoryId,
    String? parentId,
    String? parentPath,
    String? name,
    String? displayName,
    String? type,
    String? alias,
    String? detail,
    String? description,
    int? order,
    bool? isActive,
    int? status,
    String? imageUrl,
    List<BaseCategoryMetadataModel>? metadataContentObjs,
    String? parent,
    List<String>? children,
  }) {
    return ContentCategoryModel(
      id: id ?? this.id,
      categoryId: categoryId ?? this.categoryId,
      parentId: parentId ?? this.parentId,
      parentPath: parentPath ?? this.parentPath,
      name: name ?? this.name,
      displayName: displayName ?? this.displayName,
      type: type ?? this.type,
      alias: alias ?? this.alias,
      detail: detail ?? this.detail,
      description: description ?? this.description,
      order: order ?? this.order,
      isActive: isActive ?? this.isActive,
      status: status ?? this.status,
      imageUrl: imageUrl ?? this.imageUrl,
      metadataContentObjs: metadataContentObjs ?? this.metadataContentObjs,
      parent: parent ?? this.parent,
      children: children ?? this.children,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (id != null) {
      result.addAll({'id': id});
    }
    if (categoryId != null) {
      result.addAll({'categoryId': categoryId});
    }
    if (parentId != null) {
      result.addAll({'parentId': parentId});
    }
    if (parentPath != null) {
      result.addAll({'parentPath': parentPath});
    }
    if (name != null) {
      result.addAll({'name': name});
    }
    if (displayName != null) {
      result.addAll({'displayName': displayName});
    }
    if (type != null) {
      result.addAll({'type': type});
    }
    if (alias != null) {
      result.addAll({'alias': alias});
    }
    if (detail != null) {
      result.addAll({'detail': detail});
    }
    if (description != null) {
      result.addAll({'description': description});
    }
    if (order != null) {
      result.addAll({'order': order});
    }
    if (isActive != null) {
      result.addAll({'isActive': isActive});
    }
    if (status != null) {
      result.addAll({'status': status});
    }
    if (imageUrl != null) {
      result.addAll({'imageUrl': imageUrl});
    }
    if (metadataContentObjs != null) {
      result.addAll({
        'metadataContentObjs':
            metadataContentObjs!.map((x) => x.toMap()).toList()
      });
    }
    if (parent != null) {
      result.addAll({'parent': parent});
    }
    if (children != null) {
      result.addAll({'children': children});
    }

    return result;
  }

  factory ContentCategoryModel.fromMap(Map<String, dynamic> map) {
    return ContentCategoryModel(
      id: map['id'],
      categoryId: map['categoryId'],
      parentId: map['parentId'],
      parentPath: map['parentPath'],
      name: map['name'],
      displayName: map['displayName'],
      type: map['type'],
      alias: map['alias'],
      detail: map['detail'],
      description: map['description'],
      order: map['order']?.toInt(),
      isActive: map['isActive'],
      status: map['status']?.toInt(),
      imageUrl: map['imageUrl'],
      metadataContentObjs: map['metadataContentObjs'] != null
          ? List<BaseCategoryMetadataModel>.from(map['metadataContentObjs']
              ?.map((x) => BaseCategoryMetadataModel.fromMap(x)))
          : null,
      parent: map['parent'],
      children:
          map['children'] != null ? List<String>.from(map['children']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ContentCategoryModel.fromJson(String source) =>
      ContentCategoryModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ContentCategoryModel(id: $id, categoryId: $categoryId, parentId: $parentId, parentPath: $parentPath, name: $name, displayName: $displayName, type: $type, alias: $alias, detail: $detail, description: $description, order: $order, isActive: $isActive, status: $status, imageUrl: $imageUrl, metadataContentObjs: $metadataContentObjs, parent: $parent, children: $children)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is ContentCategoryModel &&
        other.id == id &&
        other.categoryId == categoryId &&
        other.parentId == parentId &&
        other.parentPath == parentPath &&
        other.name == name &&
        other.displayName == displayName &&
        other.type == type &&
        other.alias == alias &&
        other.detail == detail &&
        other.description == description &&
        other.order == order &&
        other.isActive == isActive &&
        other.status == status &&
        other.imageUrl == imageUrl &&
        listEquals(other.metadataContentObjs, metadataContentObjs) &&
        other.parent == parent &&
        listEquals(other.children, children);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        categoryId.hashCode ^
        parentId.hashCode ^
        parentPath.hashCode ^
        name.hashCode ^
        displayName.hashCode ^
        type.hashCode ^
        alias.hashCode ^
        detail.hashCode ^
        description.hashCode ^
        order.hashCode ^
        isActive.hashCode ^
        status.hashCode ^
        imageUrl.hashCode ^
        metadataContentObjs.hashCode ^
        parent.hashCode ^
        children.hashCode;
  }
}
