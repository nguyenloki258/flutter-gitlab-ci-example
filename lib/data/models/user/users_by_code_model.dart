import 'dart:convert';

import 'user_by_code_detail_model.dart';
import 'users_by_code_setting_model.dart';

class UsersByCodeModel {
  String id;
  String? userName;
  String? name;
  String? avatarUrl;
  String? applicationId;
  String? email;
  String? phoneNumber;
  int? typeObj;
  bool? isActive;
  UserDetailModel? userDetail;
  bool? isSuperUser;
  bool? forceChangePassword;
  UsersSettingValuesModel? settingValues;
  DateTime? lastActivityDate;
  String? groupType;
  String? groupId;
  String? groupName;
  String? userProfile;
  String? userProfile2;
  bool? isChecked;
  UsersByCodeModel({
    required this.id,
    this.userName,
    this.name,
    this.avatarUrl,
    this.applicationId,
    this.email,
    this.phoneNumber,
    this.typeObj,
    this.isActive,
    this.userDetail,
    this.isSuperUser,
    this.forceChangePassword,
    this.settingValues,
    this.lastActivityDate,
    this.groupType,
    this.groupId,
    this.groupName,
    this.userProfile,
    this.userProfile2,
    this.isChecked,
  });

  UsersByCodeModel copyWith({
    String? id,
    String? userName,
    String? name,
    String? avatarUrl,
    String? applicationId,
    String? email,
    String? phoneNumber,
    int? typeObj,
    bool? isActive,
    UserDetailModel? userDetail,
    bool? isSuperUser,
    bool? forceChangePassword,
    UsersSettingValuesModel? settingValues,
    DateTime? lastActivityDate,
    String? groupType,
    String? groupId,
    String? groupName,
    String? userProfile,
    String? userProfile2,
    bool? isChecked,
  }) {
    return UsersByCodeModel(
      id: id ?? this.id,
      userName: userName ?? this.userName,
      name: name ?? this.name,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      applicationId: applicationId ?? this.applicationId,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      typeObj: typeObj ?? this.typeObj,
      isActive: isActive ?? this.isActive,
      userDetail: userDetail ?? this.userDetail,
      isSuperUser: isSuperUser ?? this.isSuperUser,
      forceChangePassword: forceChangePassword ?? this.forceChangePassword,
      settingValues: settingValues ?? this.settingValues,
      lastActivityDate: lastActivityDate ?? this.lastActivityDate,
      groupType: groupType ?? this.groupType,
      groupId: groupId ?? this.groupId,
      groupName: groupName ?? this.groupName,
      userProfile: userProfile ?? this.userProfile,
      userProfile2: userProfile2 ?? this.userProfile2,
      isChecked: isChecked ?? this.isChecked,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'id': id});
    if (userName != null) {
      result.addAll({'userName': userName});
    }
    if (name != null) {
      result.addAll({'name': name});
    }
    if (avatarUrl != null) {
      result.addAll({'avatarUrl': avatarUrl});
    }
    if (applicationId != null) {
      result.addAll({'applicationId': applicationId});
    }
    if (email != null) {
      result.addAll({'email': email});
    }
    if (phoneNumber != null) {
      result.addAll({'phoneNumber': phoneNumber});
    }
    if (typeObj != null) {
      result.addAll({'typeObj': typeObj});
    }
    if (isActive != null) {
      result.addAll({'isActive': isActive});
    }
    if (userDetail != null) {
      result.addAll({'userDetail': userDetail!.toMap()});
    }
    if (isSuperUser != null) {
      result.addAll({'isSuperUser': isSuperUser});
    }
    if (forceChangePassword != null) {
      result.addAll({'forceChangePassword': forceChangePassword});
    }
    if (settingValues != null) {
      result.addAll({'settingValues': settingValues!.toMap()});
    }
    if (lastActivityDate != null) {
      result.addAll(
          {'lastActivityDate': lastActivityDate!.millisecondsSinceEpoch});
    }
    if (groupType != null) {
      result.addAll({'groupType': groupType});
    }
    if (groupId != null) {
      result.addAll({'groupId': groupId});
    }
    if (groupName != null) {
      result.addAll({'groupName': groupName});
    }
    if (userProfile != null) {
      result.addAll({'userProfile': userProfile});
    }
    if (userProfile2 != null) {
      result.addAll({'userProfile2': userProfile2});
    }
    if (isChecked != null) {
      result.addAll({'isChecked': isChecked});
    }

    return result;
  }

  factory UsersByCodeModel.fromMap(Map<String, dynamic> map) {
    return UsersByCodeModel(
      id: map['id'] ?? '',
      userName: map['userName'],
      name: map['name'],
      avatarUrl: map['avatarUrl'],
      applicationId: map['applicationId'],
      email: map['email'],
      phoneNumber: map['phoneNumber'],
      typeObj: map['typeObj']?.toInt(),
      isActive: map['isActive'],
      userDetail: map['userDetail'] != null
          ? UserDetailModel.fromMap(map['userDetail'])
          : null,
      isSuperUser: map['isSuperUser'],
      forceChangePassword: map['forceChangePassword'],
      settingValues: map['settingValues'] != null
          ? UsersSettingValuesModel.fromMap(map['settingValues'])
          : null,
      lastActivityDate: map['lastActivityDate'] != null
          ? DateTime.parse(map['lastActivityDate'])
          : null,
      groupType: map['groupType'],
      groupId: map['groupId'],
      groupName: map['groupName'],
      userProfile: map['userProfile'],
      userProfile2: map['userProfile2'],
      isChecked: map['isChecked'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UsersByCodeModel.fromJson(String source) =>
      UsersByCodeModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UsersByCodeModel(id: $id, userName: $userName, name: $name, avatarUrl: $avatarUrl, applicationId: $applicationId, email: $email, phoneNumber: $phoneNumber, typeObj: $typeObj, isActive: $isActive, userDetail: $userDetail, isSuperUser: $isSuperUser, forceChangePassword: $forceChangePassword, settingValues: $settingValues, lastActivityDate: $lastActivityDate, groupType: $groupType, groupId: $groupId, groupName: $groupName, userProfile: $userProfile, userProfile2: $userProfile2, isChecked: $isChecked)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UsersByCodeModel &&
        other.id == id &&
        other.userName == userName &&
        other.name == name &&
        other.avatarUrl == avatarUrl &&
        other.applicationId == applicationId &&
        other.email == email &&
        other.phoneNumber == phoneNumber &&
        other.typeObj == typeObj &&
        other.isActive == isActive &&
        other.userDetail == userDetail &&
        other.isSuperUser == isSuperUser &&
        other.forceChangePassword == forceChangePassword &&
        other.settingValues == settingValues &&
        other.lastActivityDate == lastActivityDate &&
        other.groupType == groupType &&
        other.groupId == groupId &&
        other.groupName == groupName &&
        other.userProfile == userProfile &&
        other.userProfile2 == userProfile2 &&
        other.isChecked == isChecked;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        userName.hashCode ^
        name.hashCode ^
        avatarUrl.hashCode ^
        applicationId.hashCode ^
        email.hashCode ^
        phoneNumber.hashCode ^
        typeObj.hashCode ^
        isActive.hashCode ^
        userDetail.hashCode ^
        isSuperUser.hashCode ^
        forceChangePassword.hashCode ^
        settingValues.hashCode ^
        lastActivityDate.hashCode ^
        groupType.hashCode ^
        groupId.hashCode ^
        groupName.hashCode ^
        userProfile.hashCode ^
        userProfile2.hashCode ^
        isChecked.hashCode;
  }
}
