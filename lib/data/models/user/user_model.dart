import 'dart:convert';

class UserModel {
  String? id;
  String? name;
  String? avatarUrl;
  String? title;
  int? gender;
  DateTime? birthdate;
  String? country;
  String? city;
  String? address;
  String? postalCode;
  String? aboutMe;
  UserModel({
    this.id,
    this.name,
    this.avatarUrl,
    this.title,
    this.gender,
    this.birthdate,
    this.country,
    this.city,
    this.address,
    this.postalCode,
    this.aboutMe,
  });

  UserModel copyWith({
    String? id,
    String? name,
    String? avatarUrl,
    String? title,
    int? gender,
    DateTime? birthdate,
    String? country,
    String? city,
    String? address,
    String? postalCode,
    String? aboutMe,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      title: title ?? this.title,
      gender: gender ?? this.gender,
      birthdate: birthdate ?? this.birthdate,
      country: country ?? this.country,
      city: city ?? this.city,
      address: address ?? this.address,
      postalCode: postalCode ?? this.postalCode,
      aboutMe: aboutMe ?? this.aboutMe,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (id != null) {
      result.addAll({'id': id});
    }
    if (name != null) {
      result.addAll({'name': name});
    }
    if (avatarUrl != null) {
      result.addAll({'avatarUrl': avatarUrl});
    }
    if (title != null) {
      result.addAll({'title': title});
    }
    if (gender != null) {
      result.addAll({'gender': gender});
    }
    if (birthdate != null) {
      result.addAll({'birthdate': birthdate!.millisecondsSinceEpoch});
    }
    if (country != null) {
      result.addAll({'country': country});
    }
    if (city != null) {
      result.addAll({'city': city});
    }
    if (address != null) {
      result.addAll({'address': address});
    }
    if (postalCode != null) {
      result.addAll({'postalCode': postalCode});
    }
    if (aboutMe != null) {
      result.addAll({'aboutMe': aboutMe});
    }

    return result;
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      id: map['id'],
      name: map['name'],
      avatarUrl: map['avatarUrl'],
      title: map['title'],
      gender: map['gender']?.toInt(),
      birthdate:
          map['birthdate'] != null ? DateTime.parse(map['birthdate']) : null,
      country: map['country'],
      city: map['city'],
      address: map['address'],
      postalCode: map['postalCode'],
      aboutMe: map['aboutMe'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserModel(id: $id, name: $name, avatarUrl: $avatarUrl, title: $title, gender: $gender, birthdate: $birthdate, country: $country, city: $city, address: $address, postalCode: $postalCode, aboutMe: $aboutMe)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserModel &&
        other.id == id &&
        other.name == name &&
        other.avatarUrl == avatarUrl &&
        other.title == title &&
        other.gender == gender &&
        other.birthdate == birthdate &&
        other.country == country &&
        other.city == city &&
        other.address == address &&
        other.postalCode == postalCode &&
        other.aboutMe == aboutMe;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        avatarUrl.hashCode ^
        title.hashCode ^
        gender.hashCode ^
        birthdate.hashCode ^
        country.hashCode ^
        city.hashCode ^
        address.hashCode ^
        postalCode.hashCode ^
        aboutMe.hashCode;
  }
}
