import 'dart:convert';

class UsersSettingValuesModel {
  String? language;
  String? sidebarColor;
  String? sidebarBackground;
  UsersSettingValuesModel({
    this.language,
    this.sidebarColor,
    this.sidebarBackground,
  });

  UsersSettingValuesModel copyWith({
    String? language,
    String? sidebarColor,
    String? sidebarBackground,
  }) {
    return UsersSettingValuesModel(
      language: language ?? this.language,
      sidebarColor: sidebarColor ?? this.sidebarColor,
      sidebarBackground: sidebarBackground ?? this.sidebarBackground,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (language != null) {
      result.addAll({'language': language});
    }
    if (sidebarColor != null) {
      result.addAll({'sidebarColor': sidebarColor});
    }
    if (sidebarBackground != null) {
      result.addAll({'sidebarBackground': sidebarBackground});
    }

    return result;
  }

  factory UsersSettingValuesModel.fromMap(Map<String, dynamic> map) {
    return UsersSettingValuesModel(
      language: map['language'],
      sidebarColor: map['sidebarColor'],
      sidebarBackground: map['sidebarBackground'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UsersSettingValuesModel.fromJson(String source) =>
      UsersSettingValuesModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'SettingValuesModel(language: $language, sidebarColor: $sidebarColor, sidebarBackground: $sidebarBackground)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UsersSettingValuesModel &&
        other.language == language &&
        other.sidebarColor == sidebarColor &&
        other.sidebarBackground == sidebarBackground;
  }

  @override
  int get hashCode =>
      language.hashCode ^ sidebarColor.hashCode ^ sidebarBackground.hashCode;
}
