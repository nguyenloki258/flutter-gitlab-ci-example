import 'dart:convert';

class UserDetailModel {
  String? firstName;
  String? lastName;
  String? fullName;
  String? title;
  int? gender;
  dynamic birthdate;
  String? country;
  String? city;
  String? address;
  String? postalCode;
  String? aboutMe;
  UserDetailModel({
    this.firstName,
    this.lastName,
    this.fullName,
    this.title,
    this.gender,
    required this.birthdate,
    this.country,
    this.city,
    this.address,
    this.postalCode,
    this.aboutMe,
  });

  UserDetailModel copyWith({
    String? firstName,
    String? lastName,
    String? fullName,
    String? title,
    int? gender,
    dynamic birthdate,
    String? country,
    String? city,
    String? address,
    String? postalCode,
    String? aboutMe,
  }) {
    return UserDetailModel(
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      fullName: fullName ?? this.fullName,
      title: title ?? this.title,
      gender: gender ?? this.gender,
      birthdate: birthdate ?? this.birthdate,
      country: country ?? this.country,
      city: city ?? this.city,
      address: address ?? this.address,
      postalCode: postalCode ?? this.postalCode,
      aboutMe: aboutMe ?? this.aboutMe,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (firstName != null) {
      result.addAll({'firstName': firstName});
    }
    if (lastName != null) {
      result.addAll({'lastName': lastName});
    }
    if (fullName != null) {
      result.addAll({'fullName': fullName});
    }
    if (title != null) {
      result.addAll({'title': title});
    }
    if (gender != null) {
      result.addAll({'gender': gender});
    }
    result.addAll({'birthdate': birthdate});
    if (country != null) {
      result.addAll({'country': country});
    }
    if (city != null) {
      result.addAll({'city': city});
    }
    if (address != null) {
      result.addAll({'address': address});
    }
    if (postalCode != null) {
      result.addAll({'postalCode': postalCode});
    }
    if (aboutMe != null) {
      result.addAll({'aboutMe': aboutMe});
    }

    return result;
  }

  factory UserDetailModel.fromMap(Map<String, dynamic> map) {
    return UserDetailModel(
      firstName: map['firstName'],
      lastName: map['lastName'],
      fullName: map['fullName'],
      title: map['title'],
      gender: map['gender']?.toInt(),
      birthdate: map['birthdate'] ?? null,
      country: map['country'],
      city: map['city'],
      address: map['address'],
      postalCode: map['postalCode'],
      aboutMe: map['aboutMe'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserDetailModel.fromJson(String source) =>
      UserDetailModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserDetailModel(firstName: $firstName, lastName: $lastName, fullName: $fullName, title: $title, gender: $gender, birthdate: $birthdate, country: $country, city: $city, address: $address, postalCode: $postalCode, aboutMe: $aboutMe)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserDetailModel &&
        other.firstName == firstName &&
        other.lastName == lastName &&
        other.fullName == fullName &&
        other.title == title &&
        other.gender == gender &&
        other.birthdate == birthdate &&
        other.country == country &&
        other.city == city &&
        other.address == address &&
        other.postalCode == postalCode &&
        other.aboutMe == aboutMe;
  }

  @override
  int get hashCode {
    return firstName.hashCode ^
        lastName.hashCode ^
        fullName.hashCode ^
        title.hashCode ^
        gender.hashCode ^
        birthdate.hashCode ^
        country.hashCode ^
        city.hashCode ^
        address.hashCode ^
        postalCode.hashCode ^
        aboutMe.hashCode;
  }
}
