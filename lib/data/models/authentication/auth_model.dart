import 'dart:convert';

import 'package:collection/collection.dart';

import 'application_model.dart';
import 'metadata_content_obj_model.dart';
import 'user_detail_model.dart';
import 'user_settings_model.dart';

class AuthModel {
  String id;
  String applicationId = '';
  String? email;
  String? username;
  String? title;
  String? firstName;
  String? lastName;
  String? name;
  String? phoneNumber;
  String? postalCode;
  String? avatarUrl;
  bool? forceChangePassword;
  List<String>? roles;
  List<String>? rights;
  List<ApplicationModel>? applications;
  List<String>? navigations;
  UserSettingsModel? settings;
  UserDetailModel? userDetail;
  bool? isSuperUser;
  String? token;
  String? fbToken;
  String? userProfile;
  String? userProfile2;
  String? userProfile3;
  List<MetadataContentObjModel>? metadataContentObjs;

  AuthModel({
    required this.id,
    required this.applicationId,
    this.email,
    this.username,
    this.title,
    this.firstName,
    this.lastName,
    this.name,
    this.phoneNumber,
    this.postalCode,
    this.avatarUrl,
    this.forceChangePassword,
    this.roles,
    this.rights,
    this.applications,
    this.navigations,
    this.settings,
    this.userDetail,
    this.isSuperUser,
    this.token,
    this.fbToken,
    this.userProfile,
    this.userProfile2,
    this.userProfile3,
    this.metadataContentObjs,
  });

  AuthModel copyWith({
    String? id,
    String? applicationId,
    String? email,
    String? username,
    String? title,
    String? firstName,
    String? lastName,
    String? name,
    String? phoneNumber,
    String? postalCode,
    String? avatarUrl,
    bool? forceChangePassword,
    List<String>? roles,
    List<String>? rights,
    List<ApplicationModel>? applications,
    List<String>? navigations,
    UserSettingsModel? settings,
    UserDetailModel? userDetail,
    bool? isSuperUser,
    String? token,
    String? fbToken,
    String? userProfile,
    String? userProfile2,
    String? userProfile3,
    List<MetadataContentObjModel>? metadataContentObjs,
  }) {
    return AuthModel(
      id: id ?? this.id,
      applicationId: applicationId ?? this.applicationId,
      email: email ?? this.email,
      username: username ?? this.username,
      title: title ?? this.title,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      name: name ?? this.name,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      postalCode: postalCode ?? this.postalCode,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      forceChangePassword: forceChangePassword ?? this.forceChangePassword,
      roles: roles ?? this.roles,
      rights: rights ?? this.rights,
      applications: applications ?? this.applications,
      navigations: navigations ?? this.navigations,
      settings: settings ?? this.settings,
      userDetail: userDetail ?? this.userDetail,
      isSuperUser: isSuperUser ?? this.isSuperUser,
      token: token ?? this.token,
      fbToken: fbToken ?? this.fbToken,
      userProfile: userProfile ?? this.userProfile,
      userProfile2: userProfile2 ?? this.userProfile2,
      userProfile3: userProfile3 ?? this.userProfile3,
      metadataContentObjs: metadataContentObjs ?? this.metadataContentObjs,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'applicationId': applicationId,
      'email': email,
      'username': username,
      'title': title,
      'firstName': firstName,
      'lastName': lastName,
      'name': name,
      'phoneNumber': phoneNumber,
      'postalCode': postalCode,
      'avatarUrl': avatarUrl,
      'forceChangePassword': forceChangePassword,
      'roles': roles,
      'rights': rights,
      'applications': applications?.map((x) => x.toMap()).toList(),
      'navigations': navigations,
      'settings': settings?.toMap(),
      'userDetail': userDetail?.toMap(),
      'isSuperUser': isSuperUser,
      'token': token,
      'fbToken': fbToken,
      'userProfile': userProfile,
      'userProfile2': userProfile2,
      'userProfile3': userProfile3,
      'metadataContentObjs':
          metadataContentObjs?.map((x) => x.toMap()).toList(),
    };
  }

  factory AuthModel.fromMap(Map<String, dynamic> map) {
    return AuthModel(
      id: map['id'] ?? '',
      applicationId: map['applicationId'] ?? '',
      email: map['email'],
      username: map['username'],
      title: map['title'],
      firstName: map['firstName'],
      lastName: map['lastName'],
      name: map['name'],
      phoneNumber: map['phoneNumber'],
      postalCode: map['postalCode'],
      avatarUrl: map['avatarUrl'],
      forceChangePassword: map['forceChangePassword'],
      roles: map['roles'] == null ? null : List<String>.from(map['roles']),
      rights: map['rights'] == null ? null : List<String>.from(map['rights']),
      applications: map['applications'] != null
          ? List<ApplicationModel>.from(
              map['applications']?.map((x) => ApplicationModel.fromMap(x)))
          : null,
      navigations: map['navigations'] == null
          ? null
          : List<String>.from(map['navigations']),
      settings: map['settings'] != null
          ? UserSettingsModel.fromMap(map['settings'])
          : null,
      userDetail: map['userDetail'] != null
          ? UserDetailModel.fromMap(map['userDetail'])
          : null,
      isSuperUser: map['isSuperUser'],
      token: map['token'],
      fbToken: map['fbToken'],
      userProfile: map['userProfile'],
      userProfile2: map['userProfile2'],
      userProfile3: map['userProfile3'],
      metadataContentObjs: map['metadataContentObjs'] != null
          ? List<MetadataContentObjModel>.from(map['metadataContentObjs']
              ?.map((x) => MetadataContentObjModel.fromMap(x)))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthModel.fromJson(String source) =>
      AuthModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'AuthModel(id: $id, applicationId: $applicationId, email: $email, username: $username, title: $title, firstName: $firstName, lastName: $lastName, name: $name, phoneNumber: $phoneNumber, postalCode: $postalCode, avatarUrl: $avatarUrl, forceChangePassword: $forceChangePassword, roles: $roles, rights: $rights, applications: $applications, navigations: $navigations, settings: $settings, userDetail: $userDetail, isSuperUser: $isSuperUser, token: $token, fbToken: $fbToken, userProfile: $userProfile, userProfile2: $userProfile2, userProfile3: $userProfile3, metadataContentObjs: $metadataContentObjs)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is AuthModel &&
        other.id == id &&
        other.applicationId == applicationId &&
        other.email == email &&
        other.username == username &&
        other.title == title &&
        other.firstName == firstName &&
        other.lastName == lastName &&
        other.name == name &&
        other.phoneNumber == phoneNumber &&
        other.postalCode == postalCode &&
        other.avatarUrl == avatarUrl &&
        other.forceChangePassword == forceChangePassword &&
        listEquals(other.roles, roles) &&
        listEquals(other.rights, rights) &&
        listEquals(other.applications, applications) &&
        listEquals(other.navigations, navigations) &&
        other.settings == settings &&
        other.userDetail == userDetail &&
        other.isSuperUser == isSuperUser &&
        other.token == token &&
        other.fbToken == fbToken &&
        other.userProfile == userProfile &&
        other.userProfile2 == userProfile2 &&
        other.userProfile3 == userProfile3 &&
        listEquals(other.metadataContentObjs, metadataContentObjs);
  }

  @override
  int get hashCode {
    return id.hashCode ^
        applicationId.hashCode ^
        email.hashCode ^
        username.hashCode ^
        title.hashCode ^
        firstName.hashCode ^
        lastName.hashCode ^
        name.hashCode ^
        phoneNumber.hashCode ^
        postalCode.hashCode ^
        avatarUrl.hashCode ^
        forceChangePassword.hashCode ^
        roles.hashCode ^
        rights.hashCode ^
        applications.hashCode ^
        navigations.hashCode ^
        settings.hashCode ^
        userDetail.hashCode ^
        isSuperUser.hashCode ^
        token.hashCode ^
        fbToken.hashCode ^
        userProfile.hashCode ^
        userProfile2.hashCode ^
        userProfile3.hashCode ^
        metadataContentObjs.hashCode;
  }
}
