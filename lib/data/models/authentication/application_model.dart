import 'dart:convert';

class ApplicationModel {
  String? parentId;
  String? name;
  String? code;
  String? description;
  String? id;
  late DateTime? lastModifiedOnDate;
  late DateTime? createdOnDate;
  ApplicationModel({
    this.parentId,
    this.name,
    this.code,
    this.description,
    this.id,
    this.lastModifiedOnDate,
    this.createdOnDate,
  });

  ApplicationModel copyWith({
    String? parentId,
    String? name,
    String? code,
    String? description,
    String? id,
    DateTime? lastModifiedOnDate,
    DateTime? createdOnDate,
  }) {
    return ApplicationModel(
      parentId: parentId ?? this.parentId,
      name: name ?? this.name,
      code: code ?? this.code,
      description: description ?? this.description,
      id: id ?? this.id,
      lastModifiedOnDate: lastModifiedOnDate ?? this.lastModifiedOnDate,
      createdOnDate: createdOnDate ?? this.createdOnDate,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'parentId': parentId,
      'name': name,
      'code': code,
      'description': description,
      'id': id,
      'lastModifiedOnDate': lastModifiedOnDate?.toString(),
      'createdOnDate': createdOnDate?.toString(),
    };
  }

  factory ApplicationModel.fromMap(Map<String, dynamic> map) {
    return ApplicationModel(
      parentId: map['parentId'],
      name: map['name'],
      code: map['code'],
      description: map['description'],
      id: map['id'],
      lastModifiedOnDate: map['lastModifiedOnDate'] != null
          ? DateTime.parse(map['lastModifiedOnDate'])
          : null,
      createdOnDate: map['createdOnDate'] != null
          ? DateTime.parse(map['createdOnDate'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ApplicationModel.fromJson(String source) =>
      ApplicationModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ApplicationModel(parentId: $parentId, name: $name, code: $code, description: $description, id: $id, lastModifiedOnDate: $lastModifiedOnDate, createdOnDate: $createdOnDate)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ApplicationModel &&
        other.parentId == parentId &&
        other.name == name &&
        other.code == code &&
        other.description == description &&
        other.id == id &&
        other.lastModifiedOnDate == lastModifiedOnDate &&
        other.createdOnDate == createdOnDate;
  }

  @override
  int get hashCode {
    return parentId.hashCode ^
        name.hashCode ^
        code.hashCode ^
        description.hashCode ^
        id.hashCode ^
        lastModifiedOnDate.hashCode ^
        createdOnDate.hashCode;
  }
}
