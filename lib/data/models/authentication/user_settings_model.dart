import 'dart:convert';

class UserSettingsModel {
  String? language;
  String? sidebarColor;
  String? sidebarBackground;
  UserSettingsModel({
    this.language,
    this.sidebarColor,
    this.sidebarBackground,
  });

  UserSettingsModel copyWith({
    String? language,
    String? sidebarColor,
    String? sidebarBackground,
  }) {
    return UserSettingsModel(
      language: language ?? this.language,
      sidebarColor: sidebarColor ?? this.sidebarColor,
      sidebarBackground: sidebarBackground ?? this.sidebarBackground,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'language': language,
      'sidebarColor': sidebarColor,
      'sidebarBackground': sidebarBackground,
    };
  }

  factory UserSettingsModel.fromMap(Map<String, dynamic> map) {
    return UserSettingsModel(
      language: map['language'],
      sidebarColor: map['sidebarColor'],
      sidebarBackground: map['sidebarBackground'],
    );
  }

  String toJson() => json.encode(toMap());

  factory UserSettingsModel.fromJson(String source) =>
      UserSettingsModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'UserSettingsModel(language: $language, sidebarColor: $sidebarColor, sidebarBackground: $sidebarBackground)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserSettingsModel &&
        other.language == language &&
        other.sidebarColor == sidebarColor &&
        other.sidebarBackground == sidebarBackground;
  }

  @override
  int get hashCode =>
      language.hashCode ^ sidebarColor.hashCode ^ sidebarBackground.hashCode;
}
