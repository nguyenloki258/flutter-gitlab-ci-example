import 'dart:convert';

import 'package:collection/collection.dart';

import 'field_selection_value_model.dart';

class MetadataContentObjModel {
  String? id;
  String? fieldName;
  String? displayName;
  int? fieldType;
  String? defaultValue;
  String? fieldValues;
  List<FieldSelectionValueModel>? fieldSelectionValues;
  bool? allowNulls;
  int? sortOrder;
  MetadataContentObjModel({
    this.id,
    this.fieldName,
    this.displayName,
    this.fieldType,
    this.defaultValue,
    this.fieldValues,
    this.fieldSelectionValues,
    this.allowNulls,
    this.sortOrder,
  });

  MetadataContentObjModel copyWith({
    String? id,
    String? fieldName,
    String? displayName,
    int? fieldType,
    String? defaultValue,
    String? fieldValues,
    List<FieldSelectionValueModel>? fieldSelectionValues,
    bool? allowNulls,
    int? sortOrder,
  }) {
    return MetadataContentObjModel(
      id: id ?? this.id,
      fieldName: fieldName ?? this.fieldName,
      displayName: displayName ?? this.displayName,
      fieldType: fieldType ?? this.fieldType,
      defaultValue: defaultValue ?? this.defaultValue,
      fieldValues: fieldValues ?? this.fieldValues,
      fieldSelectionValues: fieldSelectionValues ?? this.fieldSelectionValues,
      allowNulls: allowNulls ?? this.allowNulls,
      sortOrder: sortOrder ?? this.sortOrder,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'fieldName': fieldName,
      'displayName': displayName,
      'fieldType': fieldType,
      'defaultValue': defaultValue,
      'fieldValues': fieldValues,
      'fieldSelectionValues':
          fieldSelectionValues?.map((x) => x.toMap()).toList(),
      'allowNulls': allowNulls,
      'sortOrder': sortOrder,
    };
  }

  factory MetadataContentObjModel.fromMap(Map<String, dynamic> map) {
    return MetadataContentObjModel(
      id: map['id'],
      fieldName: map['fieldName'],
      displayName: map['displayName'],
      fieldType: map['fieldType']?.toInt(),
      defaultValue: map['defaultValue'],
      fieldValues: map['fieldValues'],
      fieldSelectionValues: map['fieldSelectionValues'] != null
          ? List<FieldSelectionValueModel>.from(map['fieldSelectionValues']
              ?.map((x) => FieldSelectionValueModel.fromMap(x)))
          : null,
      allowNulls: map['allowNulls'],
      sortOrder: map['sortOrder']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory MetadataContentObjModel.fromJson(String source) =>
      MetadataContentObjModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MetadataContentObjModel(id: $id, fieldName: $fieldName, displayName: $displayName, fieldType: $fieldType, defaultValue: $defaultValue, fieldValues: $fieldValues, fieldSelectionValues: $fieldSelectionValues, allowNulls: $allowNulls, sortOrder: $sortOrder)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is MetadataContentObjModel &&
        other.id == id &&
        other.fieldName == fieldName &&
        other.displayName == displayName &&
        other.fieldType == fieldType &&
        other.defaultValue == defaultValue &&
        other.fieldValues == fieldValues &&
        listEquals(other.fieldSelectionValues, fieldSelectionValues) &&
        other.allowNulls == allowNulls &&
        other.sortOrder == sortOrder;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        fieldName.hashCode ^
        displayName.hashCode ^
        fieldType.hashCode ^
        defaultValue.hashCode ^
        fieldValues.hashCode ^
        fieldSelectionValues.hashCode ^
        allowNulls.hashCode ^
        sortOrder.hashCode;
  }
}
