import 'dart:convert';

import 'package:collection/collection.dart';

import 'labels_obj_model.dart';

class ProjectModel {
  String id;
  String? acl;
  String? code;
  String? name;
  String? description;
  String? parentId;
  String? parentType;
  int? priorityObj;
  int? priority;
  int? weight;
  int? rank;
  List<LabelsObjModel>? labelsObjs;
  String? labels;
  List<String>? managementObj;
  String? management;
  List<String>? ownerObj;
  String? owner;
  List<String>? supervisorObj;
  String? supervisor;
  List<String>? coordinatorObj;
  String? coordinator;
  List<String>? assigneeObj;
  String? assignee;
  List<String>? viewerObj;
  String? viewer;
  int? statusObj;
  int? status;
  String? objectId;
  String? objectType;
  String? objectAction;
  DateTime? fromDate;
  DateTime? toDate;
  DateTime? finishDate;
  int? estimateTime;
  int? actualTime;
  int? order;
  int? cLeft;
  int? cRight;
  int? cLevel;
  bool? allowToModify;
  bool? allowToView;
  bool? allowToChangeStatus;
  bool? allowToModifyCoordinator;
  String? projectTemplateId;
  int? workflowStatus;
  ProjectModel({
    required this.id,
    this.acl,
    this.code,
    this.name,
    this.description,
    this.parentId,
    this.parentType,
    this.priorityObj,
    this.priority,
    this.weight,
    this.rank,
    this.labelsObjs,
    this.labels,
    this.managementObj,
    this.management,
    this.ownerObj,
    this.owner,
    this.supervisorObj,
    this.supervisor,
    this.coordinatorObj,
    this.coordinator,
    this.assigneeObj,
    this.assignee,
    this.viewerObj,
    this.viewer,
    this.statusObj,
    this.status,
    this.objectId,
    this.objectType,
    this.objectAction,
    this.fromDate,
    this.toDate,
    this.finishDate,
    this.estimateTime,
    this.actualTime,
    this.order,
    this.cLeft,
    this.cRight,
    this.cLevel,
    this.allowToModify,
    this.allowToView,
    this.allowToChangeStatus,
    this.allowToModifyCoordinator,
    this.projectTemplateId,
    this.workflowStatus,
  });

  ProjectModel copyWith({
    String? id,
    String? acl,
    String? code,
    String? name,
    String? description,
    String? parentId,
    String? parentType,
    int? priorityObj,
    int? priority,
    int? weight,
    int? rank,
    List<LabelsObjModel>? labelsObjs,
    String? labels,
    List<String>? managementObj,
    String? management,
    List<String>? ownerObj,
    String? owner,
    List<String>? supervisorObj,
    String? supervisor,
    List<String>? coordinatorObj,
    String? coordinator,
    List<String>? assigneeObj,
    String? assignee,
    List<String>? viewerObj,
    String? viewer,
    int? statusObj,
    int? status,
    String? objectId,
    String? objectType,
    String? objectAction,
    DateTime? fromDate,
    DateTime? toDate,
    DateTime? finishDate,
    int? estimateTime,
    int? actualTime,
    int? order,
    int? cLeft,
    int? cRight,
    int? cLevel,
    bool? allowToModify,
    bool? allowToView,
    bool? allowToChangeStatus,
    bool? allowToModifyCoordinator,
    String? projectTemplateId,
    int? workflowStatus,
  }) {
    return ProjectModel(
      id: id ?? this.id,
      acl: acl ?? this.acl,
      code: code ?? this.code,
      name: name ?? this.name,
      description: description ?? this.description,
      parentId: parentId ?? this.parentId,
      parentType: parentType ?? this.parentType,
      priorityObj: priorityObj ?? this.priorityObj,
      priority: priority ?? this.priority,
      weight: weight ?? this.weight,
      rank: rank ?? this.rank,
      labelsObjs: labelsObjs ?? this.labelsObjs,
      labels: labels ?? this.labels,
      managementObj: managementObj ?? this.managementObj,
      management: management ?? this.management,
      ownerObj: ownerObj ?? this.ownerObj,
      owner: owner ?? this.owner,
      supervisorObj: supervisorObj ?? this.supervisorObj,
      supervisor: supervisor ?? this.supervisor,
      coordinatorObj: coordinatorObj ?? this.coordinatorObj,
      coordinator: coordinator ?? this.coordinator,
      assigneeObj: assigneeObj ?? this.assigneeObj,
      assignee: assignee ?? this.assignee,
      viewerObj: viewerObj ?? this.viewerObj,
      viewer: viewer ?? this.viewer,
      statusObj: statusObj ?? this.statusObj,
      status: status ?? this.status,
      objectId: objectId ?? this.objectId,
      objectType: objectType ?? this.objectType,
      objectAction: objectAction ?? this.objectAction,
      fromDate: fromDate ?? this.fromDate,
      toDate: toDate ?? this.toDate,
      finishDate: finishDate ?? this.finishDate,
      estimateTime: estimateTime ?? this.estimateTime,
      actualTime: actualTime ?? this.actualTime,
      order: order ?? this.order,
      cLeft: cLeft ?? this.cLeft,
      cRight: cRight ?? this.cRight,
      cLevel: cLevel ?? this.cLevel,
      allowToModify: allowToModify ?? this.allowToModify,
      allowToView: allowToView ?? this.allowToView,
      allowToChangeStatus: allowToChangeStatus ?? this.allowToChangeStatus,
      allowToModifyCoordinator:
          allowToModifyCoordinator ?? this.allowToModifyCoordinator,
      projectTemplateId: projectTemplateId ?? this.projectTemplateId,
      workflowStatus: workflowStatus ?? this.workflowStatus,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'id': id});
    if (acl != null) {
      result.addAll({'acl': acl});
    }
    if (code != null) {
      result.addAll({'code': code});
    }
    if (name != null) {
      result.addAll({'name': name});
    }
    if (description != null) {
      result.addAll({'description': description});
    }
    if (parentId != null) {
      result.addAll({'parentId': parentId});
    }
    if (parentType != null) {
      result.addAll({'parentType': parentType});
    }
    if (priorityObj != null) {
      result.addAll({'priorityObj': priorityObj});
    }
    if (priority != null) {
      result.addAll({'priority': priority});
    }
    if (weight != null) {
      result.addAll({'weight': weight});
    }
    if (rank != null) {
      result.addAll({'rank': rank});
    }
    if (labelsObjs != null) {
      result.addAll({'labelsObjs': labelsObjs!.map((x) => x.toMap()).toList()});
    }
    if (labels != null) {
      result.addAll({'labels': labels});
    }
    if (managementObj != null) {
      result.addAll({'managementObj': managementObj});
    }
    if (management != null) {
      result.addAll({'management': management});
    }
    if (ownerObj != null) {
      result.addAll({'ownerObj': ownerObj});
    }
    if (owner != null) {
      result.addAll({'owner': owner});
    }
    if (supervisorObj != null) {
      result.addAll({'supervisorObj': supervisorObj});
    }
    if (supervisor != null) {
      result.addAll({'supervisor': supervisor});
    }
    if (coordinatorObj != null) {
      result.addAll({'coordinatorObj': coordinatorObj});
    }
    if (coordinator != null) {
      result.addAll({'coordinator': coordinator});
    }
    if (assigneeObj != null) {
      result.addAll({'assigneeObj': assigneeObj});
    }
    if (assignee != null) {
      result.addAll({'assignee': assignee});
    }
    if (viewerObj != null) {
      result.addAll({'viewerObj': viewerObj});
    }
    if (viewer != null) {
      result.addAll({'viewer': viewer});
    }
    if (statusObj != null) {
      result.addAll({'statusObj': statusObj});
    }
    if (status != null) {
      result.addAll({'status': status});
    }
    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectType != null) {
      result.addAll({'objectType': objectType});
    }
    if (objectAction != null) {
      result.addAll({'objectAction': objectAction});
    }
    if (fromDate != null) {
      result.addAll({'fromDate': fromDate!.millisecondsSinceEpoch});
    }
    if (toDate != null) {
      result.addAll({'toDate': toDate!.millisecondsSinceEpoch});
    }
    if (finishDate != null) {
      result.addAll({'finishDate': finishDate!.millisecondsSinceEpoch});
    }
    if (estimateTime != null) {
      result.addAll({'estimateTime': estimateTime});
    }
    if (actualTime != null) {
      result.addAll({'actualTime': actualTime});
    }
    if (order != null) {
      result.addAll({'order': order});
    }
    if (cLeft != null) {
      result.addAll({'cLeft': cLeft});
    }
    if (cRight != null) {
      result.addAll({'cRight': cRight});
    }
    if (cLevel != null) {
      result.addAll({'cLevel': cLevel});
    }
    if (allowToModify != null) {
      result.addAll({'allowToModify': allowToModify});
    }
    if (allowToView != null) {
      result.addAll({'allowToView': allowToView});
    }
    if (allowToChangeStatus != null) {
      result.addAll({'allowToChangeStatus': allowToChangeStatus});
    }
    if (allowToModifyCoordinator != null) {
      result.addAll({'allowToModifyCoordinator': allowToModifyCoordinator});
    }
    if (projectTemplateId != null) {
      result.addAll({'projectTemplateId': projectTemplateId});
    }
    if (workflowStatus != null) {
      result.addAll({'workflowStatus': workflowStatus});
    }

    return result;
  }

  factory ProjectModel.fromMap(Map<String, dynamic> map) {
    return ProjectModel(
      id: map['id'] ?? '',
      acl: map['acl'],
      code: map['code'],
      name: map['name'],
      description: map['description'],
      parentId: map['parentId'],
      parentType: map['parentType'],
      priorityObj: map['priorityObj']?.toInt(),
      priority: map['priority']?.toInt(),
      weight: map['weight']?.toInt(),
      rank: map['rank']?.toInt(),
      labelsObjs: map['labelsObjs'] != null
          ? List<LabelsObjModel>.from(
              map['labelsObjs']?.map((x) => LabelsObjModel.fromMap(x)))
          : null,
      labels: map['labels'],
      managementObj: map['managementObj'] != null
          ? List<String>.from(map['managementObj'])
          : null,
      management: map['management'],
      ownerObj:
          map['ownerObj'] != null ? List<String>.from(map['ownerObj']) : null,
      owner: map['owner'],
      supervisorObj: map['supervisorObj'] != null
          ? List<String>.from(map['supervisorObj'])
          : null,
      supervisor: map['supervisor'],
      coordinatorObj: map['coordinatorObj'] != null
          ? List<String>.from(map['coordinatorObj'])
          : null,
      coordinator: map['coordinator'],
      assigneeObj: map['assigneeObj'] != null
          ? List<String>.from(map['assigneeObj'])
          : null,
      assignee: map['assignee'],
      viewerObj:
          map['viewerObj'] != null ? List<String>.from(map['viewerObj']) : null,
      viewer: map['viewer'],
      statusObj: map['statusObj']?.toInt(),
      status: map['status']?.toInt(),
      objectId: map['objectId'],
      objectType: map['objectType'],
      objectAction: map['objectAction'],
      fromDate:
          map['fromDate'] != null ? DateTime.parse(map['fromDate']) : null,
      toDate: map['toDate'] != null ? DateTime.parse(map['toDate']) : null,
      finishDate:
          map['finishDate'] != null ? DateTime.parse(map['finishDate']) : null,
      estimateTime: map['estimateTime']?.toInt(),
      actualTime: map['actualTime']?.toInt(),
      order: map['order']?.toInt(),
      cLeft: map['cLeft']?.toInt(),
      cRight: map['cRight']?.toInt(),
      cLevel: map['cLevel']?.toInt(),
      allowToModify: map['allowToModify'],
      allowToView: map['allowToView'],
      allowToChangeStatus: map['allowToChangeStatus'],
      allowToModifyCoordinator: map['allowToModifyCoordinator'],
      projectTemplateId: map['projectTemplateId'],
      workflowStatus: map['workflowStatus']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProjectModel.fromJson(String source) =>
      ProjectModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProjectModel(id: $id, acl: $acl, code: $code, name: $name, description: $description, parentId: $parentId, parentType: $parentType, priorityObj: $priorityObj, priority: $priority, weight: $weight, rank: $rank, labelsObjs: $labelsObjs, labels: $labels, managementObj: $managementObj, management: $management, ownerObj: $ownerObj, owner: $owner, supervisorObj: $supervisorObj, supervisor: $supervisor, coordinatorObj: $coordinatorObj, coordinator: $coordinator, assigneeObj: $assigneeObj, assignee: $assignee, viewerObj: $viewerObj, viewer: $viewer, statusObj: $statusObj, status: $status, objectId: $objectId, objectType: $objectType, objectAction: $objectAction, fromDate: $fromDate, toDate: $toDate, finishDate: $finishDate, estimateTime: $estimateTime, actualTime: $actualTime, order: $order, cLeft: $cLeft, cRight: $cRight, cLevel: $cLevel, allowToModify: $allowToModify, allowToView: $allowToView, allowToChangeStatus: $allowToChangeStatus, allowToModifyCoordinator: $allowToModifyCoordinator, projectTemplateId: $projectTemplateId, workflowStatus: $workflowStatus)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is ProjectModel &&
        other.id == id &&
        other.acl == acl &&
        other.code == code &&
        other.name == name &&
        other.description == description &&
        other.parentId == parentId &&
        other.parentType == parentType &&
        other.priorityObj == priorityObj &&
        other.priority == priority &&
        other.weight == weight &&
        other.rank == rank &&
        listEquals(other.labelsObjs, labelsObjs) &&
        other.labels == labels &&
        listEquals(other.managementObj, managementObj) &&
        other.management == management &&
        listEquals(other.ownerObj, ownerObj) &&
        other.owner == owner &&
        listEquals(other.supervisorObj, supervisorObj) &&
        other.supervisor == supervisor &&
        listEquals(other.coordinatorObj, coordinatorObj) &&
        other.coordinator == coordinator &&
        listEquals(other.assigneeObj, assigneeObj) &&
        other.assignee == assignee &&
        listEquals(other.viewerObj, viewerObj) &&
        other.viewer == viewer &&
        other.statusObj == statusObj &&
        other.status == status &&
        other.objectId == objectId &&
        other.objectType == objectType &&
        other.objectAction == objectAction &&
        other.fromDate == fromDate &&
        other.toDate == toDate &&
        other.finishDate == finishDate &&
        other.estimateTime == estimateTime &&
        other.actualTime == actualTime &&
        other.order == order &&
        other.cLeft == cLeft &&
        other.cRight == cRight &&
        other.cLevel == cLevel &&
        other.allowToModify == allowToModify &&
        other.allowToView == allowToView &&
        other.allowToChangeStatus == allowToChangeStatus &&
        other.allowToModifyCoordinator == allowToModifyCoordinator &&
        other.projectTemplateId == projectTemplateId &&
        other.workflowStatus == workflowStatus;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        acl.hashCode ^
        code.hashCode ^
        name.hashCode ^
        description.hashCode ^
        parentId.hashCode ^
        parentType.hashCode ^
        priorityObj.hashCode ^
        priority.hashCode ^
        weight.hashCode ^
        rank.hashCode ^
        labelsObjs.hashCode ^
        labels.hashCode ^
        managementObj.hashCode ^
        management.hashCode ^
        ownerObj.hashCode ^
        owner.hashCode ^
        supervisorObj.hashCode ^
        supervisor.hashCode ^
        coordinatorObj.hashCode ^
        coordinator.hashCode ^
        assigneeObj.hashCode ^
        assignee.hashCode ^
        viewerObj.hashCode ^
        viewer.hashCode ^
        statusObj.hashCode ^
        status.hashCode ^
        objectId.hashCode ^
        objectType.hashCode ^
        objectAction.hashCode ^
        fromDate.hashCode ^
        toDate.hashCode ^
        finishDate.hashCode ^
        estimateTime.hashCode ^
        actualTime.hashCode ^
        order.hashCode ^
        cLeft.hashCode ^
        cRight.hashCode ^
        cLevel.hashCode ^
        allowToModify.hashCode ^
        allowToView.hashCode ^
        allowToChangeStatus.hashCode ^
        allowToModifyCoordinator.hashCode ^
        projectTemplateId.hashCode ^
        workflowStatus.hashCode;
  }
}
