import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:serpapp/data/models/pm/tasks/task_metadata_model.dart';

import 'labels_obj_model.dart';
import 'related_task_model.dart';

class TaskModel {
  String? acl;
  String id;
  String name;
  String? description;
  String? parentId;
  String? parentType;
  int? priorityObj;
  int? priority;
  int? weight;
  int? rank;
  List<dynamic>? avatarUrl;
  List<TaskMetadataModel>? metadataContentObjs;
  List<LabelsObjModel>? labelsObjs;
  String? labels;
  List<String>? managementObj;
  String? management;
  List<String>? ownerObj;
  String? owner;
  List<String>? supervisorObj;
  String? supervisor;
  List<String>? coordinatorObj;
  String? coordinator;
  List<String>? assigneeObj;
  String? assignee;
  List<String>? viewerObj;
  String? viewer;
  int? statusObj;
  int? status;
  String? objectId;
  String? objectType;
  String? objectAction;
  DateTime? fromDate;
  DateTime? toDate;
  DateTime? finishDate;
  int? estimateTime;
  int? actualTime;
  String? metadataContent;
  String? metaContentType;
  int? order;
  int? cLeft;
  int? cRight;
  int? cLevel;
  bool? allowToModify;
  bool? allowToView;
  bool? allowToChangeStatus;
  bool? allowToModifyCoordinator;
  bool? allowToModifyDueDate;
  String? relatedFolderId;
  bool? allowToCreateSubTasks;
  String? projectId;
  String? code;
  String? type;
  String? attachment;
  int? workflowStatus;
  String? metadata;
  String? ref;
  List<RelatedTaskModel>? relatedTasks;
  List<dynamic>? children;
  TaskModel({
    this.acl,
    required this.id,
    required this.name,
    this.description,
    this.parentId,
    this.parentType,
    this.priorityObj,
    this.priority,
    this.weight,
    this.rank,
    this.labelsObjs,
    this.avatarUrl,
    this.labels,
    this.managementObj,
    this.management,
    this.ownerObj,
    this.owner,
    this.supervisorObj,
    this.supervisor,
    this.coordinatorObj,
    this.coordinator,
    this.assigneeObj,
    this.assignee,
    this.viewerObj,
    this.viewer,
    this.statusObj,
    this.status,
    this.objectId,
    this.objectType,
    this.objectAction,
    this.fromDate,
    this.toDate,
    this.finishDate,
    this.estimateTime,
    this.actualTime,
    this.metadataContent,
    this.metaContentType,
    this.order,
    this.cLeft,
    this.cRight,
    this.cLevel,
    this.allowToModify,
    this.allowToView,
    this.allowToChangeStatus,
    this.allowToModifyCoordinator,
    this.allowToModifyDueDate,
    this.relatedFolderId,
    this.allowToCreateSubTasks,
    this.projectId,
    this.code,
    this.type,
    this.attachment,
    this.workflowStatus,
    this.metadata,
    this.ref,
    this.relatedTasks,
    this.children,
    this.metadataContentObjs,
  });

  TaskModel copyWith({
    String? acl,
    String? id,
    String? name,
    String? description,
    String? parentId,
    String? parentType,
    int? priorityObj,
    int? priority,
    int? weight,
    int? rank,
    List<LabelsObjModel>? labelsObjs,
    List<dynamic>? avatarUrl,
    String? labels,
    List<String>? managementObj,
    List<TaskMetadataModel>? metadataContentObjs,
    String? management,
    List<String>? ownerObj,
    String? owner,
    List<String>? supervisorObj,
    String? supervisor,
    List<String>? coordinatorObj,
    String? coordinator,
    List<String>? assigneeObj,
    String? assignee,
    List<String>? viewerObj,
    String? viewer,
    int? statusObj,
    int? status,
    String? objectId,
    String? objectType,
    String? objectAction,
    DateTime? fromDate,
    DateTime? toDate,
    DateTime? finishDate,
    int? estimateTime,
    int? actualTime,
    String? metadataContent,
    String? metaContentType,
    int? order,
    int? cLeft,
    int? cRight,
    int? cLevel,
    bool? allowToModify,
    bool? allowToView,
    bool? allowToChangeStatus,
    bool? allowToModifyCoordinator,
    bool? allowToModifyDueDate,
    String? relatedFolderId,
    bool? allowToCreateSubTasks,
    String? projectId,
    String? code,
    String? type,
    String? attachment,
    int? workflowStatus,
    String? metadata,
    String? ref,
    List<RelatedTaskModel>? relatedTasks,
    List<dynamic>? children,
  }) {
    return TaskModel(
      acl: acl ?? this.acl,
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      parentId: parentId ?? this.parentId,
      parentType: parentType ?? this.parentType,
      priorityObj: priorityObj ?? this.priorityObj,
      priority: priority ?? this.priority,
      weight: weight ?? this.weight,
      rank: rank ?? this.rank,
      labelsObjs: labelsObjs ?? this.labelsObjs,
      metadataContentObjs: metadataContentObjs ?? this.metadataContentObjs,
      avatarUrl: avatarUrl ?? this.avatarUrl,
      labels: labels ?? this.labels,
      managementObj: managementObj ?? this.managementObj,
      management: management ?? this.management,
      ownerObj: ownerObj ?? this.ownerObj,
      owner: owner ?? this.owner,
      supervisorObj: supervisorObj ?? this.supervisorObj,
      supervisor: supervisor ?? this.supervisor,
      coordinatorObj: coordinatorObj ?? this.coordinatorObj,
      coordinator: coordinator ?? this.coordinator,
      assigneeObj: assigneeObj ?? this.assigneeObj,
      assignee: assignee ?? this.assignee,
      viewerObj: viewerObj ?? this.viewerObj,
      viewer: viewer ?? this.viewer,
      statusObj: statusObj ?? this.statusObj,
      status: status ?? this.status,
      objectId: objectId ?? this.objectId,
      objectType: objectType ?? this.objectType,
      objectAction: objectAction ?? this.objectAction,
      fromDate: fromDate ?? this.fromDate,
      toDate: toDate ?? this.toDate,
      finishDate: finishDate ?? this.finishDate,
      estimateTime: estimateTime ?? this.estimateTime,
      actualTime: actualTime ?? this.actualTime,
      metadataContent: metadataContent ?? this.metadataContent,
      metaContentType: metaContentType ?? this.metaContentType,
      order: order ?? this.order,
      cLeft: cLeft ?? this.cLeft,
      cRight: cRight ?? this.cRight,
      cLevel: cLevel ?? this.cLevel,
      allowToModify: allowToModify ?? this.allowToModify,
      allowToView: allowToView ?? this.allowToView,
      allowToChangeStatus: allowToChangeStatus ?? this.allowToChangeStatus,
      allowToModifyCoordinator:
          allowToModifyCoordinator ?? this.allowToModifyCoordinator,
      allowToModifyDueDate: allowToModifyDueDate ?? this.allowToModifyDueDate,
      relatedFolderId: relatedFolderId ?? this.relatedFolderId,
      allowToCreateSubTasks:
          allowToCreateSubTasks ?? this.allowToCreateSubTasks,
      projectId: projectId ?? this.projectId,
      code: code ?? this.code,
      type: type ?? this.type,
      attachment: attachment ?? this.attachment,
      workflowStatus: workflowStatus ?? this.workflowStatus,
      metadata: metadata ?? this.metadata,
      ref: ref ?? this.ref,
      relatedTasks: relatedTasks ?? this.relatedTasks,
      children: children ?? this.children,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (acl != null) {
      result.addAll({'acl': acl});
    }
    result.addAll({'id': id});
    result.addAll({'name': name});
    if (description != null) {
      result.addAll({'description': description});
    }
    if (parentId != null) {
      result.addAll({'parentId': parentId});
    }
    if (parentType != null) {
      result.addAll({'parentType': parentType});
    }
    if (priorityObj != null) {
      result.addAll({'priorityObj': priorityObj});
    }
    if (priority != null) {
      result.addAll({'priority': priority});
    }
    if (weight != null) {
      result.addAll({'weight': weight});
    }
    if (rank != null) {
      result.addAll({'rank': rank});
    }
    if (labelsObjs != null) {
      result.addAll({'labelsObjs': labelsObjs!.map((x) => x.toMap()).toList()});
    }
    if (metadataContentObjs != null) {
      result.addAll({
        'metadataContentObjs':
            metadataContentObjs?.map((x) => x.toMap()).toList()
      });
    }
    if (avatarUrl != null) {
      result.addAll({'avatarUrl': avatarUrl!.map((x) => x.toMap()).toList()});
    }
    if (labels != null) {
      result.addAll({'labels': labels});
    }
    if (managementObj != null) {
      result.addAll({'managementObj': managementObj});
    }
    if (management != null) {
      result.addAll({'management': management});
    }
    if (ownerObj != null) {
      result.addAll({'ownerObj': ownerObj});
    }
    if (owner != null) {
      result.addAll({'owner': owner});
    }
    if (supervisorObj != null) {
      result.addAll({'supervisorObj': supervisorObj});
    }
    if (supervisor != null) {
      result.addAll({'supervisor': supervisor});
    }
    if (coordinatorObj != null) {
      result.addAll({'coordinatorObj': coordinatorObj});
    }
    if (coordinator != null) {
      result.addAll({'coordinator': coordinator});
    }
    if (assigneeObj != null) {
      result.addAll({'assigneeObj': assigneeObj});
    }
    if (assignee != null) {
      result.addAll({'assignee': assignee});
    }
    if (viewerObj != null) {
      result.addAll({'viewerObj': viewerObj});
    }
    if (viewer != null) {
      result.addAll({'viewer': viewer});
    }
    if (statusObj != null) {
      result.addAll({'statusObj': statusObj});
    }
    if (status != null) {
      result.addAll({'status': status});
    }
    if (objectId != null) {
      result.addAll({'objectId': objectId});
    }
    if (objectType != null) {
      result.addAll({'objectType': objectType});
    }
    if (objectAction != null) {
      result.addAll({'objectAction': objectAction});
    }
    if (fromDate != null) {
      result.addAll({'fromDate': fromDate!.millisecondsSinceEpoch});
    }
    if (toDate != null) {
      result.addAll({'toDate': toDate!.millisecondsSinceEpoch});
    }
    if (finishDate != null) {
      result.addAll({'finishDate': finishDate!.millisecondsSinceEpoch});
    }
    if (estimateTime != null) {
      result.addAll({'estimateTime': estimateTime});
    }
    if (actualTime != null) {
      result.addAll({'actualTime': actualTime});
    }
    if (metadataContent != null) {
      result.addAll({'metadataContent': metadataContent});
    }
    if (metaContentType != null) {
      result.addAll({'metaContentType': metaContentType});
    }
    if (order != null) {
      result.addAll({'order': order});
    }
    if (cLeft != null) {
      result.addAll({'cLeft': cLeft});
    }
    if (cRight != null) {
      result.addAll({'cRight': cRight});
    }
    if (cLevel != null) {
      result.addAll({'cLevel': cLevel});
    }
    if (allowToModify != null) {
      result.addAll({'allowToModify': allowToModify});
    }
    if (allowToView != null) {
      result.addAll({'allowToView': allowToView});
    }
    if (allowToChangeStatus != null) {
      result.addAll({'allowToChangeStatus': allowToChangeStatus});
    }
    if (allowToModifyCoordinator != null) {
      result.addAll({'allowToModifyCoordinator': allowToModifyCoordinator});
    }
    if (allowToModifyDueDate != null) {
      result.addAll({'allowToModifyDueDate': allowToModifyDueDate});
    }
    if (relatedFolderId != null) {
      result.addAll({'relatedFolderId': relatedFolderId});
    }
    if (allowToCreateSubTasks != null) {
      result.addAll({'allowToCreateSubTasks': allowToCreateSubTasks});
    }
    if (projectId != null) {
      result.addAll({'projectId': projectId});
    }
    if (code != null) {
      result.addAll({'code': code});
    }
    if (type != null) {
      result.addAll({'type': type});
    }
    if (attachment != null) {
      result.addAll({'attachment': attachment});
    }
    if (workflowStatus != null) {
      result.addAll({'workflowStatus': workflowStatus});
    }
    if (metadata != null) {
      result.addAll({'metadata': metadata});
    }
    if (ref != null) {
      result.addAll({'ref': ref});
    }
    if (relatedTasks != null) {
      result.addAll(
          {'relatedTasks': relatedTasks!.map((x) => x.toMap()).toList()});
    }
    if (children != null) {
      result.addAll({'children': children});
    }

    return result;
  }

  factory TaskModel.fromMap(Map<String, dynamic> map) {
    return TaskModel(
      acl: map['acl'],
      id: map['id'] ?? '',
      name: map['name'] ?? '',
      description: map['description'],
      parentId: map['parentId'],
      parentType: map['parentType'],
      priorityObj: map['priorityObj']?.toInt(),
      priority: map['priority']?.toInt(),
      weight: map['weight']?.toInt(),
      rank: map['rank']?.toInt(),
      labelsObjs: map['labelsObjs'] != null
          ? List<LabelsObjModel>.from(
              map['labelsObjs']?.map((x) => LabelsObjModel.fromMap(x)))
          : null,
      avatarUrl: map['avatarUrl'],
      labels: map['labels'],
      managementObj: map['managementObj'] != null
          ? List<String>.from(map['managementObj'])
          : null,
      management: map['management'],
      ownerObj:
          map['ownerObj'] != null ? List<String>.from(map['ownerObj']) : null,
      owner: map['owner'],
      supervisorObj: map['supervisorObj'] != null
          ? List<String>.from(map['supervisorObj'])
          : null,
      supervisor: map['supervisor'],
      coordinatorObj: map['coordinatorObj'] != null
          ? List<String>.from(map['coordinatorObj'])
          : null,
      coordinator: map['coordinator'],
      assigneeObj: map['assigneeObj'] != null
          ? List<String>.from(map['assigneeObj'])
          : null,
      assignee: map['assignee'],
      metadataContentObjs: map['metadataContentObjs'] != null
          ? List<TaskMetadataModel>.from(map['metadataContentObjs']
              ?.map((x) => x != null ? TaskMetadataModel.fromMap(x) : TaskMetadataModel.fromMap(map)))
          : null,
      viewerObj:
          map['viewerObj'] != null ? List<String>.from(map['viewerObj']) : null,
      viewer: map['viewer'],
      statusObj: map['statusObj']?.toInt(),
      status: map['status']?.toInt(),
      objectId: map['objectId'],
      objectType: map['objectType'],
      objectAction: map['objectAction'],
      fromDate:
          map['fromDate'] != null ? DateTime.parse(map['fromDate']) : null,
      toDate: map['toDate'] != null ? DateTime.parse(map['toDate']) : null,
      finishDate:
          map['finishDate'] != null ? DateTime.parse(map['finishDate']) : null,
      estimateTime: map['estimateTime']?.toInt(),
      actualTime: map['actualTime']?.toInt(),
      metadataContent: map['metadataContent'],
      metaContentType: map['metaContentType'],
      order: map['order']?.toInt(),
      cLeft: map['cLeft']?.toInt(),
      cRight: map['cRight']?.toInt(),
      cLevel: map['cLevel']?.toInt(),
      allowToModify: map['allowToModify'],
      allowToView: map['allowToView'],
      allowToChangeStatus: map['allowToChangeStatus'],
      allowToModifyCoordinator: map['allowToModifyCoordinator'],
      allowToModifyDueDate: map['allowToModifyDueDate'],
      relatedFolderId: map['relatedFolderId'],
      allowToCreateSubTasks: map['allowToCreateSubTasks'],
      projectId: map['projectId'],
      code: map['code'],
      type: map['type'],
      attachment: map['attachment'],
      workflowStatus: map['workflowStatus']?.toInt(),
      metadata: map['metadata'],
      ref: map['ref'],
      relatedTasks: map['relatedTasks'] != null
          ? List<RelatedTaskModel>.from(
              map['relatedTasks']?.map((x) => RelatedTaskModel.fromMap(x)))
          : null,
      children:
          map['children'] != null ? List<dynamic>.from(map['children']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory TaskModel.fromJson(String source) =>
      TaskModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TaskModel(acl: $acl, id: $id, name: $name, description: $description, parentId: $parentId, parentType: $parentType, priorityObj: $priorityObj, priority: $priority, weight: $weight, rank: $rank, labelsObjs: $labelsObjs, metadataContentObjs: $metadataContentObjs, avatarUrl: $avatarUrl, labels: $labels, managementObj: $managementObj, management: $management, ownerObj: $ownerObj, owner: $owner, supervisorObj: $supervisorObj, supervisor: $supervisor, coordinatorObj: $coordinatorObj, coordinator: $coordinator, assigneeObj: $assigneeObj, assignee: $assignee, viewerObj: $viewerObj, viewer: $viewer, statusObj: $statusObj, status: $status, objectId: $objectId, objectType: $objectType, objectAction: $objectAction, fromDate: $fromDate, toDate: $toDate, finishDate: $finishDate, estimateTime: $estimateTime, actualTime: $actualTime, metadataContent: $metadataContent, metaContentType: $metaContentType, order: $order, cLeft: $cLeft, cRight: $cRight, cLevel: $cLevel, allowToModify: $allowToModify, allowToView: $allowToView, allowToChangeStatus: $allowToChangeStatus, allowToModifyCoordinator: $allowToModifyCoordinator, allowToModifyDueDate: $allowToModifyDueDate, relatedFolderId: $relatedFolderId, allowToCreateSubTasks: $allowToCreateSubTasks, projectId: $projectId, code: $code, type: $type, attachment: $attachment, workflowStatus: $workflowStatus, metadata: $metadata, ref: $ref, relatedTasks: $relatedTasks, children: $children)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is TaskModel &&
        other.acl == acl &&
        other.id == id &&
        other.name == name &&
        other.description == description &&
        other.parentId == parentId &&
        other.parentType == parentType &&
        other.priorityObj == priorityObj &&
        other.priority == priority &&
        other.weight == weight &&
        other.rank == rank &&
        listEquals(other.labelsObjs, labelsObjs) &&
        listEquals(other.avatarUrl, avatarUrl) &&
        other.labels == labels &&
        listEquals(other.managementObj, managementObj) &&
        other.management == management &&
        listEquals(other.ownerObj, ownerObj) &&
        other.owner == owner &&
        listEquals(other.supervisorObj, supervisorObj) &&
        other.supervisor == supervisor &&
        listEquals(other.coordinatorObj, coordinatorObj) &&
        other.coordinator == coordinator &&
        listEquals(other.assigneeObj, assigneeObj) &&
        other.assignee == assignee &&
        listEquals(other.viewerObj, viewerObj) &&
        other.viewer == viewer &&
        listEquals(other.metadataContentObjs, metadataContentObjs) &&
        other.statusObj == statusObj &&
        other.status == status &&
        other.objectId == objectId &&
        other.objectType == objectType &&
        other.objectAction == objectAction &&
        other.fromDate == fromDate &&
        other.toDate == toDate &&
        other.finishDate == finishDate &&
        other.estimateTime == estimateTime &&
        other.actualTime == actualTime &&
        other.metadataContent == metadataContent &&
        other.metaContentType == metaContentType &&
        other.order == order &&
        other.cLeft == cLeft &&
        other.cRight == cRight &&
        other.cLevel == cLevel &&
        other.allowToModify == allowToModify &&
        other.allowToView == allowToView &&
        other.allowToChangeStatus == allowToChangeStatus &&
        other.allowToModifyCoordinator == allowToModifyCoordinator &&
        other.allowToModifyDueDate == allowToModifyDueDate &&
        other.relatedFolderId == relatedFolderId &&
        other.allowToCreateSubTasks == allowToCreateSubTasks &&
        other.projectId == projectId &&
        other.code == code &&
        other.type == type &&
        other.attachment == attachment &&
        other.workflowStatus == workflowStatus &&
        other.metadata == metadata &&
        other.ref == ref &&
        listEquals(other.relatedTasks, relatedTasks) &&
        listEquals(other.children, children);
  }

  @override
  int get hashCode {
    return acl.hashCode ^
        id.hashCode ^
        name.hashCode ^
        description.hashCode ^
        parentId.hashCode ^
        parentType.hashCode ^
        priorityObj.hashCode ^
        priority.hashCode ^
        weight.hashCode ^
        rank.hashCode ^
        labelsObjs.hashCode ^
        avatarUrl.hashCode ^
        labels.hashCode ^
        managementObj.hashCode ^
        management.hashCode ^
        ownerObj.hashCode ^
        metadataContentObjs.hashCode ^
        owner.hashCode ^
        supervisorObj.hashCode ^
        supervisor.hashCode ^
        coordinatorObj.hashCode ^
        coordinator.hashCode ^
        assigneeObj.hashCode ^
        assignee.hashCode ^
        viewerObj.hashCode ^
        viewer.hashCode ^
        statusObj.hashCode ^
        status.hashCode ^
        objectId.hashCode ^
        objectType.hashCode ^
        objectAction.hashCode ^
        fromDate.hashCode ^
        toDate.hashCode ^
        finishDate.hashCode ^
        estimateTime.hashCode ^
        actualTime.hashCode ^
        metadataContent.hashCode ^
        metaContentType.hashCode ^
        order.hashCode ^
        cLeft.hashCode ^
        cRight.hashCode ^
        cLevel.hashCode ^
        allowToModify.hashCode ^
        allowToView.hashCode ^
        allowToChangeStatus.hashCode ^
        allowToModifyCoordinator.hashCode ^
        allowToModifyDueDate.hashCode ^
        relatedFolderId.hashCode ^
        allowToCreateSubTasks.hashCode ^
        projectId.hashCode ^
        code.hashCode ^
        type.hashCode ^
        attachment.hashCode ^
        workflowStatus.hashCode ^
        metadata.hashCode ^
        ref.hashCode ^
        relatedTasks.hashCode ^
        children.hashCode;
  }
}
