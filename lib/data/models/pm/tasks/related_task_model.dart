import 'dart:convert';

class RelatedTaskModel {
  int? relationType;
  String? task;
  String? note;
  RelatedTaskModel({
    this.relationType,
    this.task,
    this.note,
  });

  RelatedTaskModel copyWith({
    int? relationType,
    String? task,
    String? note,
  }) {
    return RelatedTaskModel(
      relationType: relationType ?? this.relationType,
      task: task ?? this.task,
      note: note ?? this.note,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    if (relationType != null) {
      result.addAll({'relationType': relationType});
    }
    if (task != null) {
      result.addAll({'task': task});
    }
    if (note != null) {
      result.addAll({'note': note});
    }

    return result;
  }

  factory RelatedTaskModel.fromMap(Map<String, dynamic> map) {
    return RelatedTaskModel(
      relationType: map['relationType']?.toInt(),
      task: map['task'],
      note: map['note'],
    );
  }

  String toJson() => json.encode(toMap());

  factory RelatedTaskModel.fromJson(String source) =>
      RelatedTaskModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'RelatedTaskModel(relationType: $relationType, task: $task, note: $note)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is RelatedTaskModel &&
        other.relationType == relationType &&
        other.task == task &&
        other.note == note;
  }

  @override
  int get hashCode => relationType.hashCode ^ task.hashCode ^ note.hashCode;
}
