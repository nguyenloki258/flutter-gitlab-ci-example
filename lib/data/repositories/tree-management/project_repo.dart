import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/tree-management/project_apis.dart';
import 'package:serpapp/data/models/tree-management/project_apis/project_content_model.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class ProjectRepository {
  final ProjectAPI projectsAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();
  Future<Object> getAll(
    dynamic page,
    dynamic size,
    dynamic filter,
  ) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectsAPI.getAll(
      Environment.ApplicationId,
      userToken,
      page,
      size,
      filter,
    );

    List<ProjectContentModel> listProjectModels = [];
    if (res.code == 200) {
      res.data["content"].forEach(
          (x) => listProjectModels.add(ProjectContentModel.fromMap(x)));
    }
    if (res is ResponseError) return res;
    return listProjectModels.toList();
  }

  Future<Object> getProjectById(String id) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectsAPI.getProjectById(
        Environment.ApplicationId, userToken, id);
    if (res.code == 200 && res.data != "") {
      return ProjectContentModel.fromMap(res.data);
    }
    if (res is ResponseError) return res;
    return {};
  }

  Future<Object> updateStateProject(String id, statusObj) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectsAPI.updateStateProject(
        Environment.ApplicationId, userToken, id, statusObj.value);
    List<ProjectContentModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(ProjectContentModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }

  Future<Object> updateDescriptionProjects(
      String id, String description) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectsAPI.updateDescriptionProjects(
        Environment.ApplicationId, userToken, id, description);
    List<ProjectContentModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(ProjectContentModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }

  Future<Object> updateAssigneeObjProjects(
      String id, List<dynamic> assigneeObj) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectsAPI.updateAssigneeObjProject(
        Environment.ApplicationId, userToken, id, assigneeObj);
    List<ProjectContentModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(ProjectContentModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }

  Future<Object> updateMetadataContentObjs(
      String id, List<dynamic> metadataContentObjs) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectsAPI.updateMetadataContentObjs(
        Environment.ApplicationId, userToken, id, metadataContentObjs);
    List<ProjectContentModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(ProjectContentModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }
}
