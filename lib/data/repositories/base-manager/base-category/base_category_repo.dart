import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/models/base-manager/base-category/base_category_content_model.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

import '../../../apis/base-manager/base-category/base_category_apis.dart';

class BaseCategoryRepository {
  final BaseCategoryAPI baseCategoryAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();
  Future<Object> getListCategoryGroups(
    String type,
    dynamic page,
    dynamic size,
    dynamic queryString,
  ) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await baseCategoryAPI.getListCategoryGroups(
        Environment.ApplicationId, userToken, type, page, size, queryString);

    List<ContentCategoryModel> listBaseCategoryModels = [];
    if (res.code == 200) {
      res.data["content"].forEach(
          (x) => listBaseCategoryModels.add(ContentCategoryModel.fromMap(x)));
    }

    if (res is ResponseError) return res;
    return listBaseCategoryModels.toList();
  }
}
