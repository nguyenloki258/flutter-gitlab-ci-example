import 'package:data_module/data_module.dart';
import 'package:serpapp/configurations/app_keywords.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/authentication_apis.dart';
import 'package:serpapp/data/models/authentication/auth_model.dart';
import 'package:get/get.dart';

class AuthenticationRepository {
  final EncryptedStorage storage = Get.find();
  final AuthenticationAPI authenticationAPI = Get.find();

  Future<ResponseBase> register(String applicationId, String username,
      String firstName, String lastName, String phoneNumber) async {
    final res = await authenticationAPI.register(
        applicationId, username, firstName, lastName, phoneNumber);

    if (res is ResponseObject && res.data != null) {
      final authModel = AuthModel.fromMap(res.data);

      return ResponseObject<AuthModel>(
          code: res.code,
          message: res.message,
          totalTime: res.totalTime,
          data: authModel);
    }
    return res;
  }

  Future<AuthModel> login(
      String applicationId, String username, String password) async {
    final res =
        await authenticationAPI.login(applicationId, username, password);

    if (res is ResponseObject && res.data != null) {
      final authModel = AuthModel.fromMap(res.data);
      setLocalUserData(authModel);
      return authModel;
    }

    if (res is ResponseError) {
      throw Exception(res.message);
    } else if (res.message != null) {
      throw Exception(res.message);
    }
    throw Exception('CannotLogin');
  }

  Future<ResponseBase> changePassword(
      String oldPassword, String newPassword, String confirmPassword) async {
    var token = await getUserToken();
    final res = await authenticationAPI.changePassword(
        Environment.ApplicationId,
        token,
        oldPassword,
        newPassword,
        confirmPassword);

    if (res is ResponseObject && res.data != null) {
      final authModel = AuthModel.fromMap(res.data);
      return ResponseObject<AuthModel>(
          code: res.code,
          message: res.message,
          totalTime: res.totalTime,
          data: authModel);
    }
    if (res is ResponseError) return res;

    return res;
  }

  Future<AuthModel?> updateUserProfile({String? avatarUrl}) async {
    var token = await getUserToken();
    var res = await authenticationAPI.updateUserProfile(
        Environment.ApplicationId, token,
        avatarUrl: avatarUrl);

    if (res is ResponseObject && res.data != null) {
      var user = await getLocalUserData();
      if (user != null) {
        var userRes = AuthModel.fromMap(res.data);
        user.avatarUrl = userRes.avatarUrl ?? '';
        await setLocalUserData(user);
      }
      return user;
    }

    throw Exception('CannotGetUserInfo');
  }

  Future logout() async {
    await setLocalUserData(null);
  }

  Future<AuthModel?> getLocalUserData() async {
    final userData = await storage.readData(userKeyword);
    try {
      final user = AuthModel.fromJson(userData);
      return user;
    } catch (ex) {
      return null;
    }
  }

  Future<String> getUserToken() async {
    try {
      final user = await getLocalUserData();
      if (user == null) return '';
      return user.token ?? '';
    } catch (ex) {
      return '';
    }
  }

  Future setLocalUserData(AuthModel? user) async {
    await storage.writeData(
        userKeyword,
        /* cspell:disable */
        user == null
            ? "A236safsOHAncvnbcxvncheryhermpsedgf^5dhDOVDsdfsdf"
            : user.toJson());
  }

  Future removeLocalUserData() async {
    await storage.deleteData(userKeyword);
  }
}
