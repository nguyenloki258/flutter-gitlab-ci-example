import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/models/notification/notification_content_model.dart';

import '../../apis/notification/notification_apis.dart';
import '../authentication_repo.dart';

class NotificationRepository {
  final NotificationAPI notificationsAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();
  Future<Object> getAllNotification(
      int page, int size, dynamic filter, List<String> modules) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await notificationsAPI.getAllNotification(
      Environment.ApplicationId,
      userToken,
      modules,
      page,
      size,
      filter,
    );

    List<NotificationContentModel> listNotificationModels = [];
    if (res.code == 200) {
      res.data["content"].forEach((x) =>
          listNotificationModels.add(NotificationContentModel.fromMap(x)));
    }
    if (res is ResponseError) return res;
    return listNotificationModels.toList();
  }
}
