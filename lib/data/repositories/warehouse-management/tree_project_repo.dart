import 'package:data_module/data_module.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/warehouse-management/tree_project_apis.dart';
import 'package:serpapp/data/models/tree/tree_project_model/project_tree_model.dart';
import 'package:get/get.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class TreeProjectRepository {
  final TreeProjectAPI treeProjectAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();

  Future<Object> getAllTreeProject(
      dynamic page, dynamic size, String filter, String type) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await treeProjectAPI.getAllTreeProject(
        Environment.ApplicationId, userToken, page, size, filter, type);
    List<ProjectTreeModel> listTreeProjectModels = [];
    if (res.code == 200) {
      res.data["content"].forEach(
          (x) => listTreeProjectModels.add(ProjectTreeModel.fromMap(x)));

      // ignore: unused_local_variable
      ResponsePagination<ProjectTreeModel> temp = ResponsePagination.fromJson(
        ResponseObject(
          code: res.code,
          message: res.message,
          totalTime: res.totalTime,
          data: Pagination<ProjectTreeModel>(
              currentPage: res.data["currentPage"],
              totalPage: res.data["totalPage"],
              numberOfRecords: res.data["numberOfRecords"],
              pageSize: res.data["pageSize"],
              totalRecords: res.data["totalRecords"],
              content: listTreeProjectModels),
        ),
      );
    }
    if (res is ResponseError) return res;
    return listTreeProjectModels.toList();
  }
}
