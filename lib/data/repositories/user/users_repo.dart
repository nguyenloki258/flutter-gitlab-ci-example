import 'package:data_module/data_module.dart';
import 'package:flutter/foundation.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/user/users_apis.dart';
import 'package:serpapp/data/models/user/user_model.dart';
import 'package:serpapp/data/models/user/users_by_code_model.dart';
import 'package:get/get.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class UsersRepository {
  final EncryptedStorage storage = Get.find();
  final UsersAPI usersAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();

  Future<Object> getUsersRolesByCode(String code) async {
    var userToken = await authRepo.getUserToken();
    var res = await usersAPI.getRolesByCode(
        Environment.ApplicationId, userToken, code);

    if (res is ResponseObject && res.data != null) {
      if (res is ResponseObject<UsersByCodeModel>) {
        if (kDebugMode) {
          print('success');
        }
      }
      List<UsersByCodeModel> userModel = [];
      res.data.forEach((x) => userModel.add(UsersByCodeModel.fromMap(x)));

      return userModel;
    }
    if (res is ResponseError) return res;
    return res;
  }

  Future<List<UserModel>> getAllUsersByApplication() async {
    var userToken = await authRepo.getUserToken();
    var res = await usersAPI.getUserByApplication(
        Environment.ApplicationId, userToken);

    List<UserModel> listUserModels = [];
    if (res.code == 200) {
      res.data.forEach((x) => listUserModels.add(UserModel.fromMap(x)));
    }
    return listUserModels;
  }
}
