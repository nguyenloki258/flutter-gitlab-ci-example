import 'package:data_module/data_module.dart';
import 'package:flutter/foundation.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/file_management/file_apis.dart';
import 'package:serpapp/data/models/file-management/file_model.dart';
import 'package:get/get.dart';

import '../authentication_repo.dart';

class FileRepository {
  final FileAPI filesAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();

  Future<Object> getFiles(String relatedObjectId) async {
    var userToken = await authRepo.getUserToken();
    dynamic res = await filesAPI.getFiles(
        Environment.ApplicationId, userToken, relatedObjectId);

    List<FileModel> listFileResponse = [];
    if (res.code == 200 && res.data != null) {
      res.data.forEach((x) => listFileResponse.add(FileModel.fromMap(x)));
    } else {
      if (kDebugMode) {
        print('No data');
      }
    }
    if (res is ResponseError) return res;
    return listFileResponse.toList();
  }

  Future<FileModel> uploadFiles(
    String fileUrl, {
    String? name,
    String? type,
    String? description,
    String? parentId,
    String? objectId,
    String? objectType,
    String? relatedProjectId,
    String? relatedTaskId,
  }) async {
    var userToken = await authRepo.getUserToken();

    final res = await filesAPI.uploadFiles(
        Environment.ApplicationId, userToken, fileUrl,
        name: name,
        type: type,
        description: description,
        parentId: parentId,
        objectId: objectId,
        objectType: objectType,
        relatedProjectId: relatedProjectId,
        relatedTaskId: relatedTaskId);

    if (res is ResponseObject && res.data != null) {
      final fileModel = FileModel.fromMap(res.data);
      return fileModel;
    }

    if (res is ResponseError) {
      throw Exception(res.message);
    } else if (res.message != null) {
      throw Exception(res.message);
    }
    throw Exception('CannotUploadImage');
  }

  Future<FileModel> uploadImage(String fileUrl, String fileName) async {
    var userToken = await authRepo.getUserToken();

    final res = await filesAPI.uploadImage(
      Environment.ApplicationId,
      userToken,
      fileUrl,
      fileName,
    );

    if (res is ResponseObject && res.data != null) {
      final fileModel = FileModel.fromMap(res.data);
      return fileModel;
    }

    if (res is ResponseError) {
      throw Exception(res.message);
    } else if (res.message != null) {
      throw Exception(res.message);
    }
    throw Exception('CannotUploadImage');
  }
}
