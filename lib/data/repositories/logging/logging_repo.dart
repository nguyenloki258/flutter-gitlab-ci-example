import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/logging/logging%20_apis.dart';

import '../authentication_repo.dart';

class LoggingRepository {
  final LoggingAPI loggingAPI = Get.put(LoggingAPI());
  final AuthenticationRepository authRepo = Get.find();

  Future<List> postLogging(String fullTextSearch, int currentPage, int pageSize,
      String module, String objectType, String objectId) async {
    List loggingModel = [];
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await loggingAPI.postLogging(
        Environment.ApplicationId,
        userToken,
        fullTextSearch,
        currentPage,
        pageSize,
        module,
        objectType,
        objectId);

    if (res.data != null) {
      for (var content in res.data["content"]) {
        loggingModel.add(content);
      }
    }

    return loggingModel.toList();
  }
}
