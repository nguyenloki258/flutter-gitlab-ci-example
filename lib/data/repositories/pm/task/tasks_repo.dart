import 'package:data_module/data_module.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/pm/task/task_apis.dart';
import 'package:serpapp/data/models/pm/tasks/task_model.dart';
import 'package:get/get.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class TasksRepository {
  final TaskAPI tasksAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();
  Future<Object> getAllTasksFilters(
    dynamic page,
    dynamic size,
    dynamic sort,
    dynamic filter,
  ) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await tasksAPI.getAllTasksFilters(
      Environment.ApplicationId,
      userToken,
      page,
      size,
      sort,
      filter,
    );

    List<TaskModel> listTaskModels = [];
    if (res.code == 200) {
      res.data["content"]
          .forEach((x) => listTaskModels.add(TaskModel.fromMap(x)));
    }
    if (res is ResponseError) return res;
    return listTaskModels.toList();
  }

  Future<Object> getAllTasksByProjectId(
    String projectId,
    page,
    size,
    sort,
    filter,
  ) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await tasksAPI.getTasksByProjectId(
        Environment.ApplicationId,
        userToken,
        projectId,
        page,
        size,
        sort,
        filter);
    List<TaskModel> listTaskByProjectId = [];
    if (res.code == 200) {
      res.data["content"]
          .forEach((x) => listTaskByProjectId.add(TaskModel.fromMap(x)));
    }
    if (res is ResponseError) return res;
    return listTaskByProjectId.toList();
  }

  Future<Object> getTasksById(String id) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res =
        await tasksAPI.getTasksById(Environment.ApplicationId, userToken, id);
    if (res.code == 200 && res.data != "") {
      return TaskModel.fromMap(res.data);
    }
    if (res is ResponseError) return res;
    return {};
  }

  Future<Object> updateStateTasks(String id, statusObj) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await tasksAPI.updateStateTasks(
        Environment.ApplicationId, userToken, id, statusObj.value);
    List<TaskModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(TaskModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }

  Future<Object> updateUserTasks(String id, List<dynamic> assigneeObj) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await tasksAPI.updateUserActionTasks(
        Environment.ApplicationId, userToken, id, assigneeObj);
    List<TaskModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(TaskModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }

  Future<Object> updateDescriptionTasks(String id, String description) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await tasksAPI.updateDescriptionTasks(
        Environment.ApplicationId, userToken, id, description);
    List<TaskModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(TaskModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }

  Future<Object> updateDateTimeTasks(
      String id, String key, String dateTime) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await tasksAPI.updateDateTimeTasks(
        Environment.ApplicationId, userToken, id, key, dateTime);
    List<TaskModel> listTaskUpdateModels = [];
    if (res.code == 200) {
      listTaskUpdateModels.add(TaskModel.fromMap(res.data));
    }
    if (res is ResponseError) return res;
    return listTaskUpdateModels.toList();
  }
}
