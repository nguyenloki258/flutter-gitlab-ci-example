import 'package:data_module/data_module.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/pm/project/project_apis.dart';
import 'package:get/get.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class PMProjectRepository {
  final PMProjectAPI projectAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();

  Future<dynamic> getAllProjectFilters(
    dynamic page,
    dynamic size,
    dynamic sort,
    dynamic filter,
  ) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await projectAPI.getAllProjectFilters(
      Environment.ApplicationId,
      userToken,
      page,
      size,
      sort,
      filter,
    );

    List listProjectModels = [];
    if (res.code == 200) {
      res.data["content"].forEach((x) => listProjectModels.add(x));
    }

    if (res is ResponseError) return res;
    return listProjectModels.toList();
  }
}
