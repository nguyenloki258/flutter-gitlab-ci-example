import 'package:data_module/data_module.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/apis/communication/comment_apis.dart';
import 'package:serpapp/data/models/Communication/comment_model.dart';

import 'package:get/get.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';

class CommentRepository {
  final CommentAPI commentAPI = Get.find();
  final AuthenticationRepository authRepo = Get.find();

  Future<CommentModel> postComment(String objectId, String objectType,
      String? ref, String message, statusObj, String? attachment) async {
    var userToken = await authRepo.getUserToken();
    final res = await commentAPI.postComment(Environment.ApplicationId,
        userToken, objectId, objectType, ref, message, statusObj, attachment);

    if (res is ResponseObject && res.data != null) {
      final commentModel = CommentModel.fromMap(res.data);
      return commentModel;
    }

    if (res is ResponseError) {
      throw Exception(res.message);
    } else if (res.message != null) {
      throw Exception(res.message);
    }
    throw Exception('CannotSendMessage');
  }

  Future<Object> getComment(String objectId, int page, int size, dynamic sort,
      dynamic queryString, dynamic filter) async {
    var userToken = await authRepo.getUserToken();
    ResponseBase res = await commentAPI.getComment(Environment.ApplicationId,
        userToken, objectId, page, size, sort, queryString, filter);

    List<CommentModel> listCommentModels = [];
    if (res.code == 200) {
      res.data["content"]
          .forEach((x) => listCommentModels.add(CommentModel.fromMap(x)));
    }
    if (res is ResponseError) return res;
    return listCommentModels.toList();
  }
}
