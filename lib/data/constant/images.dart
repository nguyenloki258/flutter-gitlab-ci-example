class Images {
  static String apple = 'assets/social/apple.png';
  static String google = 'assets/social/google.png';
  static String facebook = 'assets/social/facebook.png';
  static String logo = 'assets/logo/logo-transparent.svg';
}
