import 'package:core_packages/core_package.dart';
import 'package:serpapp/widgets/app/app_priority.dart';
import 'package:serpapp/widgets/app/app_tag.dart';

const cho_kiem_tra = 'Chờ kiểm tra';
const cho_thuc_hien = 'Chờ thực hiện';
const dang_thuc_hien = 'Đang thực hiện';
const hoan_thanh = 'Hoàn thành';
const kiem_tra_lai = 'Kiểm tra lại';
const tam_hoan = 'Tạm hoãn';
const huy_bo = 'Hủy bỏ';

const khong_xac_dinh = 'Không xác định';
const cao_nhat = 'Cao nhất';
const trung_binh = 'Trung bình';
const thap = 'Thấp';
const thap_nhat = 'Thấp nhất';
const cao = 'Cao';

const cay_trang_tri = 'Cây trang trí';
const cay_che_bong = 'Cây che bóng';
const cay_phu_nen = 'Cây phủ nền';

const comment = 'Bình luận';
const history = 'Lịch sử';

List<SelectOptionItem> listStatus = [
  SelectOptionItem(key: khong_xac_dinh, value: TaskStatus.undefined.value),
  SelectOptionItem(key: cho_thuc_hien, value: TaskStatus.waitForProgress.value),
  SelectOptionItem(key: dang_thuc_hien, value: TaskStatus.inProgress.value),
  SelectOptionItem(key: cho_kiem_tra, value: TaskStatus.resolve.value),
  SelectOptionItem(key: hoan_thanh, value: TaskStatus.done.value),
  SelectOptionItem(key: kiem_tra_lai, value: TaskStatus.reopen.value),
  SelectOptionItem(key: tam_hoan, value: TaskStatus.pending.value),
  SelectOptionItem(key: huy_bo, value: TaskStatus.failed.value)
];

// Độ ưu tiên
class SelectsPriorityObj {
  String key = '';
  dynamic value = 0;
  bool status = false;
  SelectsPriorityObj(
      {required this.key, required this.value, this.status = false});
}

List<SelectOptionItem> listPriority = [
  SelectOptionItem(key: khong_xac_dinh, value: IssuePriority.undefined.value),
  SelectOptionItem(key: thap_nhat, value: IssuePriority.lowest.value),
  SelectOptionItem(key: thap, value: IssuePriority.low.value),
  SelectOptionItem(key: trung_binh, value: IssuePriority.medium.value),
  SelectOptionItem(key: cao, value: IssuePriority.high.value),
  SelectOptionItem(key: cao_nhat, value: IssuePriority.highest.value),
];
