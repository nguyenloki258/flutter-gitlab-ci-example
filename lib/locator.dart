import 'package:data_module/data_module.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';
import 'package:serpapp/data/apis/authentication_apis.dart';
import 'package:serpapp/data/apis/base-manager/base-category/base_category_apis.dart';
import 'package:serpapp/data/apis/logging/logging%20_apis.dart';
import 'package:serpapp/data/apis/pm/project/project_apis.dart';
import 'package:serpapp/data/apis/pm/task/task_apis.dart';
import 'package:serpapp/data/apis/warehouse-management/tree_project_apis.dart';
import 'package:serpapp/data/repositories/authentication_repo.dart';
import 'package:serpapp/data/repositories/base-manager/base-category/base_category_repo.dart';
import 'package:serpapp/data/repositories/file_management/file_repo.dart';
import 'package:serpapp/data/repositories/pm/project/project_repo.dart';
import 'data/apis/communication/comment_apis.dart';
import 'data/apis/file_management/file_apis.dart';
import 'data/apis/notification/notification_apis.dart';
import 'data/apis/tree-management/project_apis.dart';
import 'data/apis/user/users_apis.dart';
import 'data/repositories/communication/comment_repo.dart';
import 'data/repositories/logging/logging_repo.dart';
import 'data/repositories/notification/notification_repo.dart';
import 'data/repositories/pm/task/tasks_repo.dart';
import 'data/repositories/tree-management/project_repo.dart';
import 'data/repositories/user/users_repo.dart';
import 'package:serpapp/data/repositories/warehouse-management/tree_project_repo.dart';

void setupLocator() {
  Get.lazyPut<Dio>(() => Dio(), fenix: true);
  Get.lazyPut<AuthenticationRepository>(() => AuthenticationRepository(),
      fenix: true);
  Get.lazyPut<AuthenticationAPI>(() => AuthenticationAPI(), fenix: true);
  Get.lazyPut<HeaderRepository>(() => HeaderRepository(), fenix: true);

  Get.lazyPut<FetchManager>(() => const FetchManager(), fenix: true);
  Get.lazyPut<EncryptedStorage>(() => EncryptedStorage(), fenix: true);

  // Get.lazyPut<HealthAPI>(() =>HealthAPI(),fenix: true);

  Get.lazyPut<TaskAPI>(() => TaskAPI(), fenix: true);
  Get.lazyPut<TasksRepository>(() => TasksRepository(), fenix: true);

  Get.lazyPut<ProjectAPI>(() => ProjectAPI(), fenix: true);
  Get.lazyPut<ProjectRepository>(() => ProjectRepository(), fenix: true);

  Get.lazyPut<UsersAPI>(() => UsersAPI(), fenix: true);
  Get.lazyPut<UsersRepository>(() => UsersRepository(), fenix: true);

  // Communication
  Get.lazyPut<CommentAPI>(() => CommentAPI(), fenix: true);
  Get.lazyPut<CommentRepository>(() => CommentRepository(), fenix: true);

  //tree project
  Get.lazyPut<TreeProjectAPI>(() => TreeProjectAPI(), fenix: true);
  Get.lazyPut<TreeProjectRepository>(() => TreeProjectRepository(),
      fenix: true);

  Get.lazyPut<FileAPI>(() => FileAPI(), fenix: true);
  Get.lazyPut<FileRepository>(() => FileRepository(), fenix: true);

  Get.lazyPut<BaseCategoryRepository>(() => BaseCategoryRepository(),
      fenix: true);
  Get.lazyPut<BaseCategoryAPI>(() => BaseCategoryAPI(), fenix: true);
  Get.lazyPut<PMProjectRepository>(() => PMProjectRepository(), fenix: true);
  Get.lazyPut<PMProjectAPI>(() => PMProjectAPI(), fenix: true);

  Get.lazyPut<NotificationAPI>(() => NotificationAPI(), fenix: true);
  Get.lazyPut<NotificationRepository>(() => NotificationRepository(),
      fenix: true);

  Get.lazyPut<LoggingAPI>(() => LoggingAPI(), fenix: true);
  Get.lazyPut<LoggingRepository>(() => LoggingRepository(), fenix: true);
  // Get.lazyPut<UserAPI>(() =>UserAPI(),fenix: true);
  // Get.lazyPut<ContactAPI>(() =>ContactAPI(),fenix: true);

  // Get.lazyPut<UserRepository>(() =>UserRepository(),fenix: true);
  // Get.lazyPut<FileMngRepository>(() =>FileMngRepository(),fenix: true);
  // Get.lazyPut<ContactRepository>(() =>ContactRepository(),fenix: true);
}
