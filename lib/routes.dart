import 'package:serpapp/ui/screens/app/app_routes.dart';
import 'package:serpapp/ui/screens/auth/auth_routes.dart';

final routes = [
  ...authRoutes,
  ...appRoutes,
];
