/*
* File : Main File
* Version : 1.0.0
* */

import 'package:get/get.dart';
import 'package:serpapp/localizations/app_localization_delegate.dart';
import 'package:serpapp/localizations/language.dart';
import 'package:serpapp/theme/app_notifier.dart';
import 'package:serpapp/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutx/themes/app_theme_notifier.dart';
import 'package:provider/provider.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:serpapp/ui/screens/auth/slash/slash_screen.dart';
import 'locator.dart';
import 'routes.dart';

Future<void> main() async {
  //Init DI Service
  setupLocator();
  //End Init DI Service
  //Init Hive
  await Hive.initFlutter();
  //End Init Hive
  //You will need to initialize AppThemeNotifier class for theme changes.
  WidgetsFlutterBinding.ensureInitialized();

  AppTheme.init();

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(ChangeNotifierProvider<AppNotifier>(
    create: (context) => AppNotifier(),
    child: ChangeNotifierProvider<FxAppThemeNotifier>(
      create: (context) => FxAppThemeNotifier(),
      child: const SerpApp(),
    ),
  ));
}

class SerpApp extends StatelessWidget {
  const SerpApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppNotifier>(
      builder: (BuildContext context, AppNotifier value, Widget? child) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          getPages: routes,
          theme: AppTheme.theme,
          builder: (context, child) {
            return Directionality(
              textDirection: AppTheme.textDirection,
              child: child!,
            );
          },
          localizationsDelegates: [
            AppLocalizationsDelegate(context),
            // Add this line
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: Language.getLocales(),
          locale: Locale('vi'),
          transitionDuration: Duration(milliseconds: 300),
          defaultTransition: Transition.fadeIn,
          initialRoute: SlashScreen.routeName,
        );
      },
    );
  }
}
