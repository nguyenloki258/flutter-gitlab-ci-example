import 'package:flutter/material.dart';
import 'package:flutx/widgets/text/text.dart';
import 'package:serpapp/configurations/environment.dart';
import 'package:serpapp/data/constant/constant.dart';

Widget appTag(
    {required int status, double width = 110.0, bool isSelectedItem = false}) {
  Color borderColor = Colors.black;
  Color backgroundColor = Colors.pink;
  String statusLabel = '';

  if (Environment.ApplicationId == '45dfb441-3e62-4321-b6c0-baea2bf1012f') {
    if (status == TaskStatus.waitForProgress.value) {
      backgroundColor = Colors.orange.withOpacity(0.1);
      borderColor = Colors.orange;
      statusLabel = TaskStatus.waitForProgress.label;
    } else if (status == TaskStatus.inProgress.value) {
      backgroundColor = Colors.blue.withOpacity(0.1);
      borderColor = Colors.blue;
      statusLabel = TaskStatus.inProgress.label;
    } else if (status == TaskStatus.failed.value) {
      backgroundColor = Colors.red.withOpacity(0.1);
      borderColor = Colors.red;
      statusLabel = TaskStatus.failed.label;
    } else if (status == TaskStatus.pending.value) {
      backgroundColor = Colors.purple.withOpacity(0.1);
      borderColor = Colors.purple;
      statusLabel = TaskStatus.pending.label;
    } else if (status == TaskStatus.done.value) {
      backgroundColor = Colors.green.withOpacity(0.1);
      borderColor = Colors.green;
      statusLabel = TaskStatus.done.label;
    }
  } else {
    if (status == TaskStatus.waitForProgress.value) {
      backgroundColor = Colors.orange.withOpacity(0.1);
      borderColor = Colors.orange;
      statusLabel = TaskStatus.waitForProgress.label;
    } else if (status == TaskStatus.inProgress.value) {
      backgroundColor = Colors.blue.withOpacity(0.1);
      borderColor = Colors.blue;
      statusLabel = TaskStatus.inProgress.label;
    } else if (status == TaskStatus.resolve.value) {
      backgroundColor = Colors.white;
      borderColor = Colors.blue;
      statusLabel = TaskStatus.resolve.label;
    } else if (status == TaskStatus.reopen.value) {
      backgroundColor = Colors.black.withOpacity(0.1);
      borderColor = Colors.black;
      statusLabel = TaskStatus.reopen.label;
    } else if (status == TaskStatus.failed.value) {
      backgroundColor = Colors.red.withOpacity(0.1);
      borderColor = Colors.red;
      statusLabel = TaskStatus.failed.label;
    } else if (status == TaskStatus.pending.value) {
      backgroundColor = Colors.purple.withOpacity(0.1);
      borderColor = Colors.purple;
      statusLabel = TaskStatus.pending.label;
    } else if (status == TaskStatus.done.value) {
      backgroundColor = Colors.green.withOpacity(0.1);
      borderColor = Colors.green;
      statusLabel = TaskStatus.done.label;
    }
  }
  if (isSelectedItem) {
    return Text(
      statusLabel,
      style: TextStyle(color: borderColor),
    );
  }
  return Container(
    decoration: BoxDecoration(
        color: backgroundColor,
        border: Border.all(color: borderColor.withOpacity(0.7), width: 1),
        borderRadius: BorderRadius.circular(5)),
    padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
    child: FxText.labelMedium(
      statusLabel,
      textAlign: TextAlign.center,
      color: borderColor,
    ),
  );
}

String appTagLabel({required int status}) {
  String statusLabel = '';

  if (Environment.ApplicationId == '45dfb441-3e62-4321-b6c0-baea2bf1012f') {
    if (status == TaskStatus.waitForProgress.value) {
      statusLabel = TaskStatus.waitForProgress.label;
    } else if (status == TaskStatus.inProgress.value) {
      statusLabel = TaskStatus.inProgress.label;
    } else if (status == TaskStatus.failed.value) {
      statusLabel = TaskStatus.failed.label;
    } else if (status == TaskStatus.pending.value) {
      statusLabel = TaskStatus.pending.label;
    } else if (status == TaskStatus.done.value) {
      statusLabel = TaskStatus.done.label;
    }
  } else {
    if (status == TaskStatus.waitForProgress.value) {
      statusLabel = TaskStatus.waitForProgress.label;
    } else if (status == TaskStatus.inProgress.value) {
      statusLabel = TaskStatus.inProgress.label;
    } else if (status == TaskStatus.resolve.value) {
      statusLabel = TaskStatus.resolve.label;
    } else if (status == TaskStatus.reopen.value) {
      statusLabel = TaskStatus.reopen.label;
    } else if (status == TaskStatus.failed.value) {
      statusLabel = TaskStatus.failed.label;
    } else if (status == TaskStatus.pending.value) {
      statusLabel = TaskStatus.pending.label;
    } else if (status == TaskStatus.done.value) {
      statusLabel = TaskStatus.done.label;
    }
  }

  return statusLabel;
}

enum TaskStatus {
  undefined,
  waitForProgress,
  inProgress,
  resolve,
  reopen,
  failed,
  done,
  pending,
}

extension TaskStatusExtension on TaskStatus {
  int get value {
    switch (this) {
      case TaskStatus.undefined:
        return 0;
      case TaskStatus.waitForProgress:
        return 32;
      case TaskStatus.inProgress:
        return 47;
      case TaskStatus.resolve:
        return 53;
      case TaskStatus.done:
        return 57;
      case TaskStatus.reopen:
        return 67;
      case TaskStatus.pending:
        return 73;
      case TaskStatus.failed:
        return 99;
      default:
        return 0;
    }
  }

  String get label {
    switch (this) {
      case TaskStatus.undefined:
        return khong_xac_dinh;
      case TaskStatus.waitForProgress:
        return cho_thuc_hien;
      case TaskStatus.inProgress:
        return dang_thuc_hien;
      case TaskStatus.resolve:
        return cho_kiem_tra;
      case TaskStatus.done:
        return hoan_thanh;
      case TaskStatus.reopen:
        return kiem_tra_lai;
      case TaskStatus.pending:
        return tam_hoan;
      case TaskStatus.failed:
        return huy_bo;
      default:
        return "";
    }
  }
}
