import 'package:flutter/material.dart';
import 'package:flutx/widgets/text/text.dart';
import 'package:serpapp/data/constant/constant.dart';

Widget appPriority({required int priority, showLabel = true}) {
  Color backgroundColor = Colors.pink;
  IconData icon = Icons.compare_arrows_outlined;
  // ignore: unused_local_variable
  String statusLabel = '';

  if (priority == IssuePriority.undefined.value) {
    icon = Icons.compare_arrows_outlined;
    backgroundColor = Colors.orange.withOpacity(0.9);
    statusLabel = IssuePriority.undefined.IssuePriorityValues;
  } else if (priority == IssuePriority.highest.value) {
    icon = Icons.arrow_upward_rounded;
    backgroundColor = Colors.blue.withOpacity(0.9);
    statusLabel = IssuePriority.highest.IssuePriorityValues;
  } else if (priority == IssuePriority.high.value) {
    icon = Icons.arrow_upward_rounded;
    backgroundColor = Colors.white;
    statusLabel = IssuePriority.high.IssuePriorityValues;
  } else if (priority == IssuePriority.medium.value) {
    icon = Icons.arrow_upward_rounded;
    backgroundColor = Colors.orange.withOpacity(0.9);
    statusLabel = IssuePriority.medium.IssuePriorityValues;
  } else if (priority == IssuePriority.low.value) {
    icon = Icons.arrow_downward;
    backgroundColor = Colors.green.withOpacity(0.9);
    statusLabel = IssuePriority.low.IssuePriorityValues;
  } else if (priority == IssuePriority.lowest.value) {
    icon = Icons.arrow_downward;
    backgroundColor = Colors.green.withOpacity(0.9);
    statusLabel = IssuePriority.lowest.IssuePriorityValues;
  }

  return Row(
    children: [
      Icon(
        icon,
        color: backgroundColor,
        size: 14,
      ),
      showLabel
          ? FxText.labelMedium(
              statusLabel,
            )
          : SizedBox(),
    ],
  );
}

enum IssuePriority {
  undefined,
  highest,
  high,
  medium,
  low,
  lowest,
}

extension PriorityStatusExtension on IssuePriority {
  int get value {
    switch (this) {
      case IssuePriority.undefined:
        return 0;
      case IssuePriority.lowest:
        return 5;
      case IssuePriority.low:
        return 9;
      case IssuePriority.medium:
        return 17;
      case IssuePriority.high:
        return 45;
      case IssuePriority.highest:
        return 49;
      default:
        return 0;
    }
  }

  // ignore: non_constant_identifier_names
  String get IssuePriorityValues {
    switch (this) {
      case IssuePriority.undefined:
        return khong_xac_dinh;
      case IssuePriority.lowest:
        return thap_nhat;
      case IssuePriority.low:
        return thap;
      case IssuePriority.medium:
        return trung_binh;
      case IssuePriority.high:
        return cao;
      case IssuePriority.highest:
        return cao_nhat;
      default:
        return khong_xac_dinh;
    }
  }
}
