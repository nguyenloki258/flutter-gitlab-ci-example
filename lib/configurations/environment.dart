// ignore_for_file: constant_identifier_names

export 'api_address.dart';

class Environment {
  // static const ApplicationId = '7fd86fa3-ed00-d2dc-4295-36931edb8ff8';
  // static const ApplicationId = 'f6982879-8b0f-4005-ae2f-71a033cfa9c2'; // Hanoitax
  static const ApplicationId =
      '45dfb441-3e62-4321-b6c0-baea2bf1012f'; // Ecopark
  static const APP_VERSION = '0.3.0';
}
