// ignore_for_file: constant_identifier_names
/* cSpell:disable */
class APIAddress {
  static const AUTHENTICATION_SERVICE_API = 'http://dev.s-erp.com.vn:9041/';
  static const USERMNG_SERVICE_API = 'http://dev.s-erp.com.vn:9043/';
  // static const PROJECT_MANAGEMENT_SERVICE_API =
  //     'http://dev.s-erp.com.vn:2231/';
  static const PROJECT_MANAGEMENT_SERVICE_API = 'http://dev.s-erp.com.vn:9031/';
  static const WAREHOUSE_SERVICE_API = 'http://dev.s-erp.com.vn:2248/';
  static const FILE_SERVICE_API = 'http://dev.s-erp.com.vn:2245/';
  static const COMMUNICATION_SERVICE_API = 'http://dev.s-erp.com.vn:9056/';
  static const BASE_SERVICE_API = 'http://dev.s-erp.com.vn:9039/';
  static const TREE_MANAGEMENT_SERVICE_API = 'http://dev.s-erp.com.vn:2055/';
  static const NOTIFICATION_SERVICE_API = 'http://dev.s-erp.com.vn:9035/';
  static const LOGGING_SERVICE_API = 'http://dev.s-erp.com.vn:9032/';
}
