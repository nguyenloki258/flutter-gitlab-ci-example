import 'package:intl/intl.dart';

getFormatedDate(_date) {
  if (_date != null) {
    var inputFormat = DateFormat('yyyy-MM-dd HH:mm');
    var inputDate = inputFormat.parse(_date);
    var outputFormat = DateFormat('dd/MM/y');
    return outputFormat.format(inputDate);
  }
}
