import 'package:encrypt/encrypt.dart';
import 'package:hive/hive.dart';

class EncryptedStorage {
  /* cSpell:disable */
  final String _privateKey = 'deesqjQfpr6rFHdZi7ApM2LfrWOJfh9R';

  Future<String> readData(String keyword) async {
    try {
      final key = Key.fromUtf8(_privateKey);
      final iv = IV.fromLength(16);
      final encrypter = Encrypter(AES(key));

      var box = await getBox<String>("storage");
      String data = box.get(keyword.toLowerCase()) ?? "";
      final decrypted = encrypter.decrypt64(data, iv: iv);
      return decrypted;
    } catch (e) {
      return '';
    }
  }

  Future<void> writeData(String keyword, String data) async {
    var box = await getBox<String>("storage");
    final key = Key.fromUtf8(_privateKey);
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(data, iv: iv);
    box.put(keyword.toLowerCase(), encrypted.base64);
  }

  Future<Box<T>> getBox<T>(String boxName) async {
    if (!Hive.isBoxOpen(boxName)) {
      await Hive.openBox<T>(boxName);
    }
    return Hive.box<T>(boxName);
  }

  Future<void> writeDataObj<T>(String keyword, T data) async {
    // var box = await getBox<T>("storage" + keyword.toLowerCase());
    // box.put(keyword.toLowerCase(), data);
  }

  Future<T?> readDataObj<T>(String keyword) async {
    // var box = await getBox<T>("storage" + keyword.toLowerCase());
    // T data = box.get(keyword.toLowerCase()) as T;
    // return data;
    return null;
  }

  Future<void> deleteData(String keyword) async {
    var box = await getBox<String>("storage");
    box.delete(keyword.toLowerCase());
  }

  Future<void> deleteDataObj<T>(String keyword) async {
    var box = await getBox<T>("storage${keyword.toLowerCase()}");
    box.delete(keyword.toLowerCase());
  }
}
