import 'package:flutter/material.dart';
import 'package:flutx/widgets/widgets.dart';
import 'package:get/get.dart';

Widget buildCircleAvatar(String name,
    {String? avatarUrl,
    IconData? icon,
    double radius = 25,
    Color? bgColor,
    Color? fgColor}) {
  if (avatarUrl != null &&
      avatarUrl != '' &&
      avatarUrl != '/Attachs/avatar/avatar-default.jpg' &&
      avatarUrl != 'null') {
    return CircleAvatar(
      backgroundColor: bgColor ?? Get.theme.colorScheme.primary,
      radius: radius,
      child: CircleAvatar(
        radius: radius - 2,
        backgroundImage: NetworkImage(avatarUrl),
      ),
    );
  } else if (icon != null) {
    return CircleAvatar(
      backgroundColor: bgColor ?? Get.theme.colorScheme.primary,
      radius: radius,
      child: Icon(icon,
          size: radius + 3, color: fgColor ?? Get.theme.colorScheme.onPrimary),
    );
  } else {
    return CircleAvatar(
      backgroundColor: bgColor ?? Get.theme.colorScheme.primary,
      radius: radius,
      child: Text(
        name.isNotEmpty ? name[0].toUpperCase() : '',
        style: TextStyle(
            fontSize: radius,
            color: fgColor ?? Get.theme.colorScheme.onPrimary),
      ),
    );
  }
}

Widget buildAvatarList(List list) {
  List<Widget> listAvatar = [];
  int numberDisplayItem = list.length > 3 ? 3 : list.length;
  int numberOtherItem = list.length - 3;
  for (var i = 0; i < numberDisplayItem; i++) {
    listAvatar.add(Container(
      transform: Matrix4.translationValues(i * -8.0, 0.0, 0.0),
      child: buildCircleAvatar('', radius: 14, avatarUrl: list[i].avatarUrl),
    ));
  }
  if (numberOtherItem > 0) {
    listAvatar.add(Container(
      transform: Matrix4.translationValues(3 * -8.0, 0.0, 0.0),
      width: 38,
      height: 38,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Get.theme.colorScheme.primary,
          borderRadius: BorderRadius.circular(50),
          border: Border.all(color: Get.theme.colorScheme.onPrimary, width: 2)),
      child: FxText.labelLarge(
        "+" + numberOtherItem.toString(),
        color: Get.theme.colorScheme.onPrimary,
      ),
    ));
  }
  return Row(
    children: listAvatar,
  );
}
