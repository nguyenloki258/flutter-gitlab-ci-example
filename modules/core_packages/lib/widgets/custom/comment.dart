import 'dart:math';

import 'package:core_packages/core_package.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/utils/spacing.dart';
import 'package:flutx/widgets/container/container.dart';
import 'package:flutx/widgets/text/text.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

Widget buildComment(
    {required String avatar,
    required String name,
    required String time,
    required String contentComment}) {
  return Column(
    children: [
      FxSpacing.height(6),
      Row(
        children: [
          buildCircleAvatar(name, avatarUrl: avatar, radius: 20),
          FxSpacing.width(6),
          Expanded(
              flex: 7,
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                          flex: 5,
                          child: FxText.bodySmall(name,
                              fontWeight: 600, textAlign: TextAlign.left)),
                      FxText.bodySmall(
                        time,
                      ),
                    ],
                  ),
                  Row(
                    children: [FxText.bodySmall(contentComment)],
                  ),
                ],
              )),
        ],
      ),
      FxSpacing.height(4),
    ],
  );
}

Widget buildLog(
    {required String avatar,
    required String name,
    required String action,
    required String time,
    required String detailAction}) {
  return Column(
    children: [
      FxSpacing.height(6),
      Row(
        children: [
          buildCircleAvatar(name, avatarUrl: avatar, radius: 20),
          FxSpacing.width(6),
          Expanded(
              flex: 7,
              child: Column(
                children: [
                  Row(
                    children: [FxText.bodySmall(time)],
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 5,
                          child: FxText.bodySmall(action,
                              fontWeight: 600, textAlign: TextAlign.left)),
                    ],
                  ),
                  Row(
                    children: [FxText.bodySmall(detailAction)],
                  ),
                ],
              )),
        ],
      ),
      FxSpacing.height(4),
    ],
  );
}

Widget buildFileList(
    {required String name,
    required IconData icon,
    required String size,
    required String time}) {
  return Column(
    children: [
      FxSpacing.height(6),
      Row(
        children: [
          Icon(icon),
          FxSpacing.width(6),
          Expanded(
              flex: 7,
              child: Column(
                children: [
                  Row(
                    children: [FxText.bodySmall(time)],
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 5,
                          child: FxText.bodySmall(name,
                              overflow: TextOverflow.ellipsis,
                              fontWeight: 600,
                              textAlign: TextAlign.left)),
                      FxText.bodySmall(size)
                    ],
                  ),
                ],
              )),
        ],
      ),
      FxSpacing.height(4),
    ],
  );
}

String formatBytes(int bytes, int decimals) {
  if (bytes <= 0) return "0 B";
  const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
  var i = (log(bytes) / log(1024)).floor();
  return ((bytes / pow(1024, i)).toStringAsFixed(decimals)) + ' ' + suffixes[i];
}

Widget buildFileAttachment(List data) {
  return ExpansionTile(
    tilePadding: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 5),
    childrenPadding: FxSpacing.only(bottom: 5, top: 5, right: 5, left: 5),
    title: FxText.bodyMedium(
      "Tập tin đính kèm",
    ),
    children: [
      for (var element in data)
        buildFileList(
            icon: FeatherIcons.fileText,
            name: element.name,
            size: formatBytes(element.metadata.fileSize ?? 0, 1),
            time: DateFormat('hh:mm,dd/MM/yyyy')
                .format(element.createdOnDate ?? DateTime.now())),
      FxSpacing.height(10),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: FxContainer(
              onTap: () {
                // userProfileController.logout();
              },
              padding: FxSpacing.xy(8, 8),
              borderRadiusAll: 4,
              color: Get.theme.colorScheme.primary,
              child: Icon(
                FeatherIcons.camera,
                size: 20,
                color: Get.theme.colorScheme.onPrimary,
              ),
            ),
          ),
          FxSpacing.width(24),
          Container(
            alignment: Alignment.center,
            child: FxContainer(
              onTap: () {
                // userProfileController.logout();
              },
              padding: FxSpacing.xy(8, 8),
              borderRadiusAll: 4,
              color: Get.theme.colorScheme.primary,
              child: Icon(
                FeatherIcons.paperclip,
                size: 20,
                color: Get.theme.colorScheme.onPrimary,
              ),
            ),
          ),
        ],
      )
    ],
  );
}
