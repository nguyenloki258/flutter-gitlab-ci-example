import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutx/flutx.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class SelectOptionItem {
  String key = '';
  dynamic value = 0;
  dynamic data;
  bool status = false;
  SelectOptionItem(
      {required this.key, required this.value, this.status = false, this.data});
}

Widget buildSettingItem(
    {required String title,
    IconData? icon,
    Widget? value,
    Widget? longValue,
    IconData? iconAction,
    Function? callback}) {
  return Column(children: [
    InkWell(
      onTap: () {
        if (callback != null) callback();
      },
      child: Row(
        children: [
          Icon(icon, size: 20),
          FxSpacing.width(20),
          Expanded(
              child: FxText.bodyMedium(
            title,
            fontWeight: 600,
          )),
          FxSpacing.width(20),
          value ?? FxSpacing.width(20),
          FxSpacing.width(0),
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            child: Icon(iconAction ?? FeatherIcons.chevronRight,
                size: 16,
                color: callback != null
                    ? Get.theme.colorScheme.onBackground
                    : Get.theme.colorScheme.onBackground.withAlpha(20)),
          )
        ],
      ),
    ),
    Row(
      children: [
        FxSpacing.width(40),
        longValue ?? FxSpacing.width(20),
      ],
    ),
  ]);
}

Widget buildSelectOptions(
    String title,
    List<SelectOptionItem> options,
    List<SelectOptionItem> selectedOptions,
    Function(SelectOptionItem) onSelect,
    Function(SelectOptionItem) onRemove,
    Widget Function(SelectOptionItem)? buildOption) {
  List<Widget> choices = [];
  for (var item in options) {
    bool selected = selectedOptions.contains(item);
    if (selected) {
      choices.add(FxContainer.none(
          color: Get.theme.colorScheme.primary.withAlpha(28),
          bordered: true,
          borderRadiusAll: 12,
          padding: FxSpacing.xy(12, 8),
          onTap: () {
            onRemove(item);
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              FxSpacing.width(6),
              buildOption != null ? buildOption(item) : FxSpacing.width(0),
              FxText.bodyMedium(
                item.key,
                color: Get.theme.colorScheme.primary,
              )
            ],
          )));
    } else {
      choices.add(FxContainer.none(
          color: Get.theme.cardTheme.color,
          borderRadiusAll: 12,
          padding: FxSpacing.xy(12, 8),
          onTap: () {
            onSelect(item);
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildOption != null ? buildOption(item) : FxSpacing.width(0),
              FxText.bodyMedium(
                item.key,
              )
            ],
          )));
    }
  }
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          FxText.bodyMedium(
            title,
            color: Get.theme.colorScheme.onBackground,
            fontWeight: 600,
          ),
        ],
      ),
      FxSpacing.height(8),
      Align(
          alignment: Alignment.topLeft,
          child: Wrap(spacing: 10, runSpacing: 10, children: [...choices])),
      FxSpacing.height(8),
    ],
  );
}

Widget buildPickerSingleChoice(
    {required String title,
    required SelectOptionItem? value,
    required List<SelectOptionItem> options,
    required Function(SelectOptionItem) onSelect,
    required Function() onSubmit,
    Widget Function(SelectOptionItem)? buildOption}) {
  return Container(
    height: 250,
    margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(8), bottom: Radius.circular(0)),
      color: Get.theme.scaffoldBackgroundColor,
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Drawer(
      child: Container(
        color: Get.theme.scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: FxSpacing.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Huỷ',
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FxText.bodyLarge(
                      title,
                      textAlign: TextAlign.center,
                      fontWeight: 600,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                      onSubmit();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Xác nhận',
                        color: Get.theme.colorScheme.primary,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FxSpacing.height(8),
            Expanded(
                child: CupertinoPicker(
              backgroundColor: Get.theme.backgroundColor,
              scrollController: FixedExtentScrollController(
                  initialItem: value != null ? options.indexOf(value) : 0),
              onSelectedItemChanged: (index) {
                onSelect(options[index]);
              },
              itemExtent: 30.0,
              children: [
                for (var option in options)
                  buildOption != null
                      ? buildOption(option)
                      : FxText.titleMedium(option.key),
              ],
            )),
          ],
        ),
      ),
    ),
  );
}

Widget buildPickerDate(
    {required String title,
    required DateTime? value,
    required Function(DateTime) onSelect,
    required Function() onSubmit,
    Widget Function(SelectOptionItem)? buildOption}) {
  return Container(
    height: 250,
    margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(8), bottom: Radius.circular(0)),
      color: Get.theme.scaffoldBackgroundColor,
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Drawer(
      child: Container(
        color: Get.theme.scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: FxSpacing.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Huỷ',
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FxText.bodyLarge(
                      title,
                      textAlign: TextAlign.center,
                      fontWeight: 600,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                      onSubmit();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Xác nhận',
                        color: Get.theme.colorScheme.primary,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FxSpacing.height(8),
            Expanded(
              child: CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                  initialDateTime: value,
                  onDateTimeChanged: (dateTime) {
                    onSelect(dateTime);
                  }),
            )
          ],
        ),
      ),
    ),
  );
}

Widget buildPickerText(
    {required String title,
    required String? value,
    required Function(String) onSelect,
    required Function() onSubmit,
    Widget Function(SelectOptionItem)? buildOption,
    TextInputType? keyboardType}) {
  return Container(
    height: 250,
    margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(8), bottom: Radius.circular(0)),
      color: Get.theme.scaffoldBackgroundColor,
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Drawer(
      child: Container(
        color: Get.theme.scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: FxSpacing.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Huỷ',
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FxText.bodyLarge(
                      title,
                      textAlign: TextAlign.center,
                      fontWeight: 600,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                      onSubmit();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Xác nhận',
                        color: Get.theme.colorScheme.primary,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FxSpacing.height(8),
            Expanded(
                child: Container(
                    margin:
                        FxSpacing.only(bottom: 10, top: 0, right: 10, left: 10),
                    child: CupertinoTextField(
                      controller: TextEditingController(text: value),
                      onChanged: (getValue) => onSelect(getValue),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Get.theme.colorScheme.background,
                          border:
                              Border.all(color: Get.theme.colorScheme.primary)),
                      cursorColor: Get.theme.colorScheme.primary,
                      placeholder: "Nhập vào dữ liệu",
                      placeholderStyle: TextStyle(
                          color: Get.theme.colorScheme.onBackground
                              .withAlpha(160)),
                      minLines: 12,
                      maxLines: 24,
                      style:
                          TextStyle(color: Get.theme.colorScheme.onBackground),
                      padding: FxSpacing.xy(8, 16),
                      keyboardType: keyboardType,
                    )))
          ],
        ),
      ),
    ),
  );
}

Widget buildPickerMultiChoice({
  required String title,
  required List<SelectOptionItem> value,
  required List<SelectOptionItem> options,
  required Function(SelectOptionItem) onSelect,
  required Function(SelectOptionItem) onRemove,
  required Function() onSubmit,
  Widget Function(SelectOptionItem)? buildOption,
}) {
  return Container(
    height: 250,
    margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(8), bottom: Radius.circular(0)),
      color: Get.theme.scaffoldBackgroundColor,
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Drawer(
      child: Container(
        color: Get.theme.scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: FxSpacing.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Huỷ',
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FxText.bodyLarge(
                      title,
                      textAlign: TextAlign.center,
                      fontWeight: 600,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                      onSubmit();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Xác nhận',
                        color: Get.theme.colorScheme.primary,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FxSpacing.height(8),
            Expanded(
              child: Container(
                  margin:
                      FxSpacing.only(bottom: 0, top: 0, right: 10, left: 10),
                  child: buildSelectOptions("", options, value,
                      (p0) => onSelect(p0), (p0) => onRemove(p0), buildOption)),
            )
          ],
        ),
      ),
    ),
  );
}

Widget buildPickerTag({
  required String title,
  required List<SelectOptionItem> value,
  required Function(SelectOptionItem) onAdd,
  required Function(SelectOptionItem) onRemove,
  required Function() onSubmit,
  Widget Function(SelectOptionItem)? buildOption,
}) {
  return Container(
    height: 250,
    margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(8), bottom: Radius.circular(0)),
      color: Get.theme.scaffoldBackgroundColor,
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Drawer(
      child: Container(
        color: Get.theme.scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: FxSpacing.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Huỷ',
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FxText.bodyLarge(
                      title,
                      textAlign: TextAlign.center,
                      fontWeight: 600,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                      onSubmit();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Xác nhận',
                        color: Get.theme.colorScheme.primary,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FxSpacing.height(8),
            Expanded(
                child: Container(
                    margin:
                        FxSpacing.only(bottom: 0, top: 0, right: 10, left: 10),
                    child: Wrap(spacing: 10, runSpacing: 10, children: [
                      for (var item in value)
                        FxContainer.none(
                            color: Get.theme.colorScheme.primary.withAlpha(28),
                            bordered: true,
                            borderRadiusAll: 12,
                            padding: FxSpacing.xy(12, 8),
                            onTap: () {
                              onRemove(item);
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                FxSpacing.width(6),
                                buildOption != null
                                    ? buildOption(item)
                                    : FxSpacing.width(0),
                                FxText.bodyMedium(
                                  item.key,
                                  color: Get.theme.colorScheme.primary,
                                ),
                                Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: Get.theme.colorScheme.primary),
                                      child: FxText.bodyMedium(
                                        "X",
                                        color: Get.theme.colorScheme.onPrimary,
                                      ),
                                    ))
                              ],
                            )),
                      CupertinoTextField(
                        onSubmitted: (p0) =>
                            onAdd(SelectOptionItem(key: p0, value: p0)),
                        decoration: BoxDecoration(
                            color: Get.theme.colorScheme.background),
                        cursorColor: Get.theme.colorScheme.secondary,
                        placeholder: "Nhập thêm",
                        style: TextStyle(
                            color: Get.theme.colorScheme.onBackground),
                        placeholderStyle: TextStyle(
                            color: Get.theme.colorScheme.onBackground
                                .withAlpha(160)),
                        padding: FxSpacing.xy(8, 16),
                      )
                    ])))
          ],
        ),
      ),
    ),
  );
}

Widget buildQuickSelectOptions(
    {required String title,
    required List<SelectOptionItem> selectedOptions,
    required Function onClick}) {
  String finalTitle = title;
  if (selectedOptions.length == 1) {
    finalTitle = title + ": " + selectedOptions[0].key;
  }
  if (selectedOptions.length > 1) {
    finalTitle = selectedOptions.length.toString() + " " + title;
  }
  return FxContainer.none(
      borderRadiusAll: 12,
      padding: FxSpacing.xy(12, 12),
      color: selectedOptions.isNotEmpty
          ? Get.theme.colorScheme.primary.withAlpha(28)
          : Get.theme.cardTheme.color,
      onTap: () {
        onClick();
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          FxText.bodySmall(
            finalTitle,
            color: selectedOptions.isNotEmpty
                ? Get.theme.colorScheme.primary
                : null,
          )
        ],
      ));
}

Widget buildQuickDateRangeOptions(
    {required String title,
    required DateTimeRange? dateTime,
    required Function onClick}) {
  String finalTitle = title;
  if (dateTime != null) {
    finalTitle = title +
        ": " +
        DateFormat("dd/MM/yyyy").format(dateTime.start) +
        " - " +
        DateFormat("dd/MM/yyyy").format(dateTime.end);
  }

  return FxContainer.none(
      borderRadiusAll: 12,
      padding: FxSpacing.xy(12, 12),
      color: dateTime != null
          ? Get.theme.colorScheme.primary.withAlpha(28)
          : Get.theme.cardTheme.color,
      onTap: () {
        onClick();
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          FxText.bodySmall(
            finalTitle,
            color: dateTime != null ? Get.theme.colorScheme.primary : null,
          )
        ],
      ));
}

Widget buildPickerGender({
  required String title,
  required int? value,
  required Function(int) onSelect,
  required Function() onSubmit,
}) {
  List _gender = ['Nữ', 'Nam', 'Khác'];
  List<Widget> _genderIcon = [
    const Icon(Icons.female),
    const Icon(Icons.male),
    const Icon(Icons.transgender),
  ];
  return Container(
    height: 250,
    margin: FxSpacing.only(bottom: 0, top: 0, right: 0, left: 0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(8), bottom: Radius.circular(0)),
      color: Get.theme.scaffoldBackgroundColor,
    ),
    clipBehavior: Clip.antiAliasWithSaveLayer,
    child: Drawer(
      child: Container(
        color: Get.theme.scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: FxSpacing.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Huỷ',
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FxText.bodyLarge(
                      'Sửa: $title',
                      textAlign: TextAlign.center,
                      fontWeight: 600,
                    ),
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    onTap: () {
                      Get.back();
                      onSubmit();
                    },
                    child: SizedBox(
                      width: 100,
                      child: FxText.bodyMedium(
                        'Xác nhận',
                        color: Get.theme.colorScheme.primary,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FxSpacing.height(8),
            Expanded(
                child: Container(
              margin: FxSpacing.only(bottom: 10, top: 0, right: 10, left: 10),
              child: CupertinoPicker(
                  useMagnifier: true,
                  itemExtent: 42,
                  onSelectedItemChanged: (value) {
                    onSelect(value);
                    // setState(() {});
                  },
                  children: List<Widget>.generate(
                      3,
                      (index) => Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: _genderIcon[index])),
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(_gender[index]))),
                            ],
                          ))),
            ))
          ],
        ),
      ),
    ),
  );
}
